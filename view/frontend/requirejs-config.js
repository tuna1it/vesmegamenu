/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
 var config = {
 	map: {
 		"*": {
 			OwlCarousel: "Vnecoms_Megamenu/lib/owl.carousel/owl.carousel.min",
 			Bootstrap: "Vnecoms_Megamenu/lib/bootstrap/js/bootstrap.min",
 			Colorbox: "Vnecoms_Megamenu/lib/colorbox/jquery.colorbox.min",
 			Fancybox: "Vnecoms_Megamenu/lib/fancybox/jquery.fancybox.pack",
 			FancyboxMouseWheel: "Vnecoms_Megamenu/lib/fancybox/jquery.mousewheel-3.0.6.pack"
 		}
 	},
    paths: {
        "bootstrap": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min"
    },
 	shim: {
        'bootstrap': {
            'deps': ['jquery']
        },
        'Vnecoms_Megamenu/lib/bootstrap/js/bootstrap.min': {
            'deps': ['jquery']
        },
        'Vnecoms_Megamenu/lib/bootstrap/js/bootstrap': {
            'deps': ['jquery']
        },
        'Vnecoms_Megamenu/lib/owl.carousel/owl.carousel': {
            'deps': ['jquery']
        },
        'Vnecoms_Megamenu/lib/owl.carousel/owl.carousel.min': {
        	'deps': ['jquery']
        },
        'Vnecoms_Megamenu/lib/fancybox/jquery.fancybox': {
            'deps': ['jquery']  
        },
        'Vnecoms_Megamenu/lib/fancybox/jquery.fancybox.pack': {
            'deps': ['jquery']  
        },
        'Vnecoms_Megamenu/lib/colorbox/jquery.colorbox': {
            'deps': ['jquery']  
        },
        'Vnecoms_Megamenu/lib/colorbox/jquery.colorbox.min': {
            'deps': ['jquery']  
        }
    }
 };