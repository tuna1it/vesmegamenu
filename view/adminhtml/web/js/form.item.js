/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'jquery/ui',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'mage/backend/validation'
], function ($) {
    'use strict';

    $.widget('mage.itemFormModal', {
        _create: function () {
            var itemModalForm = $('#item_modal_form');
            var widget = this;

            itemModalForm.mage('validation', {
                errorPlacement: function (error, element) {

                }
            }).on('highlight.validate', function (e) {
                var options = $(this).validation('option');
            });
            this.element.modal({
                type: 'slide',
                modalClass: 'item-form-modal-widget form-inline',
                title: $.mage.__('Add New Item'),
                opened: function () {
                   
                },
                closed: function () {

                },
                buttons: [{
                    text: '<i class="fa fa-download"></i> ' + $.mage.__('Save Item'),
                    class: 'action-primary',
                    click: function (e) {
                        if (!itemModalForm.valid()) {
                            return;
                        }

                        var thisButton = $(e.currentTarget);
                        thisButton.prop('disabled', true);

                        //make selected category_ids

                    }
                }]
            });
        }
    });

    return $.mage.itemFormModal;
});
