/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'jquery/ui',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'mage/backend/tree-suggest',
    'mage/backend/validation'
], function ($) {
    'use strict';

    var clearCategory = function () {
        $('#category_ids').find('option').each(function () {
            $('#category_ids-suggest').treeSuggest('removeOption', null, this);
        });
    };

    $.widget('mage.ubImportCategoriesDialog', {
        _create: function () {
            var widget = this;
            $('#category_ids').before($('<input>', {
                id: 'category_ids-suggest',
                placeholder: $.mage.__('start typing to search category')
            }));

            $('#category_ids-suggest').treeSuggest(this.options.suggestOptions)
                .on('suggestbeforeselect', function (event) {
                    //$(event.target).treeSuggest('close');
                });

            $.validator.addMethod('validate-category', function () {
                return $('#category_ids').val() || $('#category_ids-suggest').val() === '';
            }, $.mage.__('Choose existing category.'));

            var importCategoriesForm = $('#import_categories_form');
            importCategoriesForm.mage('validation', {
                errorPlacement: function (error, element) {
                    error.insertAfter(element.is('#category_ids') ?
                        $('#category_ids-suggest').closest('.mage-suggest') :
                        element);
                }
            }).on('highlight.validate', function (e) {
                var options = $(this).validation('option');
                if ($(e.target).is('#category_ids')) {
                    options.highlight($('#category_ids-suggest').get(0),
                        options.errorClass, options.validClass || '');
                }
            });

            this.element.modal({
                type: 'slide',
                modalClass: 'ub-import-categories-dialog form-inline',
                title: $.mage.__('Import Categories'),
                opened: function () {
                    $('#import_categories_messages').html('');
                    $('#category_ids-suggest').focus();
                },
                closed: function () {
                    var validationOptions = importCategoriesForm.validation('option');

                    //reset form fields
                    $('#category_ids-suggest').val('');

                    validationOptions.unhighlight($('#category_ids-suggest').get(0),
                        validationOptions.errorClass, validationOptions.validClass || '');
                    importCategoriesForm.validation('clearError');
                },
                buttons: [{
                    text: '<i class="fa fa-download"></i> ' + $.mage.__('Import Categories'),
                    class: 'action-primary',
                    click: function (e) {
                        if (!importCategoriesForm.valid()) {
                            return;
                        }

                        var thisButton = $(e.currentTarget);
                        thisButton.prop('disabled', true);

                        //make selected category_ids
                        var category_ids = '';
                        var parent_id = $('#parent_id').val();
                        $('#category_ids').find('option').each(function () {
                            if (category_ids.length){
                                category_ids += ',' + $(this).val();
                            } else {
                                category_ids += $(this).val();
                            }
                        });

                        //ajax request to import selected categories
                        $.ajax({
                            type: 'POST',
                            url: widget.options.saveUrl,
                            data: {
                                parent_id: parent_id,
                                category_ids: category_ids,
                                form_key: FORM_KEY
                            },
                            //dataType: 'json',
                            context: $('body')
                        }).success(function (rs) {
                            if (rs.success) {
                                //reset form fields
                                var $suggest = $('#category_ids-suggest');
                                $('#category_ids-suggest').val('');
                                $suggest.val('');
                                clearCategory();
                                //close modal
                                $(widget.element).modal('closeModal');

                                //expand menu items
                                if (!parseInt(parent_id)) {
                                    $('#ub-mega-menu-0').prepend(rs.menu_items);
                                } else {
                                    $("li[data-id='" + parent_id +"']").append(rs.menu_items);
                                }
                                $('.dd').trigger('change');

                                //alert message
                                $.mage.alert({title: rs.message});

                            } else {
                                //show errors
                                $('#import_categories_messages').html(rs.message);
                            }
                        }).complete(
                            function () {
                                thisButton.prop('disabled', false);
                            }
                        );
                    }
                }]
            });
        }
    });

    return $.mage.ubImportCategoriesDialog;
});
