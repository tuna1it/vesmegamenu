/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/form/components/insert-form'
], function (InsertForm) {
    'use strict';

    return InsertForm.extend({
        defaults: {
            itemId: 0,
            modules: {
                itemForm: 'item_form.item_form'
            },
            listens: {
                responseStatus: 'processResponseStatus'
            }
        },

        /*initialize: function()
        {
            return this._super().observe('responseStatus');
        },*/

        /**
         * Process response status.
         */
        processResponseStatus: function () {
            if (this.responseStatus()) {
               console.log('responseStatus');
            }
        }

    });
});
