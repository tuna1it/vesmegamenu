/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
], function (_, uiRegistry, ShowLeftBlock) {
    'use strict';

    return ShowLeftBlock.extend({
        defaults: {
            listens: {
                value: 'ShowFields'
            },
            filterPlaceholder: 'ns = ${ $.ns }, parentScope = ${ $.parentScope }'
        },

        /**
         * Initialize component.
         * @returns {Element}
         */
        initialize: function () {
            return this
                ._super()
                .ShowFields(this.initialValue);
        },

        /**
         *
         * @param {String} currentValue
         */
        onUpdate: function(currentValue) {
            //console.log(currentValue);
            this.ShowFields(currentValue);
            return this._super();
        },

        /**
         *
         * @param value
         */
        ShowFields: function(value) {
            var left_sidebar_width = this.filterPlaceholder + ', index=left_sidebar_width',
                left_sidebar_html = this.filterPlaceholder + ', index=left_sidebar_html';

            switch (value) {

                case '0':
                    this.changeVisible(left_sidebar_width, false);
                    this.changeVisible(left_sidebar_html, false);
                    break;

                case '1':
                    this.changeVisible(left_sidebar_width, true);
                    this.changeVisible(left_sidebar_html, true);
                    break;
            }

        },

        /**
         * Change visible
         *
         * @param {String} filter
         * @param {Boolean} visible
         */
        changeVisible: function (filter, visible) {
            uiRegistry.async(filter)(
                function (currentComponent) {
                    currentComponent.visible(visible);
                }
            );
        },

        /** @inheritdoc */
        filter: function () {
            this._super();
            this.disableSelect();
        },

        /**
         * Disables select if there's no regions/states
         *
         * @returns {*} instance - Chainable
         */
        disableSelect: function () {
            var empty = !this.options().length;

            this.disabled(empty);

            if (empty) {
                this.error('');
            }

            return this;
        }

    });
});
