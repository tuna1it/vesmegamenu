/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
], function (_, uiRegistry, CustomSelect) {
    'use strict';

    return CustomSelect.extend({
        defaults: {
            listens: {
                value: 'changeTypeContent'
            },
            filterPlaceholder: 'ns = ${ $.ns }, parentScope = ${ $.parentScope }'
        },

        /**
         * Initialize component.
         * @returns {Element}
         */
        initialize: function () {
            return this
                ._super()
                .changeTypeContent(this.initialValue);
        },

        /**
         *
         * @param {String} currentValue
         */
        onUpdate: function(currentValue) {
            console.log(currentValue);
            this.changeTypeContent(currentValue);
            return this._super();
        },

        /**
         *
         * @param value
         */
        changeTypeContent: function(value) {
            var st = this.filterPlaceholder + ', index=static_blocks',
                ch = this.filterPlaceholder + ', index=content_html',
                megacol = this.filterPlaceholder + ', index=mega_cols';

            switch (value) {

                case 'static-blocks':
                    this.changeVisible(st, true);
                    this.changeVisible(ch, false);
                    this.changeVisible(megacol, false);
                    break;

                case 'content-html':
                    this.changeVisible(st, false);
                    this.changeVisible(ch, true);
                    this.changeVisible(megacol, false);
                    break;

                case 'child-items':
                    this.changeVisible(st, false);
                    this.changeVisible(ch, false);
                    this.changeVisible(megacol, true);
                    break;

                case 'category':
                    this.changeVisible(st, false);
                    this.changeVisible(ch, false);
                    this.changeVisible(megacol, true);
                    break;

                case 'dynamic-content':
                    this.changeVisible(st, false);
                    this.changeVisible(ch, false);
                    this.changeVisible(megacol, false);
                    break;
            }

        },

        /**
         * Change visible
         *
         * @param {String} filter
         * @param {Boolean} visible
         */
        changeVisible: function (filter, visible) {
            uiRegistry.async(filter)(
                function (currentComponent) {
                    currentComponent.visible(visible);
                }
            );
        },

        /** @inheritdoc */
        filter: function () {
            this._super();
            this.disableSelect();
        },

        /**
         * Disables select if there's no regions/states
         *
         * @returns {*} instance - Chainable
         */
        disableSelect: function () {
            var empty = !this.options().length;

            this.disabled(empty);

            if (empty) {
                this.error('');
            }

            return this;
        }



    });
});
