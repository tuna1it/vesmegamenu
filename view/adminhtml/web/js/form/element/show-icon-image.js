/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
], function (_, uiRegistry, ShowIcon) {
    'use strict';

    return ShowIcon.extend({
        defaults: {
            listens: {
                value: 'ShowIconImage'
            },
            filterPlaceholder: 'ns = ${ $.ns }, parentScope = ${ $.parentScope }'
        },

        /**
         * Initialize component.
         * @returns {Element}
         */
        initialize: function () {
            return this
                ._super()
                .ShowIconImage(this.initialValue);
        },

        /**
         *
         * @param {String} currentValue
         */
        onUpdate: function(currentValue) {
            //console.log(currentValue);
            this.ShowIconImage(currentValue);
            return this._super();
        },

        /**
         *
         * @param value
         */
        ShowIconImage: function(value) {
            var icon_image = this.filterPlaceholder + ', index=icon_image';

            switch (value) {

                case '0':
                    this.changeVisible(icon_image, false);
                    break;

                case '1':
                    this.changeVisible(icon_image, true);
                    break;
            }

        },

        /**
         * Change visible
         *
         * @param {String} filter
         * @param {Boolean} visible
         */
        changeVisible: function (filter, visible) {
            uiRegistry.async(filter)(
                function (currentComponent) {
                    currentComponent.visible(visible);
                }
            );
        },

        /** @inheritdoc */
        filter: function () {
            this._super();
            this.disableSelect();
        },

        /**
         * Disables select if there's no regions/states
         *
         * @returns {*} instance - Chainable
         */
        disableSelect: function () {
            var empty = !this.options().length;

            this.disabled(empty);

            if (empty) {
                this.error('');
            }

            return this;
        }

    });
});
