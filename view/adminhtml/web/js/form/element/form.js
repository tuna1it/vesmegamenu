/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/components/fieldset',
    'Magento_Ui/js/lib/view/utils/async',
    'Magento_Ui/js/modal/modal'
], function (_, uiRegistry, fieldset, async) {
    'use strict';

    return fieldset.extend({

        /*eslint-disable no-unused-vars*/
        /**
         * Initialize element
         *
         * @returns {Abstract} Chainable
         */
        initialize: function (elems, position) {
            var obj = this;

            this._super();

            async.async('#megamenu-form-tab-items', document.getElementById('container'), function (node) {
                obj.enableDisableFields();
            });

            console.log(elems);

            return this;
        }

        /*eslint-enable no-unused-vars*/
        /*eslint-disable lines-around-comment*/

    });
});
