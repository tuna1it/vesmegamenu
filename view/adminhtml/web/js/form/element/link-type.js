/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
], function (_, uiRegistry, LinkSelect) {
    'use strict';

    return LinkSelect.extend({
        defaults: {
            listens: {
                value: 'changeLinkType'
            },
            filterPlaceholder: 'ns = ${ $.ns }, parentScope = ${ $.parentScope }'
        },

        /**
         * Initialize component.
         * @returns {Element}
         */
        initialize: function () {
            return this
                ._super()
                .changeLinkType(this.initialValue);
        },

        /**
         *
         * @param {String} currentValue
         */
        onUpdate: function(currentValue) {
            //console.log(currentValue);
            this.changeLinkType(currentValue);
            return this._super();
        },

        /**
         *
         * @param value
         */
        changeLinkType: function(value) {
            var link = this.filterPlaceholder + ', index=link',
                cms = this.filterPlaceholder + ', index=cms_page',
                category = this.filterPlaceholder + ', index=category_id',
                number_product = this.filterPlaceholder + ', index=show_number_product';

            switch (value) {

                case 'custom-link':
                    this.changeVisible(link, true);
                    this.changeVisible(cms, false);
                    this.changeVisible(category, false);
                    this.changeVisible(number_product, false);
                    break;

                case 'category-link':
                    this.changeVisible(link, false);
                    this.changeVisible(cms, false);
                    this.changeVisible(category, true);
                    this.changeVisible(number_product, true);
                    break;

                case 'cms-link':
                    this.changeVisible(link, false);
                    this.changeVisible(cms, true);
                    this.changeVisible(category, false);
                    this.changeVisible(number_product, false);
                    break;
            }

        },

        /**
         * Change visible
         *
         * @param {String} filter
         * @param {Boolean} visible
         */
        changeVisible: function (filter, visible) {
            uiRegistry.async(filter)(
                function (currentComponent) {
                    currentComponent.visible(visible);
                }
            );
        },

        /** @inheritdoc */
        filter: function () {
            this._super();
            this.disableSelect();
        },

        /**
         * Disables select if there's no regions/states
         *
         * @returns {*} instance - Chainable
         */
        disableSelect: function () {
            var empty = !this.options().length;

            this.disabled(empty);

            if (empty) {
                this.error('');
            }

            return this;
        }



    });
});
