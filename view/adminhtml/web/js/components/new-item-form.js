/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/form/form'
], function ($, Form) {
    'use strict';

    return Form.extend({
        defaults: {
            listens: {
            },
            modules: {
                itemForm: 'item_form.item_form'
            }
        }

    });

});
