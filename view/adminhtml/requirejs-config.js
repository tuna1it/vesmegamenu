/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
	"shim": {
		"nestable": {
			"deps": ["jquery"]
		}
	},
	map: {
		"*": {
			"nestable": 		"Vnecoms_Megamenu/js/jquery.nestable",
			"importCategories": "Vnecoms_Megamenu/js/import.categories",
			"itemFormModal": 	"Vnecoms_Megamenu/js/form.item",
			"megabrowser":      "Vnecoms_Megamenu/js/browser"
		}
	},
	"deps": [
		"js/theme",
		"mage/adminhtml/globals"
    ]
};