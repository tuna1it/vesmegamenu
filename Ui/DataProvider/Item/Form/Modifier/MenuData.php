<?php


namespace Vnecoms\Megamenu\Ui\DataProvider\Item\Form\Modifier;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory;
use Vnecoms\Megamenu\Model\ItemFactory;
use Vnecoms\Megamenu\Model\MenuFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\MultiSelect;
use Magento\Ui\Component\Form\Field;

class MenuData extends AbstractModifier
{
    /**
     * @var \Vnecoms\Megamenu\Model\ResourceModel\Item\Collection
     */
    protected $collection;


    /** @var ItemFactory  */
    protected $itemFactory;

    /** @var MenuFactory  */
    protected $menuFactory;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Vnecoms\Megamenu\Api\ItemRepositoryInterface
     */
    private $itemRepository;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * MenuData constructor.
     * @param CollectionFactory $itemCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param ArrayManager $arrayManager
     * @param ItemFactory $itemFactory
     * @param MenuFactory $menuFactory
     * @param \Vnecoms\Megamenu\Api\ItemRepositoryInterface $itemRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        CollectionFactory $itemCollectionFactory,
        DataPersistorInterface $dataPersistor,
        ArrayManager $arrayManager,
        ItemFactory $itemFactory,
        MenuFactory $menuFactory,
        \Vnecoms\Megamenu\Api\ItemRepositoryInterface $itemRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->arrayManager = $arrayManager;
        $this->collection = $itemCollectionFactory->create();
        $this->itemFactory = $itemFactory;
        $this->menuFactory = $menuFactory;
        $this->itemRepository = $itemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Add components or elements by this
     *
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        //Add select menu here
        $this->meta = $meta;
        $meta = $this->addMenuSelectionField($meta);
        return $meta;
    }

    /**
     * @param array $meta
     * @return array
     */
    protected function addMenuSelectionField(array $meta)
    {
        $fieldCode = 'menu_id';

        $meta['item_settings'] = [
                'arguments' => [
                    'data' => [
                        'config' => [

                        ]
                    ],
                ],
                'children' => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Parent Menu'),
                                    'componentType' => Field::NAME,
                                    'formElement' => Select::NAME,
                                    'dataType' => Text::NAME,
                                    'options' => $this->_getMenuOptions(),
                                    'visible' => true,
                                    'disabled' => true,
                                    'sortOrder' => 40,
                                    'dataScope' => $fieldCode
                                ],
                            ],
                        ],
                    ]
                ]
        ];

        return $meta;
    }

    protected function _getMenuOptions()
    {
        $menus = [];
        $menuCollection = $this->menuFactory->create()->getCollection();

        if ($menuCollection) {
            /** @var \Vnecoms\Megamenu\Model\Menu $menu */
            foreach ($menuCollection as $menu) {

                $menus[] = [
                    'label' => $menu->getTitle(),
                    'value' => $menu->getId(),
                ];
            }
        }

        return $menus;
    }

    /**
     * Modify form data before form creation
     * @param array $data
     * @return array|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function modifyData(array $data)
    {
        return $data;
    }
}
