<?php

namespace Vnecoms\Megamenu\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Vnecoms\Megamenu\Block\Adminhtml\Menu\Grid\Renderer\Action\UrlBuilder;

/**
 * Class MenuActions
 * @package Vnecoms\Megamenu\Ui\Component\Listing\Columns
 */
class MenuActions extends Column
{
    const MENU_URL_PATH_EDIT       = 'megamenuadmin/menu/edit';
    const MENU_URL_PATH_DELETE     = 'megamenuadmin/menu/delete';
    const MENU_ITEMS_URL_PATH      = 'megamenuadmin/item/index';
    const MENU_URL_PATH_DUPLICATE  = 'megamenuadmin/menu/duplicate';

    /** @var UrlInterface  */
    protected $urlBuilder;

    /** @var UrlBuilder  */
    protected $actionUrlBuilder;

    /** @var string  */
    protected $editUrl;

    /**
     * BannerActions constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param UrlBuilder $actionUrlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        UrlBuilder $actionUrlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::MENU_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                // $storeId = $this->context->getFilterParam('store_id');
                // $store = $this->context->getRequestParam('store');
                //echo $item['store_id']; exit;

                $name = $this->getData('name');
                if (isset($item['menu_id'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl,
                            ['menu_id' => $item['menu_id']]),
                        'label' => __('Edit')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::MENU_URL_PATH_DELETE,
                            ['menu_id' => $item['menu_id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete ${ $.$data.title }'),
                            'message' => __('Are you sure you wan\'t to delete a ${ $.$data.title } record?')
                        ]
                    ];
                    
                }

                if (isset($item['identifier'])) {
                    $item[$name]['listitem'] = [
                        'href' => $this->urlBuilder->getUrl(self::MENU_ITEMS_URL_PATH.'/menu_id/'.$item['menu_id'], null),
                        'label' => __('Manage Menu Items')
                    ];
                }

                $item[$name]['duplicate'] = [
                    'href' => $this->urlBuilder->getUrl(self::MENU_URL_PATH_DUPLICATE, ['menu_id' => $item['menu_id']]),
                    'label' => __('Make Duplicate')
                ];

            }
        }
        return $dataSource;
    }
}