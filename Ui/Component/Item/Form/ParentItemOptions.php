<?php

namespace Vnecoms\Megamenu\Ui\Component\Item\Form;

use Magento\Framework\Data\OptionSourceInterface;
use Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory as ItemCollectionFactory;
use Magento\Framework\App\RequestInterface;
use Vnecoms\Megamenu\Model\Item as ItemModel;
use Magento\Framework\Registry;
use Magento\Framework\App\Request\DataPersistorInterface;
use Vnecoms\Megamenu\Helper\Data as MenuHelper;
use Magento\Framework\Convert\DataObject;

/**
 * Options tree for "Items Parent" field
 */
class ParentItemOptions implements OptionSourceInterface
{
    /**
     * @var \Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $itemsTree;

    /** @var  \Vnecoms\Megamenu\Model\Item */
    protected $itemModel;

    /** @var Registry  */
    protected $_coreRegistry;

    /** @var  array */
    protected $_options;

    /** @var MenuHelper  */
    protected $_helper;

    /**
     * @var \Magento\Framework\Convert\DataObject
     */
    private $objectConverter;

    /**
     * @var \Vnecoms\Megamenu\Api\ItemRepositoryInterface
     */
    private $itemRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * ItemOptions constructor.
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param Registry $registry
     * @param RequestInterface $request
     * @param DataPersistorInterface $dataPersistor
     * @param MenuHelper $helper
     * @param ItemModel $model
     * @param DataObject $objectConverter
     * @param \Vnecoms\Megamenu\Api\ItemRepositoryInterface $itemRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array $data
     */
    public function __construct(
        ItemCollectionFactory $itemCollectionFactory,
        Registry $registry,
        RequestInterface $request,
        DataPersistorInterface $dataPersistor,
        MenuHelper $helper,
        ItemModel $model,
        DataObject $objectConverter,
        \Vnecoms\Megamenu\Api\ItemRepositoryInterface $itemRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = []
    ) {
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->_coreRegistry = $registry;
        $this->request = $request;
        $this->dataPersistor = $dataPersistor;
        $this->itemModel = $model;
        $this->_helper = $helper;
        $this->objectConverter = $objectConverter;
        $this->itemRepository = $itemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Array items parent
     *
     * @return array
     */
    public function toOptionArray()
    {

        if (!$this->_options) {
            //$menuId = $this->_coreRegistry->registry('current_menu_id');
            $menuId = $this->dataPersistor->get('current_menu_id');

            if (isset($menuId)) {

                $collection = $this->itemModel
                    ->getCollection()
                    ->addFieldToSelect(['item_id', 'parent_id', 'title'])
                    ->addFieldToFilter('menu_id', $menuId)
                    ->addOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
                    ->addOrder('title', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
                    ->load();

                $tree = $this->_helper->buildTree(0, $collection, 99, 'title', 'item_id', 'parent_id', true);

                if (is_object($tree)) {
                   return;
                } elseif (is_array($tree)) {

                    foreach($tree as $k => $v) {
                        $this->_options[$k]['value'] = $k;
                        $this->_options[$k]['label'] = $v;
                    }
                }
                $this->_options[0] = ['value' => '0', 'label' => ' -- NONE -- '];

            }
        }
        return $this->_options;
    }

    /**
     * @return mixed
     */
    public function getMenuId()
    {
        $menuId = $this->_coreRegistry->registry('current_menu_id');
        return $menuId;
    }

}
