<?php

namespace Vnecoms\Megamenu\Helper;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Api\Filter;
use \Magento\Framework\Api\Search\FilterGroup;
use \Magento\Framework\Api\SearchCriteriaInterface;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Catalog\Model\CategoryLinkRepository;
use Vnecoms\Megamenu\Api\Data\MenuInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 * @package Vnecoms\Megamenu\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * Store manager.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * category collection factory.
     *
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollectionFactory;

    /**
     * item collection factory.
     *
     * @var \Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * @var \Magento\Framework\App\ScopeResolverInterface
     */
    private $scopeResolver;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /** @var  \Vnecoms\Megamenu\Model\MenuFactory */
    protected $menuFactory;

    /** @var  \Vnecoms\Megamenu\Model\ItemFactory */
    protected $itemFactory;

    /** @var \Magento\Customer\Model\Group  */
    protected $_groupCollection;


    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     * @param StoreManagerInterface $storeManager
     * @param \Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory $itemCollectionFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param \Vnecoms\Megamenu\Model\MenuFactory $menuFactory
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param \Magento\Framework\App\ScopeResolverInterface $scopeResolver
     * @param \Magento\Customer\Model\Group $groupManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Vnecoms\Megamenu\Model\MenuFactory $menuFactory,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        \Magento\Framework\App\ScopeResolverInterface $scopeResolver,
        \Magento\Customer\Model\Group $groupManager
    ){
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->_storeManager = $storeManager;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->menuFactory = $menuFactory;
        $this->itemFactory = $itemFactory;
        $this->scopeResolver = $scopeResolver;
        $this->_groupCollection = $groupManager;
    }

    /**
     * get Base Url Media.
     *
     * @param string $path   [description]
     * @param bool   $secure [description]
     *
     * @return string [description]
     */
    public function getBaseUrlMedia($path = '', $secure = false)
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, $secure) . $path;
    }

    /*
     * Get item collection
     *
     * @param void
     * @return array
     */
    public function getItemsCollection()
    {
        $itemCollection = $this->_itemCollectionFactory->create()
            ->addFieldToSelect('*')
            ->setOrder('item_id', 'ASC')
            ->load()
            ->toArray();

        return $itemCollection;
    }

    /**
     * get categories array.
     *
     * @return array
     */
    public function getCategoriesArray()
    {
        $categoriesArray = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->load()
            ->toArray();

        $categories = array();
        foreach ($categoriesArray as $categoryId => $category) {
            if (isset($category['name']) && isset($category['level'])) {
                $categories[] = array(
                    'label' => $category['name'],
                    'level' => $category['level'],
                    'value' => $categoryId,
                );
            }
        }

        return $categories;
    }


    /**
     * Check extension enable
     *
     * @param: void
     * @return mixed
     */
    public function isExtensionEnable(){
        return  $this->_scopeConfig->getValue('megamenu/general/is_enable', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * Build tree items function
     *
     * @param int $rootId
     * @param $models
     * @param int $maxLevel
     * @param string $labelField
     * @param string $keyField
     * @param string $parentField
     * @param bool|false $isFilter
     * @param bool|false $countProduct
     * @return array
     */
    public function buildTree($rootId = 0, $models, $maxLevel = 99, $labelField = "name", $keyField = "entity_id", $parentField = "parent_id", $isFilter = false, $countProduct = false)
    {
        //grouping
        @$children = [];
        /** @var \Vnecoms\Megamenu\Model\Item $model */
        foreach ($models as $model) {
            $pt = $model->getData($parentField);
            $list = (isset($children[$pt]) && $children[$pt]) ? $children[$pt] : [];
            array_push($list, $model);
            $children[$pt] = $list;
        }

        //build tree
        $lists = $this->_toTree($rootId, '', [], $children, $maxLevel, 0, $labelField, $keyField, $parentField, $countProduct);


        if ($isFilter) {
            $outputs = ['0' => __('All')];
        }

        foreach ($lists as $id => $list) {
            $lists[$id]->$labelField = $lists[$id]->$labelField;
            $outputs[$lists[$id]->getData($keyField)] = $lists[$id]->$labelField;
        }
        return $outputs;
    }

    /**
     * Generate tree items
     *
     * @param $id
     * @param $indent
     * @param $list
     * @param $children
     * @param int $maxLevel
     * @param int $level
     * @param $label
     * @param $key
     * @param $parent
     * @param bool|false $countProduct
     * @return mixed
     */
    protected function _toTree($id, $indent, $list, &$children, $maxLevel = 99, $level = 0, $label, $key, $parent, $countProduct = false)
    {
        if (@$children[$id] && $level <= $maxLevel) {

            foreach ($children[$id] as $v) {
                $id = $v->getData($key);

                $pre = '';
                $spacer = '--- ';
                if ($v->getData($parent) == 0) {
                    $txt = $v->getData($label);
                } else {
                    $txt = $pre . $v->getData($label);
                }

                $list[$id] = $v;
                $list[$id]->$label = "{$indent}{$txt}";

                if($countProduct) {
                    $list[$id]->$label .= " (".$v->getProductCount().")";
                }

                //$list[$id]->children = count(@$children[$id]);
                $list = $this->_toTree($id, $indent . $spacer, $list, $children, $maxLevel, $level + 1, $label, $key, $parent, $countProduct);
            }
        }
        return $list;
    }

    /**
     * Get config from settings
     *
     * @param null $key
     * @param array $data
     * @return mixed|null
     */
    public function getConfigValue($key = null, $data = [])
    {
        $scopeCode = $this->scopeResolver->getScope()->getCode();

        $currentStoreCode = $this->_storeManager->getStore()->getCode();
        $currentWebsiteCode = $this->_storeManager->getWebsite()->getCode();

        if ($scopeCode == $currentStoreCode) {
            $scope  = ScopeInterface::SCOPE_STORES;
        } elseif ($scopeCode == $currentWebsiteCode) {
            $scope  = ScopeInterface::SCOPE_WEBSITES;
        } else {
            $scope = 'default';
            //$scopeId = 0;
            $scopeCode = '';
        }

        $sections = ['megamenu'];
        $value = null;
        if (isset($data[$key])) {
            $value = $data[$key];
        } else {
            foreach ($sections as $section) {
                $menus = $this->_scopeConfig->getValue($section, $scope, $scopeCode);
                if ($menus) {
                    foreach ($menus as $configs) {
                        if (isset($configs[$key])) {
                            $value = $configs[$key];
                            break;
                        }
                    }
                }
                if ($value)
                    break;
            }
        }

        return $value;
    }

    /**
     * @param $text
     * @param $length
     * @param string $replacer
     * @param bool $is_striped
     * @return string
     */
    public function subString($text, $length, $replacer = '...', $is_striped = true) {
        if((int)$length==0) return $text;
        $text = ($is_striped == true) ? strip_tags($text) : $text;
        if (strlen($text) <= $length) {
            return $text;
        }
        $text = substr($text, 0, $length);
        $pos_space = strrpos($text, ' ');
        return substr($text, 0, $pos_space) . $replacer;
    }

    public function getCustomerGroups()
    {
        $data_array = array();

        $customer_groups = $this->_groupCollection->getCollection();

        foreach ($customer_groups as $item_group) {
            $data_array[] =  array('value' => $item_group->getId(), 'label' => $item_group->getCode());
        }

        return $data_array;

    }

    /**
     * @param $menuId
     * @return mixed
     */
    public function getMenuById($menuId){
        $storeId = $this->_storeManager->getStore()->getId();
        $collection = $this->menuFactory->create()->getCollection()
            ->addFieldToSelect(['menu_id', 'title', 'identifier', 'animation_type', 'is_active'])
            ->addFieldToFilter('menu_id', ['eq' => $menuId])
            ->addFieldToFilter('is_active', ['eq' => \Vnecoms\Megamenu\Model\Menu::STATUS_ENABLED])
            ->addStoreFilter($storeId, true)
            ->addOrder('menu_id', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
            ->getFirstItem();
        return $collection;
    }

    /**
     * @param $identifier
     * @return mixed
     */
    public function getMenuByIdentifier($identifier){
        $storeId = $this->_storeManager->getStore()->getId();
        $collection = $this->menuFactory->create()->getCollection()
            ->addFieldToSelect(['menu_id', 'title', 'identifier', 'animation_type', 'is_active'])
            ->addFieldToFilter('identifier', ['eq' => $identifier])
            ->addFieldToFilter('is_active', ['eq' => \Vnecoms\Megamenu\Model\Menu::STATUS_ENABLED])
            ->addStoreFilter($storeId, true)
            ->addOrder('menu_id', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
            ->getFirstItem();
        return $collection;
    }

    /**
     * Get all items of menu by id
     *
     * @param $menuId
     * @return array
     */
    public function getMenuItems($menuId) {
        $items = null;
        $menuItems = [];
        if ($menuId) {
            $collection = $this->itemFactory->create()->getCollection()
                ->addFieldToFilter('menu_id', ['eq' => $menuId])
                ->addFieldToFilter('is_active', ['eq' => \Vnecoms\Megamenu\Model\Item::STATUS_ENABLED])
                ->addOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
                ->load()
                ->getData();
            $items = $collection;
        }


        if(!empty($items)){
            foreach ($items as $k => $v) {
                $v['htmlId'] = 'vesitem-' . $v['item_id'] . time() . rand();
                $menuItems[$v['item_id']] = $v;
            }
        }

        return $menuItems;

    }

    /**
     * Get all items of menu by id
     *
     * @param $menuId
     * @return \Vnecoms\Megamenu\Model\ResourceModel\Item\Collection
     */
    public function getMenuItems2($menuId) {
        $items = null;
        if ($menuId) {
            $collection = $this->itemFactory->create()->getCollection()
                ->addFieldToFilter('menu_id', ['eq' => $menuId])
                ->addFieldToSelect(['item_id','parent_id'])
                ->addFieldToFilter('is_active', ['eq' => \Vnecoms\Megamenu\Model\Item::STATUS_ENABLED])
                ->addOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
            $items = $collection;
        }

        return $items;
    }

}
