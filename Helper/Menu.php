<?php

namespace Vnecoms\Megamenu\Helper;

use Vnecoms\Megamenu\Model\Item\Image as ImageModel;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Cms\Model\Block as ModelBlock;
use Vnecoms\Megamenu\Model\Processor;
use Vnecoms\Megamenu\Model\ItemFactory;

/**
 * Menu Helper
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class Menu extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Design package instance
     *
     * @var \Magento\Framework\View\DesignInterface
     */
    protected $_design;

    /**
     * @var \Vnecoms\Megamenu\Model\Menu
     */
    protected $menu;

    protected $_cats;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Menu factory
     *
     * @var \Vnecoms\Megamenu\Model\MenuFactory
     */
    protected $menuFactory;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /** @var UrlInterface  */
    protected $urlInterface;

    /** @var  LayoutInterface */
    protected $layoutInterface;

    /** @var  ModelBlock\ */
    protected $modelBlock;

    /** @var  \Vnecoms\Megamenu\Model\Processor */
    protected $processor;

    /** @var  \Vnecoms\Megamenu\Model\ItemFactory */
    protected $itemFactory;

    /**
     * @var ImageModel
     */
    protected $imageModel;

    protected $children = [];

    protected $items = [];

    protected $active_tree = [];


    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Magento\Cms\Model\Template\Filter
     */
    protected $_filter;

    protected $_url;

    /**
     * Menu constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Vnecoms\Megamenu\Model\Menu $menu
     * @param \Magento\Framework\View\DesignInterface $design
     * @param \Vnecoms\Megamenu\Model\MenuFactory $menuFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param UrlInterface $urlInterface
     * @param LayoutInterface $layout
     * @param ModelBlock $modelBlock
     * @param Processor $processor
     * @param ItemFactory $itemFactory
     * @param ImageModel $imageModel
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Cms\Model\Template\Filter $filter
     * @param \Magento\Framework\Url $url
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Vnecoms\Megamenu\Model\Menu $menu,
        \Magento\Framework\View\DesignInterface $design,
        \Vnecoms\Megamenu\Model\MenuFactory $menuFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        UrlInterface $urlInterface,
        LayoutInterface $layout,
        ModelBlock $modelBlock,
        Processor $processor,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        ImageModel $imageModel,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Cms\Model\Template\Filter $filter,
        \Magento\Framework\Url $url
    ) {
        $this->messageManager = $messageManager;
        $this->menu = $menu;
        $this->_design = $design;
        $this->menuFactory = $menuFactory;
        $this->_storeManager = $storeManager;
        $this->_localeDate = $localeDate;
        $this->_escaper = $escaper;
        $this->resultPageFactory = $resultPageFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->urlInterface = $urlInterface;
        $this->layoutInterface = $layout;
        $this->modelBlock = $modelBlock;
        $this->processor = $processor;
        $this->itemFactory = $itemFactory;
        $this->imageModel = $imageModel;
        $this->_filterProvider  = $filterProvider;
        $this->_filter          = $filter;
        $this->_url             = $url->getCurrentUrl();

        parent::__construct($context);
    }


    /**
     * @param $str
     * @return string
     */
    public function filter($str)
    {
        $html = $this->_filterProvider->getPageFilter()->filter($str);
        return $html;
    }

    /**
     * Restructure tree items from collection
     * @param \Vnecoms\Megamenu\Model\ResourceModel\Item\Collection $items
     * @return array
     */
    public function rebuildStructure($items)
    {
        $structure = [];

        if (!is_array($items) && empty($items)) {
            return '';
        } elseif (is_object($items) && $items->count() > 0) {
            $itemsArray = $items->toArray(); // 2 key TotalRecords : items
            //var_dump($itemsArray);exit;
            if (is_array($itemsArray['items'])) {
                $structure = $this->buildTree($itemsArray['items']);
            }
        }
        //echo"<pre>";var_dump($structure);exit;
        return $structure;
    }

    /**
     * Build flat tree
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    public function buildTree(array &$elements) {
        $map = [
            0 => ['children' => []]
        ];

        foreach ($elements as &$element) {
            $element['children'] = [];
            $map[$element['item_id']] = &$element;
        }

        foreach ($elements as &$element) {
            $map[$element['parent_id']]['children'][] = &$element;
        }

        return $map[0]['children'];
    }

    /**
     * Get all data parent and child of item. Ex item id = 1 with all child
     * @param array $data
     * @param array $itemBuild
     * @param $menuItems
     */
    public function renderMenuItemData($data = [] , $itemBuild = [], $menuItems){
        //var_dump($data);
        //echo "<br/>";
        //var_dump($menuItems);exit;
        $data_id = isset($data['item_id'])? $data['item_id'] : 0;
        //echo $data_id;exit;
        $itemBuild = isset($menuItems[$data_id]) ? $menuItems[$data_id] : [];
        $children = [];
        if(isset($data['children']) && count($data['children']>0)){
            foreach ($data['children'] as $k => $v) {
                $children[] = $this->renderMenuItemData($v, $itemBuild, $menuItems);
            }
        }
        $itemBuild['children'] = $children;
        //var_dump($itemBuild);exit;
        return $itemBuild;
    }


    /**
     * @param $item
     * @param int $level
     * @param int $x
     * @param bool $listTag
     * @return string
     */
    public function drawItem($item, $level = 0, $x = 0, $listTag = true)
    {
        try{
            $hasChildren = false;
            $class = $style = $attr = '';
            //if(isset($item['class'])) $class = $item['class'];
            if(!isset($item['is_active'])) return;
            if(isset($item['children']) && count($item['children'])>0) $hasChildren = true;

            $class .= ' nav-item ui-menu-item level' . $level . ' nav-' . $x;
            if($item['align'] == '1'){
                $class .= ' submenu-left';
            }elseif($item['align'] == '2'){
                $class .= ' submenu-right';
            }elseif($item['align'] == '3'){
                $class .= ' submenu-alignleft';
            }elseif($item['align'] == '4'){
                $class .= ' submenu-alignright';
            }

            // Group Childs Item
            if($item['is_group']){
                $class .= ' subgroup ';
            }else{
                $class .= ' subhover ';
            }

            if($item['content_type'] == 'dynamic-content') $class .= ' subdynamic';

            if($level==0){
                $class .=' dropdown level-top';
            }else{
                $class .=' dropdown-submenu';
            }
            /*$class .= ' '.$item['font_classes'];*/

            // Custom Link, Category Link
            $href = '';
            if($item['link_type'] == 'custom-link'){
                $href = $item['link'];
            }elseif($item['link_type'] == 'category-link'){
                $cats = $this->getAllCategory();
                foreach ($cats as $_cat) {
                    if($_cat->getId() == $item['category_id']){
                        $href = $_cat->getUrl();
                        break;
                    }
                }
            }
            $link = $this->filter($href);
            $link = trim($link);
            if($this->endsWith($link, '/')){
                $link = substr_replace($link, "", -1);
            }
            $currentUrl = trim($this->_url);
            $currentUrl = $this->filter($currentUrl);
            if($this->endsWith($currentUrl, '/')){
                $currentUrl = substr_replace($currentUrl, "", -1);
            }
            if($link == $currentUrl && ($href != '' && $href!='#')){
                $class .= ' active';
            }

            if($listTag){
                if($class!='') $class = 'class="' . $class . '"';
                $html = '<li id="item_' . $item['item_id'] . '" ' . $class . ' ' . $style . ' ' . $attr . ' role="presentation">';
            }else{
                if(isset($item['dynamic'])){
                    $class .= ' dynamic-item '.$item['htmlId'];
                }
                if($class!='') $class = 'class="' . $class . '"';
                $html = '<div ' . $class . ' ' . $style . ' ' . $attr . '>';
            }

            if(!isset($item['dynamic'])) $html .= $this->drawAnchor($item);

            $tChildren = false;
            if($item['content_type'] == 'category'){
                $catChildren = $this->getTreeCategories($item['category_id']);
                if($catChildren) $tChildren = true;
            }
            if(($item['show_footer'] && $item['footer_html']!='') || ($item['show_header'] && $item['header_html']!='') ||  ($item['show_left_sidebar'] && $item['left_sidebar_html']!='') || ($item['show_right_sidebar'] && $item['right_sidebar_html']!='') || ($item['show_content'] && ((($item['content_type'] == 'child-items' || $item['content_type'] == 'dynamic-content') && (isset($item['children']) && count($item['children'])>0)) || ($item['content_type'] == 'content-html' && $item['content_html']!=''))) || ($item['content_type'] == 'category' && $tChildren) ){
                $level++;
                $subClass = $subStyle = $subAttr = '';

                if($item['sub_width']!='') $subStyle .= 'width:'.$item['sub_width'].'; ';
                //ADD color, background here soon

                if(isset($item['dynamic'])){
                    $subClass .= ' content-wrap';
                }

                // Div wrap content child
                if(!isset($item['dynamic'])){
                    $subClass .= ' submenu ';
                    if($item['is_group']){
                        $subClass .= ' dropdown-mega';
                    }else{
                        $subClass .= ' dropdown-menu';
                    }
                }

                // Add attribute to html for custom animation
                if(isset($item['animation_in'])){
                    $subClass .= ' animated ';
                    $subClass .= $item['animation_in'];
                    if($item['animation_in']){
                        $subAttr .= ' data-animation-in="' . $item['animation_in'] . '"';
                    }
                    if($item['animation_time']){
                        $subStyle .= ' animation-duration: ' . $item['animation_time'] . 's; -webkit-animation-duration: ' . $item['animation_time'] . 's;';
                    }
                }


                if($subClass!='') $subClass = 'class="' . $subClass . '"';
                if($subStyle!='') $subStyle = 'style="' . $subStyle . '"';

                if(!isset($item['dynamic']))
                    $html .= '<div ' . $subClass . ' ' . $subAttr . ' ' . $subStyle . '>';

                // TOP BLOCK
                if($item['show_header'] && $item['header_html']!=''){
                    $html .= '<div class="megamenu-header">' . $this->filter($item['header_html']) . '</div>';
                }

                if($item['show_left_sidebar'] || $item['show_content'] || $item['show_right_sidebar']){

                    if(!isset($item['dynamic'])){
                        $html .= '<div class="content-wrap">';
                    }else{
                        $html .= '<div ' . $subClass . ' ' . $subAttr . ' ' . $subStyle . '>';
                    }

                    $left_sidebar_width = isset($item['left_sidebar_width'])?$item['left_sidebar_width']:0;
                    $content_width = $item['content_width'];
                    $right_sidebar_width = isset($item['right_sidebar_width'])?$item['right_sidebar_width']:0;

                    // LEFT SIDEBAR BLOCK
                    if ($item['show_left_sidebar'] && $item['left_sidebar_html']!='') {
                        if($left_sidebar_width) $left_sidebar_width = 'style="width:'.$left_sidebar_width.'"';

                        $html .= '<div class="megamenu-sidebar left-sidebar" '.$left_sidebar_width.'>'.$this->filter($item['left_sidebar_html']).'</div>';
                    }
                    // MAIN CONTENT BLOCK
                    if ($item['show_content'] && ((($item['content_type'] == 'child-items' || $item['content_type'] == 'dynamic-content') && $hasChildren) || $item['content_type'] == 'category' || ($item['content_type'] == 'content-html' && $item['content_html']!=''))) {
                        $html .= '<div class="megamenu-content" '.($content_width==''?'':'style="width:'.$content_width.'"').'>';

                        // Content HTML
                        if ($item['content_type'] == 'content-html' && $item['content_html']!='') {
                            $html .= '<div class="nav-dropdown">' . $this->filter($item['content_html']) . '</div>';
                        }

                        // Dynamic Tab
                        if ($item['content_type'] == 'dynamic-content' && $hasChildren) {
                            $html .= '<div class="level' . $level . ' nav-dropdown">';
                            $children = $item['children'];
                            $i = $z = 0;
                            $total = count($children);
                            $column = (int)$item['mega_cols'];

                            $html .= '<div class="dorgin-items row hidden-sm hidden-xs">';
                            $html .= '<div class="dynamic-items col-xs-3 hidden-xs hidden-sm">';
                            $html .= '<ul>';
                            foreach ($children as $it) {
                                $iClass = '';
                                if($z==0){
                                    $iClass = 'class="dynamic-active"';
                                }
                                $html .= '<li ' . $iClass . ' data-dynamic-id="' . $it['htmlId'] . '">';
                                $html .= $this->drawAnchor($it, $level);
                                $html .= '</li>';
                                $i++;
                                $z++;
                            }
                            $html .= '</ul>';
                            $html .= '</div>';
                            $html .= '<div class="dynamic-content col-xs-9 hidden-xs hidden-sm">';
                            $z = 0;
                            foreach ($children as $it) {
                                if($z==0){ $it['class'] = 'dynamic-active'; }
                                $it['dynamic'] = true;
                                $html .= $this->filter($this->drawItem($it, $level, $i, false));
                                $i++;
                                $z++;
                            }
                            $html .= '</div>';
                            $html .= '</div>';


                            $html .= '<div class="orgin-items hidden-lg hidden-md">';
                            $i = 0;
                            $column = 1;
                            foreach ($children as $it) {
                                if( $column == 1 || $i%$column == 0){
                                    $html .= '<div class="row">';
                                }
                                $html .= '<div class="mega-col col-sm-' . (12/$column) . ' mega-col-' . $i . ' mega-col-level-' . $level . '">';
                                $html .= $this->filter($this->drawItem($it, $level, $i, false));
                                $html .= '</div>';
                                if( $column == 1 || ($i+1)%$column == 0 || $i == ($total-1) ) {
                                    $html .= '</div>';
                                }
                                $i++;
                            }
                            $html .= '</div>';


                            $html .= '</div>';
                        }

                        // Child item
                        if ($item['content_type'] == 'child-items' && $hasChildren) {
                            $html .= '<div class="level' . $level . ' nav-dropdown">';
                            $children = $item['children'];
                            $i = 0;
                            $total = count($children);
                            $column = (int)$item['mega_cols'];
                            foreach ($children as $it) {
                                if( $column == 1 || $i%$column == 0){
                                    $html .= '<div class="row">';
                                }
                                $html .= '<div class="mega-col col-sm-' . (12/$column) . ' mega-col-' . $i . ' mega-col-level-' . $level . ' col-xs-12">';
                                $html .= $this->drawItem($it, $level, $i, false);
                                $html .= '</div>';
                                if( $column == 1 || ($i+1)%$column == 0 || $i == ($total-1) ) {
                                    $html .= '</div>';
                                }
                                $i++;
                            }
                            $html .= '</div>';
                        }

                        // CATEGORY item
                        if ($item['content_type'] == 'category') {
                            $html .= '<div class="level' . $level . ' nav-dropdown">';

                            $catChildren = $this->getTreeCategories($item['category_id']);

                            $i = 0;
                            $total = count($catChildren);
                            $column = (int)$item['mega_cols'];
                            foreach ($catChildren as $it) {
                                if( $column == 1 || $i%$column == 0){
                                    $html .= '<div class="row">';
                                }
                                $html .= '<div class="mega-col col-sm-' . (12/$column) . ' mega-col-' . $i . ' mega-col-level-' . $level . ' col-xs-12">';
                                $html .= $this->drawItem($it, $level, $i, false);
                                $html .= '</div>';
                                if( $column == 1 || ($i+1)%$column == 0 || $i == ($total-1) ) {
                                    $html .= '</div>';
                                }
                                $i++;
                            }
                            $html .= '</div>';

                        }
                        $html .= '</div>';
                    }

                    // RIGHT SIDEBAR BLOCK
                    if ($item['show_right_sidebar'] && $item['right_sidebar_html']!='') {
                        if($right_sidebar_width) $right_sidebar_width = 'style="width:' . $right_sidebar_width . '"';
                        $html .= '<div class="megamenu-sidebar right-sidebar" '.$right_sidebar_width.'>'.$this->filter($item['right_sidebar_html']).'</div>';
                    }

                    $html .= '</div>';

                }

                // BOOTM BLOCK
                if ($item['show_footer'] && $item['footer_html']!='') {
                    $html .= '<div class="megamenu-footer">'.$this->filter($item['footer_html']).'</div>';
                }

                if (!isset($item['dynamic'])) $html .= '</div>';
            }
            if($listTag){
                $html .= '</li>';
            }else{
                $html .= '</div>';
            }
            $html = $this->filter($html);
        }catch(\Exception $e){
            die($e->getMessage());
        }
        return $html;
    }

    /**
     * Anchor draw
     *
     * @param $item
     * @return mixed|string
     */
    public function drawAnchor($item){
        $hasChildren = false;
        $tChildren = false;

        if($item['content_type'] == 'category'){
            $catChildren = $this->getTreeCategories($item['category_id']);
            if($catChildren) $tChildren = true;
        }
        if(($item['show_footer'] && $item['footer_html']!='') || ($item['show_header'] && $item['header_html']!='') ||  ($item['show_left_sidebar'] && $item['left_sidebar_html']!='') || ($item['show_right_sidebar'] && $item['right_sidebar_html']!='') || ($item['show_content'] && ((($item['content_type'] == 'child-items' || $item['content_type'] == 'dynamic-content') && (isset($item['children']) && count($item['children'])>0)) || ($item['content_type'] == 'content-html' && $item['content_html']!=''))) || ($item['content_type'] == 'category' && $tChildren) ) $hasChildren = true;

        $html = $class = $style = $attr = '';

        $class .= ' nav-anchor';

        if($item['content_type'] == 'dynamic-content') $class .= ' subdynamic';
        if($item['is_group']) $class .= ' subitems-group';

        // Custom Link, Category Link
        $href = '';
        if($item['link_type'] == 'custom-link'){
            $href = $this->filter($item['link']);
            if($this->endsWith($href, '/')){
                $href = substr_replace($href, "", -1);
            }

        }elseif($item['link_type'] == 'category-link'){
            $cats = $this->getAllCategory();
            foreach ($cats as $_cat) {
                if($_cat->getId() == $item['category_id']){
                    $href = $_cat->getUrl();
                    break;
                }
            }
        }

        if($class!='') $class = 'class="ui-corner-all ' . $class . '"';


        $target = $item['link_target']?'target="' . $item['link_target'] . '"':'';
        if($href=='') $href = '#';
        if ($href[0] === '#' &&  strlen($href) > 1) {
            $href = substr_replace($href, "", 0, 1);
        }
        if($href == '#') $target = '';
        $html .= '<a href="' . $href . '" ' . $target . ' ' . $attr . ' ' . $style . ' ' . $class . ' role="menuitem">';

        if (($item['show_icon'] == 0) && $item['font_classes']!='') {
            $html .= '<i class="fa font-awe mid-align ' .$item['font_classes'] . '"></i>';
        }

        // Icon Image
        if (($item['show_icon'] == 1) && $item['icon_image']!='') {
            $html .= '<img class="icon-left mid-align" ';
            $html .= ' src="'.$this->imageModel->getMediaUrl().'item/image'.$item['icon_image'].'" alt="'.$item['title'].'"/>';
        }

        if($item['title']!=''){
            $html .= '<span class="item-title mid-align">' . $item['title'] . '</span>';
        }

        if($hasChildren) $html .= '<span class="mid-align caret"></span><span class="mid-align opener"></span>';

        $html .= '</a>';
        return $html;
    }

    /**
     * @return mixed
     */
    public function getAllCategory(){
        if(!$this->_cats){
            $this->_cats = $this->_categoryFactory->create()->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('is_active','1')
                ->addAttributeToSort('position', 'asc');
        }
        return $this->_cats;
    }

    /**
     * @param $parentId
     * @param int $level
     * @param array $list
     * @return array
     */
    public function getTreeCategories($parentId, $level = 0, $list = []){
        $cats = $this->getAllCategory();
        foreach($cats as $category)
        {
            if($category->getParentId() == $parentId){
                $tmp = [];
                $tmp["name"] = $category->getName();
                $tmp['link_type'] = 'custom_link';
                $tmp['link'] = $category->getUrl();
                $tmp['show_footer'] = $tmp['show_header'] = $tmp['show_left_sidebar'] = $tmp['show_right_sidebar'] = 0;
                $tmp['show_content'] = 1;
                $tmp['content_width'] = $tmp['sub_width'] = '100%';
                $tmp['color'] = '';
                $tmp['show_icon'] = $tmp['is_group'] = false;
                $tmp['content_type'] = 'childmenu';
                $tmp['target'] = '_self';
                $tmp['align'] = 3;
                $tmp['mega_cols'] = 1;
                $tmp['status'] = 1;
                $tmp['disable_bellow'] = 0;
                $tmp['classes'] = '';
                $tmp['id'] = $category->getId();
                $subcats = $category->getChildren();
                if($subcats){
                    $tmp['children'] = $this->getTreeCategories($category->getId(),(int)$level + 1);
                }
                $list[] = $tmp;
            }
        }
        return $list;
    }

    /**
     * Check if a string ends with a substring
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    private function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }
}
