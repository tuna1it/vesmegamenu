<?php

namespace Vnecoms\Megamenu\Helper\Wysiwyg;
use Magento\Framework\App\Filesystem\DirectoryList;

class Images extends \Magento\Cms\Helper\Wysiwyg\Images
{
	public function getImageHtmlDeclaration($filename, $renderAsTag = false)
	{
		if($renderAsTag == 'ves-megamenu'){
			$fileurl = $this->getCurrentUrl() . $filename;
			$mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$mediaPath = str_replace($mediaUrl, '', $fileurl);
			return $fileurl;
		}else{
			return parent::getImageHtmlDeclaration($filename, $renderAsTag);
		}
	}

}