<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 04/10/2016
 * Time: 16:22
 */

namespace Vnecoms\Megamenu\Api;


interface ItemRepositoryInterface
{
    /**
     * Save menu.
     *
     * @param \Vnecoms\Megamenu\Api\Data\ItemInterface $item
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Vnecoms\Megamenu\Api\Data\ItemInterface $item);

    /**
     * Retrieve menu.
     *
     * @param int $itemId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($itemId);

    /**
     * Retrieve menus matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vnecoms\Megamenu\Api\Data\MenuSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete menu.
     *
     * @param \Vnecoms\Megamenu\Api\Data\ItemInterface $item
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Vnecoms\Megamenu\Api\Data\ItemInterface $item);

    /**
     * Delete menu by ID.
     *
     * @param int $itemId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($itemId);
}