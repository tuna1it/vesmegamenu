<?php

namespace Vnecoms\Megamenu\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * MENU CRUD interface.
 * @api
 */
interface MenuRepositoryInterface
{
    /**
     * Save menu.
     *
     * @param \Vnecoms\Megamenu\Api\Data\MenuInterface $menu
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Vnecoms\Megamenu\Api\Data\MenuInterface $menu);

    /**
     * Retrieve menu.
     *
     * @param int $menuId
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($menuId);

    /**
     * Retrieve menus matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Vnecoms\Megamenu\Api\Data\MenuSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete menu.
     *
     * @param \Vnecoms\Megamenu\Api\Data\MenuInterface $menu
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Vnecoms\Megamenu\Api\Data\MenuInterface $menu);

    /**
     * Delete menu by ID.
     *
     * @param int $menuId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($menuId);
}
