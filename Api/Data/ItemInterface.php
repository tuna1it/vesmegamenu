<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 04/10/2016
 * Time: 16:23
 */

namespace Vnecoms\Megamenu\Api\Data;

/**
 * Interface Item
 * @package Vnecoms\Megamenu\Api\Data
 * @api
 */
interface ItemInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ITEM_ID                 = 'item_id';
    const PARENT_ID               = 'parent_id';
    const MENU_ID                 = 'menu_id';
    const TITLE                   = 'title';
    const SHOW_TITLE              = 'show_title';
    const SHOW_CONTENT            = 'show_content';
    const SHOW_ICON               = 'show_icon';
    const ICON_IMAGE              = 'icon_image';
    const FONT_CLASSES            = 'font_classes';
    const LINK_TYPE               = 'link_type';
    const LINK                    = 'link';
    const LINK_TARGET             = 'link_target';
    const CATEGORY_ID             = 'category_id';
    const SHOW_NUMBER_PRODUCT     = 'show_number_product';
    const CMS_PAGE                = 'cms_page';
    const IS_GROUP                = 'is_group';
    const MEGA_COLS               = 'mega_cols';
    const CONTENT_WIDTH           = 'content_width';
    const SUB_WIDTH               = 'sub_width';
    const LEFT_SIDEBAR_WIDTH      = 'left_sidebar_width';
    const RIGHT_SIDEBAR_WIDTH     = 'right_sidebar_width';
    const CONTENT_HTML            = 'content_html';
    const HEADER_HTML             = 'header_html';
    const LEFT_SIDEBAR_HTML       = 'left_sidebar_html';
    const RIGHT_SIDEBAR_HTML      = 'right_sidebar_html';
    const FOOTER_HTML             = 'footer_html';
    const CONTENT_TYPE            = 'content_type';
    const STATIC_BLOCKS           = 'static_blocks';
    const VISIBLE_OPTION          = 'visible_option';
    const VISIBLE_IN              = 'visible_in';
    const ADDITION_CLASS          = 'addition_class';
    const CREATION_TIME           = 'creation_time';
    const UPDATE_TIME             = 'update_time';
    const IS_ACTIVE               = 'is_active';
    const SORT_ORDER              = 'sort_order';
    const STORE_ID                = 'store_id';
    const ANIMATION_IN            = 'animation_in';
    const ANIMATION_TIME          = 'animation_time';
    const ALIGN                   = 'align';
    const DROPDOWN_INLINECSS      = 'dropdown_inlinecss';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Parent ID
     *
     * @return int|null
     */
    public function getParentId();

    /**
     * Get Menu ID
     *
     * @return int|null
     */
    public function getMenuId();

    /**
     * Get Title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get Show Title
     *
     * @return bool|null
     */
    public function isShowTitle();

    /**
     * Get Icon Image
     *
     * @return string|null
     */
    public function getIconImage();

    /**
     * Get Font Icon Classes
     *
     * @return string|null
     */
    public function getFontClasses();

    /**
     * Get Link Type
     *
     * @return string|null
     */
    public function getLinkType();

    /**
     * Get Show Icon
     *
     * @return int|null
     */
    public function getShowIcon();

    /**
     * Get Link
     *
     * @return string|null
     */
    public function getLink();

    /**
     * Get Link Target
     *
     * @return string|null
     */
    public function getLinkTarget();

    /**
     * Get Category ID
     *
     * @return int|null
     */
    public function getCategoryId();

    /**
     * Get Show Number Product
     *
     * @return bool|null
     */
    public function isShowNumberProduct();

    /**
     * Get CMS Page
     *
     * @return string|null
     */
    public function getCmsPage();

    /**
     * Get Is Group
     *
     * @return bool|null
     */
    public function isGroup();

    /**
     * Get Mega Cols
     *
     * @return int|null
     */
    public function getMegaCols();

    /**
     * Get Show Content
     *
     * @return bool|null
     */
    public function IsShowContent();

    /**
     * @param $isShowContent
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsShowContent($isShowContent);

    /**
     * Get Content Width
     *
     * @return int|null
     */
    public function getContentWidth();

    /**
     * Get Sub Width
     *
     * @return int|null
     */
    public function getSubWidth();

    /**
     * Get Left Side Width
     *
     * @return int|null
     */
    public function getLeftSideWidth();

    /**
     * Get Right Side Width
     *
     * @return int|null
     */
    public function getRightSideWidth();

    /**
     * Get content type
     * @return string|null
     */
    public function getContentType();

    /**
     * @param $contentType
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setContentType($contentType);



    /**
     * Get Content Html
     *
     * @return string|null
     */
    public function getContentHtml();

    /**
     * Get Static Blocks
     *
     * @return string|null
     */
    public function getStaticBlocks();

    /**
     * Get Visible Option
     *
     * @return string|null
     */
    public function getVisibleOption();

    /**
     * Get Visible In
     *
     * @return string|null
     */
    public function getVisibleIn();

    /**
     * Get Addition Class
     *
     * @return string|null
     */
    public function getAdditionClass();


    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Get sort order
     *
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setId($id);

    /**
     * Set Parent ID
     *
     * @param int $parentId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setParentId($parentId);

    /**
     * Set Menu ID
     *
     * @param int $groupId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setMenuId($groupId);

    /**
     * Set Show Icon
     *
     * @param int $showIcon
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setShowIcon($showIcon);

    /**
     * Set Title
     *
     * @param string $title
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setTitle($title);

    /**
     * Set Show Title
     *
     * @param int $isShowTitle
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsShowTitle($isShowTitle);

    /**
     * Set Icon Image
     *
     * @param string $iconImage
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIconImage($iconImage);

    /**
     * Set Font Icon Class
     *
     * @param string $fontClasses
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setFontClasses($fontClasses);

    /**
     * Set Link Type
     *
     * @param string $linkType
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLinkType($linkType);

    /**
     * Set Link
     *
     * @param string $link
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLink($link);

    /**
     * Set Link Target
     *
     * @param string $linkTarget
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLinkTarget($linkTarget);

    /**
     * Set Category ID
     *
     * @param string $categoryId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setCategoryId($categoryId);

    /**
     * Set Show Number Product
     *
     * @param int $isShowNumberProduct
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsShowNumberProduct($isShowNumberProduct);

    /**
     * Set CMS page
     *
     * @param string $cmsPage
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setCmsPage($cmsPage);

    /**
     * Set Is Group
     *
     * @param string $isGroup
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsGroup($isGroup);

    /**
     * Set Mega Cols
     *
     * @param string $megaCols
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setMegaCols($megaCols);

    /**
     * Set Content Width
     *
     * @param string $contentWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setContentWidth($contentWidth);


    /**
     * Set Sub Width
     *
     * @param string $subWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setSubWidth($subWidth);

    /**
     * Set Left Side Width
     *
     * @param string $leftWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLeftSideWidth($leftWidth);

    /**
     * Set Right Side Width
     *
     * @param string $rightWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setRightSideWidth($rightWidth);

    /**
     * Set Content Html
     *
     * @param string $contentHtml
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setContentHtml($contentHtml);

    /**
     * Set Static Blocks
     *
     * @param string $staticBlocks
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setStaticBlocks($staticBlocks);

    /**
     * Set Visible Option
     *
     * @param string $visibleOption
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setVisibleOption($visibleOption);

    /**
     * Set Visible In
     *
     * @param string $visibleIn
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setVisibleIn($visibleIn);

    /**
     * Set Addition Class
     *
     * @param string $additionClass
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setAdditionClass($additionClass);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set sort order
     *
     * @param string $sortOrder
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsActive($isActive);


    /**
     * @param $storeId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setStoreId($storeId);

    /**
     * @return int[]
     */
    public function getStoreId();

    /**
     * Get animation subcontent
     * @return string|null
     */
    public function getAnimationIn();

    /**
     * @param $animation
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setAnimationIn($animation);


    /**
     * Get animation time
     * @return string|null
     */
    public function getAnimationTime();

    /**
     * @param $animationTime
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setAnimationTime($animationTime);


    /**
     * Get subcontent align
     * @return string|null
     */
    public function getAlign();

    /**
     * @param $align
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setAlign($align);

    /**
     * Get subcontent inline style
     * @return string|null
     */
    public function getDropdownInlinceCss();

    /**
     * @param $inline
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setDropdownInlineCss($inline);

    /**
     * Get header html
     * @return string|null
     */
    public function getHeaderHtml();

    /**
     * Set header html
     * @param $header
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setHeaderHtml($header);

    /**
     * Get left side html
     * @return string|null
     */
    public function getLeftSideHtml();

    /**
     * Set left side html
     * @param $left
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLeftSideHtml($left);

    /**
     * Get right side html
     * @return string|null
     */
    public function getRightSideHtml();

    /**
     * Set right side html
     * @param $right
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setRightSideHtml($right);

    /**
     * Get footer html
     * @return string|null
     */
    public function getFooterHtml();

    /**
     * Set footer html
     * @param $footer
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setFooterHtml($footer);
}
