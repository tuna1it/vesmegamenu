<?php

namespace Vnecoms\Megamenu\Api\Data;

/**
 * Interface Menu
 * @package Vnecoms\Megamenu\Api\Data
 * @api
 */
interface MenuInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const MENU_ID                  = 'menu_id';
    const IDENTIFIER               = 'identifier';
    const TITLE                    = 'title';
    const CREATION_TIME            = 'creation_time';
    const UPDATE_TIME              = 'update_time';
    const IS_ACTIVE                = 'is_active';
    const DESKTOP_TEMPLATE         = 'desktop_template';
    const MOBILE_TEMPLATE          = 'mobile_template';
    const STRUCTURE                = 'structure';
    const ANIMATION_TYPE           = 'animation_type';
    const DESCRIPTION              = 'description';
    const SORT_ORDER               = 'sort_order';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier();

    /**
     * Get menu title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();


    /**
     * Get Menu animation
     *
     * @return string|null
     */
    public function getAnimationType();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * @return MenuInterface
     */
    public function getDesktopTemplate();

    /**
     * @return MenuInterface
     */
    public function getMobileTemplate();

    /**
     * @return MenuInterface
     */
    public function getStructure();

    /**
     * @return MenuInterface
     */
    public function getSortOrder();

    /**
     * Get ids of customer groups that the menu applies to
     *
     * @return int[]
     */
    public function getCustomerGroupIds();

    /**
     * status
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return MenuInterface
     */
    public function setId($id);

    /**
     * Set alias
     *
     * @param string $identifier
     * @return MenuInterface
     */
    public function setIdentifier($identifier);

    /**
     * Set menu Title
     *
     * @param string $title
     * @return MenuInterface
     */
    public function setTitle($title);


    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return MenuInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return MenuInterface
     */
    public function setUpdateTime($updateTime);


    /**
     * @param $animation
     * @return MenuInterface
     */
    public function setAnimationType($animation);


    /**
     * @param $template
     * @return MenuInterface
     */
    public function setDesktopTemplate($template);

    /**
     * @param $mobileTemplate
     * @return MenuInterface
     */
    public function setMobileTemplate($mobileTemplate);

    /**
     * @param $structure
     * @return MenuInterface
     */
    public function setStructure($structure);

    /**
     * @param $sortOrder
     * @return mixed
     */
    public function setSortOrder($sortOrder);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return MenuInterface
     */
    public function setIsActive($isActive);
    
}
