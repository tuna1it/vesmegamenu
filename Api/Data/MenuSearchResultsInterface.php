<?php

namespace Vnecoms\Megamenu\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for cms page search results.
 * @api
 */
interface MenuSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get menu list.
     *
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface[]
     */
    public function getItems();

    /**
     * Set menu list.
     *
     * @param \Vnecoms\Megamenu\Api\Data\MenuInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
