<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 04/10/2016
 * Time: 16:23
 */

namespace Vnecoms\Megamenu\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ItemSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get items.
     *
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface[]
     */
    public function getItems();


    /**
     * Set items.
     *
     * @param \Vnecoms\Megamenu\Api\Data\ItemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}