<?php

namespace Vnecoms\Megamenu\Model\ResourceModel\Item;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
//use Vnecoms\Megamenu\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;
use Vnecoms\Megamenu\Api\Data\ItemInterface;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'item_id';

    /** @var  \Magento\Framework\ObjectManagerInterface */
    protected $_objectManager;

    /** @var \Magento\Framework\Registry  */
    protected $_coreRegistry;
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vnecoms\Megamenu\Model\Item', 'Vnecoms\Megamenu\Model\ResourceModel\Item');
        $this->_map['fields']['item_id'] = 'main_table.item_id';
    }

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ){
        $this->_coreRegistry = $registry;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }


    /**
     * Returns pairs id -title
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('item_id', 'title');
    }


    /**
     * Action before render all item
     *
     * @return $this
     */
    protected function _beforeLoad()
    {
        //$this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\ObjectManagerInterface');
        //$menuId = $this->_objectManager->get('Magento\Framework\Registry')->registry('current_menu_id');
        $menuId = $this->_coreRegistry->registry('current_menu_id');

        if ($menuId) {
            $this->addFieldToFilter('menu_id', $menuId);
        }
        return parent::_beforeLoad();
    }

    /**
     * {@inheritdoc}
     */
    protected function _afterLoad()
    {
        return parent::_afterLoad();
    }

    /**
     * @param array|string $field
     * @param null $condition
     * @return $this|Collection
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field === 'store_id') {
            return $this->addStoreFilter($condition, false);
        }

        return parent::addFieldToFilter($field, $condition);
    }

    /**
     * Add filter by store
     *
     * @param int|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            if ($store instanceof Store) {
                $store = [$store->getId()];
            }

            if (!is_array($store)) {
                $store = [$store];
            }

            if ($withAdmin) {
                $store[] = Store::DEFAULT_STORE_ID;
            }

            $this->addFilter('store_id', ['in' => $store], 'public');
        }
        return $this;
    }


}
