<?php

namespace Vnecoms\Megamenu\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime;
use Vnecoms\Megamenu\Api\Data\MenuInterface;
use Vnecoms\Megamenu\Model\Item\Image as ImageModel;
use Vnecoms\Megamenu\Api\Data\ItemInterface;
//use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class ResourceModel Item
 * @package Vnecoms\Megamenu\Model\ResourceModel
 */
class Item extends AbstractDb
{

    /**
     * @var DateTime
     */
    protected $dateTime;

    /** @var  \Vnecoms\Megamenu\Model\Item\Image */
    protected $imageModel;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /** @var  \Magento\Framework\Registry */
    protected $_coreRegistry;


    /**
     * Item constructor.
     * @param Context $context
     * @param DateTime $dateTime
     * @param ImageModel $imageModel
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\RequestInterface $request
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        ImageModel $imageModel,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\RequestInterface $request,
        $connectionName = null
    ){
        parent::__construct($context, $connectionName);
        $this->dateTime = $dateTime;
        $this->_coreRegistry = $registry;
        $this->request = $request;
        $this->imageModel = $imageModel;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_megamenu_item', 'item_id');
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        return parent::_getLoadSelect($field, $value, $object);
    }

    /**
     * Do something before save item
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        return parent::_beforeSave($object);
    }

    /**
     * After save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        return parent::_afterSave($object);
    }


    /**
     * Load an object
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param mixed $value
     * @param string $field
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }

    /**
     * Perform operations after item object load
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        return parent::_afterLoad($object);
    }


    /**
     * @param $itemId
     * @return string
     */
    public function lookupMenuId($itemId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from(
            $this->getTable('ves_megamenu_item'),
            'menu_id'
        )->where(
            'item_id = ?',
            (int)$itemId
        );

        return $connection->fetchOne($select);
    }

    /**
     * Process Item Data Before Delete
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _beforeDelete(\Magento\Framework\Model\AbstractModel $object)
    {
        //check has child items of this item
        $id = (int) $object->getId();
        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('ves_megamenu_item'),
            'menu_id'
        )->where(
            'parent_id = ?',
            (int)$id
        );
        $child = $this->getConnection()->fetchAll($select);
        if (sizeof($child)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Can not delete this menu item. The first you have to delete child menu items of this menu item.')
            );
        } else {
            //delete related icon image uploaded
            $image = $object->getIconImage();
            if ($image) {
                $imagePath = $this->imageModel->getMediaUrl().'ves_megamenu/item/image'.$image;
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
            //delete this item
            $condition = ['item_id = ?' => (int) $object->getId()];
            $this->getConnection()->delete($this->getMainTable(), $condition);
        }

        return parent::_beforeDelete($object);
    }

    /**
     * Retrieves menu item title from DB by passed id.
     *
     * @param string $id
     * @return string|false
     */
    public function getMenuItemTitleById($id)
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from($this->getMainTable(), 'title')->where('item_id = :item_id');
        $binds = ['item_id' => (int) $id];
        return $connection->fetchOne($select, $binds);
    }

    /**
     * Retrieves available menu.
     * @return array
     */
    public function getMenuOptions()
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from($this->getTable('ves_megamenu_menu'), ['menu_id', 'title']);
        return $connection->fetchAll($select);
    }
}
