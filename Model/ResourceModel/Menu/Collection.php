<?php

namespace Vnecoms\Megamenu\Model\ResourceModel\Menu;

use Vnecoms\Megamenu\Api\Data\MenuInterface;
use Vnecoms\Megamenu\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * Class Menu Collection
 * @package Vnecoms\Megamenu\Model\ResourceModel\Menu
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'menu_id';


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vnecoms\Megamenu\Model\Menu', 'Vnecoms\Megamenu\Model\ResourceModel\Menu');
        $this->_map['fields']['menu_id'] = 'main_table.menu_id';
        $this->_map['fields']['store_id'] = 'store_table.store_id';
    }

    /**
     * Returns pairs menu_id - identifier
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('menu_id', 'title');
    }


    /**
     * Add filter by store
     *
     * @param int|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            if ($store instanceof Store) {
                $store = [$store->getId()];
            }

            if (!is_array($store)) {
                $store = [$store];
            }

            if ($withAdmin) {
                $store[] = Store::DEFAULT_STORE_ID;
            }

            $this->addFilter('store_id', ['in' => $store], 'public');
        }
        return $this;
    }

    /**
     * @param array|string $field
     * @param null $condition
     * @return $this|Collection
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field === 'store_id') {
            return $this->addStoreFilter($condition, false);
        }

        return parent::addFieldToFilter($field, $condition);
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @return \Magento\Framework\DB\Select
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(\Zend_Db_Select::GROUP);
        return $countSelect;
    }

    /**
     * Perform operations after collection load
     *
     * @return $this
     */
    protected function _afterLoad()
    {

        $entityMetadata = $this->metadataPool->getMetadata(MenuInterface::class);
        $this->performAfterLoad('ves_megamenu_menu_store', $entityMetadata->getLinkField());

        return parent::_afterLoad();
    }

    /**
     * Join store relation table if there is store filter
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('store_id')) {
            $this->getSelect()->join(
                ['store_table' => $this->getTable('ves_megamenu_menu_store')],
                'main_table.menu_id = store_table.menu_id',
                []
            )->group('main_table.menu_id');

        }
        parent::_renderFiltersBefore();
    }


    /**
     * @param $tableName
     * @param $linkField
     */
    protected function performAfterLoad($tableName, $linkField)
    {
        $linkedIds = $this->getColumnValues($linkField);
        if (count($linkedIds)) {
            $connection = $this->getConnection();
            $select = $connection->select()->from(['ves_megamenu_menu_store' => $this->getTable($tableName)])
                ->where('ves_megamenu_menu_store.' . $linkField . ' IN (?)', $linkedIds);
            // @codingStandardsIgnoreStart
            $result = $connection->fetchAll($select);
            // @codingStandardsIgnoreEnd
            if ($result) {
                $storesData = [];
                foreach ($result as $storeData) {
                    $storesData[$storeData[$linkField]][] = $storeData['store_id'];
                }

                foreach ($this as $item) {
                    $linkedId = $item->getData($linkField);
                    if (!isset($storesData[$linkedId])) {
                        continue;
                    }
                    $storeIdKey = array_search(Store::DEFAULT_STORE_ID, $storesData[$linkedId], true);
                    if ($storeIdKey !== false) {
                        $stores = $this->storeManager->getStores(false, true);
                        $storeId = current($stores)->getId();
                        $storeCode = key($stores);
                    } else {
                        $storeId = current($storesData[$linkedId]);
                        $storeCode = $this->storeManager->getStore($storeId)->getCode();
                    }
                    $item->setData('store_id', $storesData[$linkedId]);
                }
            }

            $select = $connection->select()->from(['ves_megamenu_menu_customergroup' => $this->getTable($tableName)])
                ->where('ves_megamenu_menu_customergroup.' . $linkField . ' IN (?)', $linkedIds);
            $result = $connection->fetchAll($select);
            if ($result) {
                $customerGroupsData = [];
                foreach ($result as $customerGroup) {
                    //var_dump($customerGroup);exit;
                    $customerGroupsData[$customerGroup[$linkField]][] = $customerGroup['menu_id'];
                }

                foreach ($this as $item) {
                    $linkedId = $item->getData($linkField);
                    //var_dump($customerGroupsData[1]);exit;
                    if (!isset($customerGroupsData[$linkedId])) {
                        continue;
                    }
                    $item->setData('customer_group_ids', $customerGroupsData[$linkedId]);
                }
            }
        }
    }

}
