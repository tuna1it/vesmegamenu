<?php

namespace Vnecoms\Megamenu\Model\ResourceModel\Menu\Relation\Store;

use Vnecoms\Megamenu\Model\ResourceModel\Menu;
use Magento\Framework\EntityManager\Operation\ExtensionInterface;

/**
 * Class ReadHandler
 * @package Vnecoms\Megamenu\Model\ResourceModel\Menu\Relation\Store
 */
class ReadHandler implements ExtensionInterface
{
    /**
     * @var Menu
     */
    protected $resourceMenu;

    /**
     * ReadHandler constructor.
     * @param Menu $resourceMenu
     */
    public function __construct(
        Menu $resourceMenu
    ) {
        $this->resourceMenu = $resourceMenu;
    }

    /**
     * @param object $entity
     * @param array $arguments
     * @return object
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute($entity, $arguments = [])
    {
        if ($entity->getId()) {
            $stores = $this->resourceMenu->lookupStoreIds((int)$entity->getId());
            $entity->setData('store_id', $stores);
            //var_dump($entity->getData());exit;
            //$entity->setData('structure','test-test-test'); CAN SET DATA FOR FIELD OF ENTITY
            /*if (isset($stores)) {
                $entity->setData('stores', $stores);
            }*/
        }
        return $entity;
    }
}
