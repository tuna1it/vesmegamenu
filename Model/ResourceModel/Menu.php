<?php

namespace Vnecoms\Megamenu\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Store\Model\Store;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\DB\Select;
use Vnecoms\Megamenu\Api\Data\MenuInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ResourceModel Menu
 * @package Vnecoms\Megamenu\Model\ResourceModel
 */
class Menu extends AbstractDb
{

    /**
     * Store model
     *
     * @var null|Store
     */
    protected $_store = null;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var array
     */
    protected $_data;


    /** @var \Vnecoms\Megamenu\Helper\Data  */
    protected $_helperData;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;


    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var MetadataPool
     */
    protected $metadataPool;

    /** @var \Vnecoms\Megamenu\Model\ItemFactory  */
    protected $_itemFactory;

    /**
     * Menu constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Vnecoms\Megamenu\Helper\Data $helperData
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime $dataTime
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param EntityManager $entityManager
     * @param MetadataPool $metadataPool
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Vnecoms\Megamenu\Helper\Data $helperData,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime $dataTime,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager    = $storeManager;
        $this->_filesystem      = $filesystem;
        $this->_helperData      = $helperData;
        $this->_date            = $date;
        $this->_itemFactory     = $itemFactory;
        $this->_dateTime        = $dataTime;
        $this->entityManager    = $entityManager;
        $this->metadataPool     = $metadataPool;
    }

    /**
     * Init resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_megamenu_menu', 'menu_id');
    }


    /**
     * Process group data before deleting
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _beforeDelete(\Magento\Framework\Model\AbstractModel $object)
    {
        //we will delete all menu items of this menu group first
        $itemIds = $this->lookupItemIds((int)$object->getId());
        if ($itemIds && is_array($itemIds)){
            $model = $this->_itemFactory->create();
            foreach ($itemIds as $id) {
                $model->load($id);
                $model->delete();
            }
        }

        $condition = ['menu_id = ?' => (int)$object->getId()];
        $this->getConnection()->delete($this->getMainTable(), $condition);
        $this->getConnection()->delete($this->getTable('ves_megamenu_menu_customergroup'), $condition);

        return parent::_beforeDelete($object);
    }

    /**
     * Process group data before saving
     *
     * @param \Magento\Framework\Model\AbstractModel|\Vnecoms\Megamenu\Model\Menu $object
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if (!$this->isValidIdentifier($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The menu key contains capital letters or disallowed symbols.')
            );
        }

        if ($this->isNumericIdentifier($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The menu key cannot be made of only numbers.')
            );
        }

        //check exists identifier
        if ($this->isExistsIdentifier($object)){
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The menu key was used by selected store views.')
            );
        }

        return parent::_beforeSave($object);
    }

    /**
     * Assign group to store views
     *
     * @param \Magento\Framework\Model\AbstractModel|\Vnecoms\Megamenu\Model\Menu $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();

        $table = $this->getTable('ves_megamenu_menu_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);

        if ($delete) {
            $where = ['menu_id = ?' => (int)$object->getId(), 'store_id IN (?)' => $delete];
            $this->getConnection()->delete($table, $where);
        }

        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = ['menu_id' => (int)$object->getId(), 'store_id' => (int)$storeId];
            }
            $this->getConnection()->insertMultiple($table, $data);
        }

        // CUSTOMER GROUP
        $oldCustomerGroups = $this->lookupCustomerGroupIds($object->getId());
        $newCustomerGroups = (array)$object->getCustomerGroupIds();
        $table = $this->getTable('ves_megamenu_menu_customergroup');
        $insert = array_diff($newCustomerGroups, $oldCustomerGroups);
        $delete = array_diff($oldCustomerGroups, $newCustomerGroups);
        if ($delete) {
            $where = ['menu_id = ?' => (int)$object->getId(), 'customer_group_id IN (?)' => $delete];
            $this->getConnection()->delete($table, $where);
        }
        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = ['menu_id' => (int)$object->getId(), 'customer_group_id' => (int)$storeId];
            }
            $this->getConnection()->insertMultiple($table, $data);
        }

        return parent::_afterSave($object);
    }

    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param mixed $value
     * @param string $field
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * Perform operations after object load
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {

        $menuId = $object->getId();
        if ($menuId) {
            $select = $this->getConnection()->select()->from($this->getTable('ves_megamenu_item'))
                ->where(
                    'menu_id = ?',
                    intval($menuId)
                );
            $data = $this->getConnection()->fetchAll($select);
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $webUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            $menuItems = [];
            if(!empty($data)){
                foreach ($data as $k => $v) {
                    if(is_array($v)){
                        foreach ($v as $x => $y) {
                            //$v contain data of each item .. $v['item_id'] ....
                            if($y!='' && $x == 'icon_image' ){
                                $v[$x] = $mediaUrl.'ves_megamenu/item/image'.substr($y, 1);
                            }

                            if($y!='' && $x == 'link' && substr($y, 0 ,1) == '#' ){
                                $v[$x] = $webUrl.substr($y, 1);
                            }
                        }
                    }
                    $v['htmlId'] = 'menuitem-' . $v['item_id'] . time() . rand();
                    $menuItems[$v['item_id']] = $v;
                }
            }
            $object->setData('menuItems', $menuItems);
        }

        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
        }

        if ($object->getId()) {
            $customerGroupIds = $this->lookupCustomerGroupIds($object->getId());
            $object->setData('customer_group_ids', $customerGroupIds);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Get customer groups ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupCustomerGroupIds($id)
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('ves_megamenu_menu_customergroup'),
            'customer_group_id'
        )->where(
            'menu_id = :menu_id'
        );
        $binds = [':menu_id' => (int)$id];
        return $connection->fetchCol($select, $binds);
    }


    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Vnecoms\Megamenu\Model\Menu|AbstractModel $object
     * @return Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $entityMetadata = $this->metadataPool->getMetadata(MenuInterface::class);
        $linkField = $entityMetadata->getLinkField();

        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {
            $stores = [(int)$object->getStoreId(), Store::DEFAULT_STORE_ID];

            $select->join(
                ['cbs' => $this->getTable('ves_megamenu_menu_store')],
                $this->getMainTable() . '.' . $linkField . ' = cbs.' . $linkField,
                ['store_id']
            )
                ->where('is_active = ?', 1)
                ->where('cbs.store_id in (?)', $stores)
                ->order('store_id DESC')
                ->limit(1);
        }

        return $select;
    }

    /**
     * Retrieve load select with filter by identifier, store and activity
     *
     * @param string $identifier
     * @param int|array $store
     * @param int $isActive
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadByIdentifierSelect($identifier, $store, $isActive = null)
    {
        $select = $this->getConnection()->select()->from(
            ['mg' => $this->getMainTable()]
        )->join(
            ['gs' => $this->getTable('ves_megamenu_menu_store')],
            'mg.menu_id = gs.menu_id',
            []
        )->where(
            'mg.identifier = ?',
            $identifier
        )->where(
            'gs.store_id IN (?)',
            $store
        );

        if (!is_null($isActive)) {
            $select->where('sl.is_active = ?', $isActive);
        }

        return $select;
    }

    /**
     *  Check whether group identifier is numeric
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    protected function isNumericIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[0-9]+$/', $object->getData('identifier'));
    }

    /**
     *  Check whether group identifier is valid
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    protected function isValidIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/', $object->getData('identifier'));
    }

    public function isExistsIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        $storeIds = (array)$object->getStores();
        array_push($storeIds, \Magento\Store\Model\Store::DEFAULT_STORE_ID);

        $select = $this->getConnection()->select()->from(
            ['mg' => $this->getMainTable()]
        )->join(
            ['gs' => $this->getTable('ves_megamenu_menu_store')],
            'mg.menu_id = gs.menu_id',
            []
        )->where(
            'mg.identifier = ?',
            $object->getData('identifier')
        )->where(
            'gs.store_id IN (?)',
            $storeIds
        );

        //if is edit
        if ($object->getData('menu_id')) {
            $select->where('mg.menu_id != ?', $object->getData('menu_id'));
        }

        $select->reset(\Zend_Db_Select::COLUMNS)->columns('mg.menu_id')->order('gs.store_id DESC')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }

    /**
     * Check if menu identifier exist for specific store
     * return menu id if menu exists
     *
     * @param string $identifier
     * @param int $storeId
     * @return int
     */
    public function checkIdentifier($identifier, $storeId)
    {
        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID, $storeId];
        $select = $this->_getLoadByIdentifierSelect($identifier, $stores, 1);
        $select->reset(\Zend_Db_Select::COLUMNS)->columns('mg.menu_id')->order('gs.store_id DESC')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }


    /**
     * Retrieves menu title from DB by passed identifier.
     *
     * @param string $identifier
     * @return string|false
     */
    public function getMenuTitleByIdentifier($identifier)
    {
        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID];
        if ($this->_store) {
            $stores[] = (int)$this->getStore()->getId();
        }

        $select = $this->_getLoadByIdentifierSelect($identifier, $stores);
        $select->reset(\Zend_Db_Select::COLUMNS)->columns('mg.title')->order('gs.store_id DESC')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }

    /**
     * Retrieves menu id from DB by passed identifier.
     *
     * @param string $identifier
     * @return string|false
     */
    public function getMenuIdByIdentifier($identifier)
    {
        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID];
        $stores[] = (int)$this->getStore()->getId();
        if ($this->_store) {
            $stores[] = (int)$this->getStore()->getId();
        }

        $select = $this->_getLoadByIdentifierSelect($identifier, $stores);
        $select->reset(\Zend_Db_Select::COLUMNS)->columns('mg.menu_id')->order('gs.store_id DESC')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }

    /**
     * Retrieves menu title from DB by passed id.
     *
     * @param string $id
     * @return string|false
     */
    public function getMenuTitleById($id)
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from($this->getMainTable(), 'title')->where('menu_id = :menu_id');

        $binds = ['menu_id' => (int)$id];

        return $connection->fetchOne($select, $binds);
    }


    /**
     * Retrieves menu identifier from DB by passed id.
     *
     * @param string $id
     * @return string|false
     */
    public function getMenuIdentifierById($id)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from($this->getMainTable(), 'identifier')->where('menu_id = :menu_id');

        $binds = ['menu_id' => (int)$id];

        return $connection->fetchOne($select, $binds);
    }


    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $menuId
     * @return int[]
     */
    public function lookupStoreIds($menuId)
    {
        $connection = $this->getConnection();

        $entityMetadata = $this->metadataPool->getMetadata(MenuInterface::class);
        $linkField = $entityMetadata->getLinkField();

        $select = $connection->select()
            ->from(['cbs' => $this->getTable('ves_megamenu_menu_store')], 'store_id')
            ->join(
                ['cb' => $this->getMainTable()],
                'cbs.' . $linkField . ' = cb.' . $linkField,
                []
            )
            ->where('cb.' . $entityMetadata->getIdentifierField()  . ' = :menu_id');

        return $connection->fetchCol($select, ['menu_id' => (int)$menuId]);
    }

    /**
     * Get menu item ids to which specified item is assigned
     *
     * @param int $menuId
     * @return array
     */
    public function lookupItemIds($menuId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from(
            $this->getTable('ves_megamenu_item'),
            'item_id'
        )->where(
            'menu_id = ?',
            (int)$menuId
        );

        return $connection->fetchCol($select);
    }

    /**
     * Retrieves available options menu group.
     * @return array
     */
    public function getOptions()
    {
        $connection = $this->getConnection();

        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID];
        if ($this->_store) {
            $stores[] = (int)$this->getStore()->getId();
        }

        $select = $connection->select()->from(
            ['mg' => $this->getMainTable()],
            ['menu_id', 'title']
        )->join(
            ['gs' => $this->getTable('ves_megamenu_menu_store')],
            'mg.menu_id = gs.menu_id',
            []
        )->where(
            'gs.store_id IN (?)',
            $stores
        );

        return $connection->fetchAll($select);
    }

    /**
     * @param $menuId
     * @return array
     */
    public function getMenuItems($menuId){

        $connection = $this->getConnection();

        $select = $connection->select()->from(
            ['mi' => $this->getTable('ves_megamenu_item')]
        )->where(
            'mi.menu_id = ?',
            (int)$menuId
        )->where(
            'mi.is_active = ?',
            \Vnecoms\Megamenu\Model\Item::STATUS_ENABLED
        )->order('mi.sort_order ASC');

        return $connection->fetchAll($select);
    }

    public function setPkAutoIncrement($value = true){
        $this->_isPkAutoIncrement = $value;
    }


    /**
     * Set store model
     *
     * @param Store $store
     * @return $this
     */
    public function setStore($store)
    {
        $this->_store = $store;
        return $this;
    }

    /**
     * Retrieve store model
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore($this->_store);
    }
}
