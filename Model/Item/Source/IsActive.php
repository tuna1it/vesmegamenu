<?php

namespace Vnecoms\Megamenu\Model\Item\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * @var \Vnecoms\Megamenu\Model\Item
     */
    protected $modelItem;

    /**
     * Constructor
     *
     * @param \Vnecoms\Megamenu\Model\Item $modelItem
     */
    public function __construct(\Vnecoms\Megamenu\Model\Item $modelItem)
    {
        $this->modelItem = $modelItem;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->modelItem->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
