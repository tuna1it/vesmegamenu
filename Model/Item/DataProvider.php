<?php

namespace Vnecoms\Megamenu\Model\Item;

use Vnecoms\Megamenu\Model\Menu;
use Vnecoms\Megamenu\Model\Item;
use Vnecoms\Megamenu\Model\ResourceModel\Item\Collection;
use Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Store\Model\Store;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

/**
 * DataProvider for item form
 */
class DataProvider extends AbstractDataProvider
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var array
     */
    protected $loadedData;


    /**
     * @var string
     */
    protected $requestScopeFieldName = 'store';


    /** @var  \Vnecoms\Megamenu\Model\MenuFactory */
    protected $_menuFactory;

    /** @var  \Vnecoms\Megamenu\Model\ItemFactory */
    protected $_itemFactory;

    /** @var Image  */
    protected $imageModel;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Vnecoms\Megamenu\Model\MenuFactory $menuFactory
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param Image $imageModel
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\RequestInterface $request,
        \Vnecoms\Megamenu\Model\MenuFactory $menuFactory,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        \Vnecoms\Megamenu\Model\Item\Image $imageModel,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create(); //Set collection
        $this->registry = $registry;
        $this->request = $request;
        $this->_menuFactory = $menuFactory;
        $this->_itemFactory = $itemFactory;
        $this->imageModel = $imageModel;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->meta = $this->prepareMeta($this->meta);
    }

    /**
     * Get data from provider and populate
     * @param void
     * @return array
     */
    public function getData()
    {

        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $menuId = $this->dataPersistor->get('current_menu_id');

        $items = $this->collection->addFieldToFilter('menu_id', $menuId)->getItems();

        if ($items) {

            /** @var Item $item */
            foreach ($items as $item) {
                $_data = $item->getData();
                $item->load($item->getId());

                if (isset($_data['icon_image'])) {
                    $icon = [];
                    $icon[0]['name'] = $item->getIconImage();
                    $icon[0]['url'] = $this->imageModel->getMediaUrl().'item/image'.$item->getIconImage();
                    $_data['icon_image'] = $icon;
                }
                if (isset($_data['menu_id'])) {
                    $_data['menu_id'] = $item['menu_id'];
                }
                if (isset($_data['parent_id'])) {
                    $_data['parent_id'] = $item['parent_id'];
                }
                $item->setData($_data);
                $this->loadedData[$item->getId()] = $_data;

            }
        } else {
            $parentId = $this->request->getParam('parent_id');
            $data = $this->dataPersistor->get('init_data');
            if (isset($parentId)) {
                $data['parent_id'] = $parentId;
            }

            if (!empty($data)) {
                $item = $this->collection->getNewEmptyItem();
                /** @var Item $item */
                $item->setData('menu_id', $menuId);
                $item->setData('parent_id', $data['parent_id']);
                $this->loadedData[$item->getId()] = $item->getData();
            }

        }

        return $this->loadedData;
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }


}
