<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 10/11/2016
 * Time: 10:07
 */

namespace Vnecoms\Megamenu\Model;


use Magento\Catalog\Api\ProductRepositoryInterface;

class Product extends \Magento\Framework\DataObject
{

    /** @var ProductRepositoryInterface  */
    protected $productRepository;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Reports\Model\ResourceModel\Product\CollectionFactory
     */
    protected $reportCollection;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Product constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $reportCollection
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $reportCollection,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ){
        $this->localeDate = $localeDate;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->reportCollection = $reportCollection;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->storeManager = $storeManager;
        parent::__construct($data);
    }

    /**
     * New arrival product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getNewarrivalProducts($config)
    {
        $todayStartOfDayDate = $this->localeDate->date()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $todayEndOfDayDate = $this->localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()->addAttributeToFilter(
            'news_from_date',
            [
                'or' => [
                    0 => ['date' => true, 'to' => $todayEndOfDayDate],
                    1 => ['is' => new \Zend_Db_Expr('null')],
                ]
            ],
            'left'
        )->addAttributeToFilter(
            'news_to_date',
            [
                'or' => [
                    0 => ['date' => true, 'from' => $todayStartOfDayDate],
                    1 => ['is' => new \Zend_Db_Expr('null')],
                ]
            ],
            'left'
        )->addAttributeToFilter(
            [
                ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                ['attribute' => 'news_to_date', 'is' => new \Zend_Db_Expr('not null')],
            ]
        )->addAttributeToSort(
            'news_from_date',
            'desc'
        )
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        return $collection;
    }

    /**
     * Latest product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getLatestProducts($config)
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        return $collection;
    }

    /**
     * Best seller product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getBestsellerProducts($config)
    {
        $storeId = $this->_storeManager->getStore(true)->getId();
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()
            ->joinField(
                'qty_ordered',
                'sales_bestsellers_aggregated_monthly',
                'qty_ordered',
                'product_id=entity_id',
                'at_qty_ordered.store_id=' . (int)$storeId,
                'at_qty_ordered.qty_ordered > 0',
                'left'
            )
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        return $collection;
    }

    /**
     * Random product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getRandomProducts($config)
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        $collection->getSelect()->order('rand()');
        return $collection;
    }

    /**
     * Top rated product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getTopratedProducts($config)
    {
        $storeId = $this->_storeManager->getStore(true)->getId();
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()
            ->joinField(
                'ves_review',
                'review_entity_summary',
                'reviews_count',
                'entity_pk_value=entity_id',
                'at_ves_review.store_id=' . (int)$storeId,
                'ves_review > 0',
                'left'
            )
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        $collection->getSelect()->order('ves_review DESC');
        return $collection;
    }

    /**
     * Speical product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getSpecialProducts($config)
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()
            ->addMinimalPrice()
            ->addUrlRewrite()
            ->addTaxPercents()
            ->addFinalPrice();
        $collection->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        $collection->getSelect()->where('price_index.final_price < price_index.price');
        return $collection;
    }

    /**
     * Most viewed product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getMostViewedProducts($config)
    {
        /** @var $collection \Magento\Reports\Model\ResourceModel\Product\CollectionFactory */
        $collection = $this->reportCollection->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addStoreFilter()
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);

        return $collection;
    }

    /**
     * Featured product collection
     * @param array $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    public function getFeaturedProducts($config)
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create()->addAttributeToSelect('*');
        if(is_array($config['cats']) && !empty($config['cats'])){
            $collection->addFieldToFilter('visibility', array(
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
                \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG
            ))
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addFinalPrice();
            $collection ->joinTable(
                'catalog_category_product',
                'product_id=entity_id',
                array('category_id'=>'category_id'),
                null,
                'left')
                ->addAttributeToFilter( array( array('attribute' => 'category_id', 'in' => array('finset' => $config['cats']))))
                ->groupByAttribute('entity_id');
        }
        $collection->addAttributeToFilter(array(array( 'attribute'=>'featured', 'eq' => '1')))
            ->addStoreFilter()
            ->setPageSize(isset($config['pagesize'])?$config['pagesize']:5)
            ->setCurPage(isset($config['curpage'])?$config['curpage']:1);
        return $collection;
    }

    /**
     * @param $source_key
     * @param $config
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|\Magento\Framework\Data\Collection|Object|
     */
    public function getProductBySource($source_key, $config)
    {
        $collection = '';
        switch ($source_key) {
            case 'latest':
                $collection = $this->getLatestProducts($config);
                break;
            case 'new_arrivals':
                $collection = $this->getNewarrivalProducts($config);
                break;
            case 'special':
                $collection = $this->getSpecialProducts($config);
                break;
            case 'most_popular':
                $collection = $this->getMostViewedProducts($config);
                break;
            case 'best_seller':
                $collection = $this->getBestsellerProducts($config);
                break;
            case 'top_rated':
                $collection = $this->getTopratedProducts($config);
                break;
            case 'random':
                $collection = $this->getRandomProducts($config);
                break;
            case 'featured':
                $collection = $this->getFeaturedProducts($config);
                break;
        }
        return $collection;
    }
}
