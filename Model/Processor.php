<?php
/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 *
 */

namespace Vnecoms\Megamenu\Model;

class Processor
{
    /** @var \Magento\Cms\Model\Template\FilterProvider  */
    protected $_filterProvider;

    /** @var \Magento\Cms\Model\BlockFactory  */
    protected $_blockFactory;

    /**
     * Processor constructor.
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     */
    public function __construct(
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Cms\Model\BlockFactory $blockFactory
    ) {
        $this->_filterProvider = $filterProvider;
        $this->_blockFactory = $blockFactory;
    }

    /**
     * @param $content
     * @return string
     */
    public function filter($content){
        return $this->_filterProvider->getBlockFilter()->filter($content);
    }
}
