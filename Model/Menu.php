<?php

namespace Vnecoms\Megamenu\Model;

use Vnecoms\Megamenu\Api\Data\MenuInterface;
use Vnecoms\Megamenu\Model\ResourceModel\Menu as ResourceMenu;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
/**
 * Class Model Menu
 * @package Vnecoms\Megamenu\Model
 * @method ResourceMenu _getResource()
 * @method ResourceMenu getResource()
 * @method Menu setStoreId(array $storeId)
 * @method array getStoreId()
 */
class Menu extends AbstractModel implements MenuInterface, IdentityInterface
{

    /**
     * CMS block cache tag
     */
    const CACHE_TAG = 'vesmegamenu_menu';

    /**
     * @var string
     */
    protected $_cacheTag = 'vesmegamenu_menu';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'vesmegamenu_menu';


    /**
     * @var int
     */
	const STATUS_ENABLED = 1;

    /**
     * @var int
     */
    const STATUS_DISABLED = 0;


    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /**
     * URL Model instance
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /** @var ResourceMenu  */
    protected $_resource;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /** @var   */
    protected $_store;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Vnecoms\Megamenu\Model\ResourceModel\Menu $resource = null,
        \Vnecoms\Megamenu\Model\ResourceModel\Menu\Collection $resourceCollection = null,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_url = $url;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_resource = $resource;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vnecoms\Megamenu\Model\ResourceModel\Menu');
    }

    /**
     * Load object data
     *
     * @param int|null $modelId
     * @param null|string $field
     * @return $this
     */
    public function load($modelId, $field = null)
    {
        $this->_beforeLoad($modelId, $field);
        $store = $this->getStore();
        $this->_getResource()->setStore($store)->load($this, $modelId, $field);
        $this->_afterLoad();
        $this->setOrigData();
        $this->_hasDataChanges = false;
        $this->updateStoredData();
        return $this;
    }


    /**
     * Synchronize object's stored data with the actual data
     *
     * @return $this
     */
    private function updateStoredData()
    {
        if (isset($this->_data)) {
            $this->storedData = $this->_data;
        } else {
            $this->storedData = [];
        }
        return $this;
    }

    /**
     * Get customer group ids
     *
     * @param void
     * @return array|null
     */
    public function getCustomerGroupIds()
    {
        if (!$this->hasCustomerGroupIds()) {
            $customerGroupIds = $this->_getResource()->getCustomerGroupIds($this->getId());
            $this->setData('customer_group_ids', (array)$customerGroupIds);
        }
        return $this->_getData('customer_group_ids');
    }


    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Get desktop template
     *
     * @param void
     * @return array
     */
    public function getDesktopTemplate()
    {
        return $this->getData(MenuInterface::DESKTOP_TEMPLATE);
    }

    /**
     * @param $template
     * @return $this
     */
    public function setDesktopTemplate($template)
    {
        return $this->setData(MenuInterface::DESKTOP_TEMPLATE, $template);

    }

    /**
     * Get desktop template
     *
     * @param void
     * @return array
     */
    public function getMobileTemplate()
    {
        return $this->getData(MenuInterface::MOBILE_TEMPLATE);
    }

    /**
     * @param $mobileTemplate
     * @return $this
     */
    public function setMobileTemplate($mobileTemplate)
    {
        return $this->setData(MenuInterface::MOBILE_TEMPLATE, $mobileTemplate);

    }

    /**
     * @return string
     */
    public function getAnimationType()
    {
        return $this->getData(MenuInterface::ANIMATION_TYPE);
    }

    /**
     * @param $animation
     * @return MenuInterface
     */
    public function setAnimationType($animation)
    {
        return $this->setData(MenuInterface::ANIMATION_TYPE, $animation);
    }

    /**
     * Retrieve menu id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(MenuInterface::MENU_ID);

    }

    /**
     * @param int|mixed $id
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setId($id)
    {
        return $this->setData(MenuInterface::MENU_ID, $id);
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
       return $this->getData(MenuInterface::IDENTIFIER);
    }

    /**
     * @param string $identifier
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setIdentifier($identifier)
    {
        $this->setData(MenuInterface::IDENTIFIER, $identifier);
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(MenuInterface::TITLE);
    }

    /**
     * @param string $title
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setTitle($title)
    {
        $this->setData(MenuInterface::TITLE, $title);
        return $this;
    }

    /**
     * @return bool
     */
    public function IsActive()
    {
        return (bool)$this->getData(MenuInterface::IS_ACTIVE);
    }

    /**
     * @param bool|int $isActive
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(MenuInterface::IS_ACTIVE, $isActive);
    }

    /**
     * @return string
     */
    public function getCreationTime()
    {
        return $this->getData(MenuInterface::CREATION_TIME);
    }

    /**
     * @param string $creationTime
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(MenuInterface::CREATION_TIME, $creationTime);

    }

    /**
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->getData(MenuInterface::UPDATE_TIME);
    }

    /**
     * @param string $updateTime
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(MenuInterface::UPDATE_TIME, $updateTime);
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Set description
     *
     * @param string $description
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get sort order
     *
     * @return string
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * Set sort order
     *
     * @param string $sortOrder
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * @return string
     */
    public function getStructure()
    {
        return $this->getData(self::STRUCTURE);
    }

    /**
     * @param $structure
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     */
    public function setStructure($structure)
    {
        return $this->setData(self::STRUCTURE, $structure);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        $rs = [];
        $options = $this->_getResource()->getOptions();
        if ($options){
            foreach ($options as $option){
                $rs[$option['menu_id']] = $option['title'];
            }
        }

        return $rs;
    }

    /**
     * @param null $menuId
     * @return mixed
     */
    public function getItems($menuId = null){
        $id = ($menuId) ? $menuId : $this->getId();
        return $this->_getResource()->getItems($id);
    }

    /**
     * @param $identifier
     * @param null $storeId
     * @return mixed
     */
    public function getMenuIdByIdentifier($identifier, $storeId = null){
        $rsModel = $this->_getResource();
        if ($storeId) {
            $rsModel->setStore($storeId);
        }
        $id = $rsModel->getMenuIdByIdentifier($identifier);
        return $id;
    }

    /**
     * @return mixed
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }


    /**
     * Set store model
     *
     * @param \Magento\Store\Model\Store $store
     * @return $this
     */
    public function setStore($store)
    {
        $this->_store = $store;
        return $this;
    }

    /**
     * Retrieve store model
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        if($this->_store){
            return $this->_storeManager->getStore();
        }else{
            return false;
        }
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


}
