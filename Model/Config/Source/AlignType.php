<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class AlignType
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class AlignType implements OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('From left menu'),
                'value' => '1',
            ];
        $options[] = [
                'label' => __('From right menu'),
                'value' => '2',
        ];
        $options[] = [
                'label' => __('From left item'),
                'value' => '3',
        ];
        $options[] = [
                'label' => __('From right item'),
                'value' => '4',
        ];
        return $options;
    }
}
