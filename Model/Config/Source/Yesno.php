<?php

namespace Vnecoms\Megamenu\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Yesno
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class Yesno implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('Yes'),
                'value' => 1,
            ];
        $options[] = [
                'label' => __('No'),
                'value' => 0,
            ];
        return $options;
    }
}
