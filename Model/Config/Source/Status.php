<?php

namespace Vnecoms\Megamenu\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class Status implements OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('Enabled'),
                'value' => 1,
            ];
        $options[] = [
                'label' => __('Disabled'),
                'value' => 0,
            ];
        return $options;
    }
}



