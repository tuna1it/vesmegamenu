<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

/**
 * Class LayoutType
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class LayoutType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $layouts = [];
        $layouts[] = [
        'value' => 'owl_carousel',
        'label' => 'OWL Carousel'];
        $layouts[] = [
        'value' => 'bootstrap_carousel',
        'label' => 'Bootstrap Carousel'];
        $layouts[] = [
            'value' => 'jcarousel',
            'label' => 'Basic JCarousel'];
        return $layouts;
    }
}