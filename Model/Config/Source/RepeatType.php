<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class RepeatType
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class RepeatType implements OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('No Repeat'),
                'value' => 'no-repeat',
        ];
        $options[] = [
                'label' => __('Repeat X'),
                'value' => 'repeat-x',
        ];
        $options[] = [
                'label' => __('Repeat Y'),
                'value' => 'repeat-y',
        ];
        $options[] = [
                'label' => __('Round'),
                'value' => 'round',
        ];
        $options[] = [
                'label' => __('Space'),
                'value' => 'space',
        ];
        return $options;
    }
}
