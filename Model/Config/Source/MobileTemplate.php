<?php

namespace Vnecoms\Megamenu\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class MobileTemplate
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class MobileTemplate implements OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('Off Canvas'),
                'value' => 1,
            ];
        $options[] = [
                'label' => __('Tree Menu'),
                'value' => 0,
            ];
        return $options;
    }
}
