<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

/**
 * Class Menu
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class Menu implements \Magento\Framework\Option\ArrayInterface
{
    /** @var  \Magento\Backend\Model\Session */
    protected $_session;

    /**
     * @var \Vnecoms\Megamenu\Model\Menu
     */
	protected  $_menu;

    public function __construct(
    	\Vnecoms\Megamenu\Model\Menu $menu,
        \Magento\Backend\Model\Session $session
    	) {
    	$this->_menu = $menu;
        $this->_session = $session;
    }

    /**
     * @return array
     */
	public function toOptionArray()
	{
		$data = [];
		/** @var \Vnecoms\Megamenu\Model\ResourceModel\Menu\Collection $collection */
        $menuId = $this->_session->getMenuId();
        //echo $menuId;exit;
		$collection = $this->_menu->getCollection();
		/** @var \Vnecoms\Megamenu\Model\Menu $menu */
		foreach ($collection as $menu) {
			$data[] = [
				'value' => $menu->getId(),
				'label' => $menu->getTitle()
			];
		}
		return $data;
	}

}
