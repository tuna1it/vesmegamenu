<?php

namespace Vnecoms\Megamenu\Model\Config\Source\Menu;
/**
 * Class DesktopTemplate
 * @package Vnecoms\Megamenu\Model\Config\Source\Menu
 */
class DesktopTemplate implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        return [
            [
                'value' => 'horizontal',
                'label' => __('Horizontal Menu')
            ],
            [
                'value' => 'vertical-left',
                'label' => __('Vertical Menu Left')
            ],
            [
                'value' => 'vertical-right',
                'label' => __('Vertical Menu Right')
            ]
        ];
    }

}
