<?php

namespace Vnecoms\Megamenu\Model\Config\Source\Menu;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class MobileTemplate
 * @package Vnecoms\Megamenu\Model\Config\Source\Menu
 */
class MobileTemplate implements OptionSourceInterface
{
    public function toOptionArray()
    {

        return [
            [
                'value' => 0,
                'label' => __('Off Canvas Menu Left')
            ],
            [
                'value' => 1,
                'label' => __('Accordion Menu')
            ],
            [
                'value' => 2,
                'label' => __('Drill Down Menu')
            ]
        ];
    }
}
