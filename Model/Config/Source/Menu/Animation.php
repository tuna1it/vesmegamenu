<?php

namespace Vnecoms\Megamenu\Model\Config\Source\Menu;
/**
 * Class Event
 * @package Vnecoms\Megamenu\Model\Config\Source\Menu
 */
class Animation implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        return [
            [
                'value' => 'none',
                'label' => __('None')
            ],
            [
                'value' => 'hover',
                'label' => __('Hover')
            ],
            [
                'value' => 'click',
                'label' => __('Click')
            ]
        ];
    }

}
