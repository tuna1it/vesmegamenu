<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

/**
 * Class Event
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class Event implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('Hover'),
                'value' => 'hover',
            ];
        $options[] = [
                'label' => __('Click'),
                'value' => 'click',
        ];
        return $options;
    }
}
