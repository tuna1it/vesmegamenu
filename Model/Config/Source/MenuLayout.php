<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

/**
 * Class MenuLayout
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class MenuLayout implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('Vertical Menu'),
                'value' => 'vertical',
            ];
        $options[] = [
                'label' => __('Horizontal Menu'),
                'value' => 'horizontal',
        ];
        return $options;
    }
}
