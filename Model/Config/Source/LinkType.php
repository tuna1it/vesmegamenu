<?php

namespace Vnecoms\Megamenu\Model\Config\Source;


use \Vnecoms\Megamenu\Model\Item\Link;

/**
 * Class LinkType
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class LinkType implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('Custom Link'),
            'value' => Link::ITEM_CUSTOM_LINK
        ];
        $options[] = [
            'label' => __('Category Page'),
            'value' => Link::ITEM_CATEGORY_LINK
        ];
        $options[] = [
            'label' => __('Cms Page'),
            'value' => Link::ITEM_CMS_PAGE_LINK
        ];
        return $options;
    }
}
