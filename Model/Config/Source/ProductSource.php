<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

/**
 * Class ProductSource
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class ProductSource implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Yes')],
            ['value' => 2, 'label' => __('No')],
            ['value' => 0, 'label' => __('Use General Config')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('No'), 1 => __('Yes')];
    }
}
