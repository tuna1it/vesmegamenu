<?php

namespace Vnecoms\Megamenu\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class MenuType
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class ContentType implements OptionSourceInterface
{
    const ITEM_CONTENT_ITEM_CHILD = 'child-items';
    const ITEM_CONTENT = 'content-html';
    const ITEM_CONTENT_DYNAMIC = 'dynamic-content';
    const ITEM_CONTENT_STATICBLOCKS = 'static-blocks';
    const ITEM_CONTENT_CATEGORY = 'category';


    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'value' => static::ITEM_CONTENT_ITEM_CHILD,
            'label' => __('Child Items')
        ];
        $options[] = [
            'value' => static::ITEM_CONTENT,
            'label' => __('Content')
        ];
        $options[] = [
            'value' => static::ITEM_CONTENT_DYNAMIC,
            'label' => __('Dynamic Content')
        ];
        $options[] = [
            'value' => static::ITEM_CONTENT_STATICBLOCKS,
            'label' => __('Static Blocks')
        ];
        $options[] = [
            'value' => static::ITEM_CONTENT_CATEGORY,
            'label' => __('Sub Category')
        ];

        return $options;
    }
}
