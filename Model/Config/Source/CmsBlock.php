<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 11/08/2016
 * Time: 15:45
 */

namespace Vnecoms\Megamenu\Model\Config\Source;

use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as PageCollection;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollection;

class CmsBlock extends \Vnecoms\Megamenu\Model\Config\Source\CmsPage
{

    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = $this->_blockCollectionFactory->create()->load()->toOptionArray();
            array_unshift($this->_options, ['value' => '', 'label' => __('Please select a static blocks.')]);
        }
        return $this->_options;
    }
    
}