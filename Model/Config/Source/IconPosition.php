<?php

namespace Vnecoms\Megamenu\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IconPosition
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class IconPosition implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
                'label' => __('Left'),
                'value' => 'left',
            ];
        $options[] = [
                'label' => __('Right'),
                'value' => 'right',
            ];
        return $options;
    }
}
