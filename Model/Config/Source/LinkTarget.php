<?php

namespace Vnecoms\Megamenu\Model\Config\Source;

/**
 * Class LinkTarget
 * @package Vnecoms\Megamenu\Model\Config\Source
 */
class LinkTarget implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('Load in a new window'),
            'value' => '_blank'
        ];
        $options[] = [
            'label' => __('Load in the same frame as it was clicked'),
            'value' => '_self'
        ];
        $options[] = [
            'label' => __('Load in the parent frameset'),
            'value' => '_parent'
        ];
        $options[] = [
            'label' => __('Load in the full body of the window'),
            'value' => '_top'
        ];
        return $options;
    }
}