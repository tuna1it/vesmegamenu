<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 11/08/2016
 * Time: 15:45
 */

namespace Vnecoms\Megamenu\Model\Config\Source;

use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as PageCollection;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollection;

class CmsPage extends \Magento\Catalog\Model\Category\Attribute\Source\Page
{

    /**
     * @var PageCollection
     */
    protected $_pageCollectionFactory;

    /**
     * @var BlockCollection
     */
    protected $_blockCollectionFactory;


    /**
     * CmsPage constructor.
     * @param PageCollection $pageCollectionFactory
     * @param BlockCollection $blockCollectionFactory
     */
    public function __construct(
        PageCollection $pageCollectionFactory,
        BlockCollection $blockCollectionFactory
    ){
        $this->_pageCollectionFactory = $pageCollectionFactory;
        $this->_blockCollectionFactory = $blockCollectionFactory;
    }


    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = $this->_pageCollectionFactory->create()->load()->toOptionIdArray();
            array_unshift($this->_options, ['value' => '', 'label' => __('Please select a page.')]);
        }
        return $this->_options;
    }
}