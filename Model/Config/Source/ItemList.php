<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 11/08/2016
 * Time: 16:04
 */

namespace Vnecoms\Megamenu\Model\Config\Source;

use Vnecoms\Megamenu\Model\ResourceModel\Item\Collection as ItemCollectionFactory;
use Vnecoms\Megamenu\Model\Item as ItemModel;
use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Vnecoms\Megamenu\Helper\Data as MegaHelper;
use Magento\Framework\Registry;
use Magento\Framework\App\Request\DataPersistorInterface;

class ItemList extends AbstractSource implements ArrayInterface
{
    /**
     * @var \Vnecoms\Megamenu\Model\Item
     */
    protected $_itemModel;

    /** @var  \Vnecoms\Megamenu\Helper\Data */
    protected $_magehelper;

    /** @var Registry  */
    protected $_coreRegistry;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    public function __construct(
        ItemModel $itemModel,
        MegaHelper $megahelper,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        array $options = []
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_itemModel = $itemModel;
        $this->_magehelper = $megahelper;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($options);
    }

    /**
     * get options as key value pair
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (count($this->options) == 0) {

            $menuId = $this->_coreRegistry->registry('current_menu_id');

            if ($menuId) {
                $items = $this->_itemModel->getCollection()
                    ->addFieldToSelect('item_id')
                    ->addFieldToSelect('title')
                    ->addFieldToSelect('parent_id')
                    ->addFieldToFilter('menu_id', $menuId)
                    ->setCurPage(1)->getData();
                $itemOptions = [];
                foreach ($items as $itemId => $item) {
                    //if (!$item['item_parent_id']) continue;
                    $itemOptions[] = [
                        'value' => $item['item_id'],
                        'label' => $item['title']
                    ];
                }
                $this->options = $itemOptions;
                array_unshift($this->options, ['value' => '0', 'label' => __('-- NONE --')]);
            }

        }
        return $this->options;
    }
}
