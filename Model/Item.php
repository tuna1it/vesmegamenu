<?php

namespace Vnecoms\Megamenu\Model;


use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Config\Definition\IntegerNode;
use Vnecoms\Megamenu\Api\Data\ItemInterface;
use Vnecoms\Megamenu\Model\ResourceModel\Item as ItemResourceModel;

/**
 * @method ItemResourceModel _getResource()
 * @method ItemResourceModel getResource()
 */
class Item extends AbstractModel implements IdentityInterface, ItemInterface
{
    /** @var int */
	const STATUS_ENABLED = 1;
    /** @var int */
    const STATUS_DISABLED = 0;

    const SHOW_TITLE_YES = 1;
    const SHOW_TITLE_NO = 0;

    const IS_GROUP_YES = 1;
    const IS_GROUP_NO = 0;

    const CACHE_TAG = 'ves_megamenu_item';

    /**
     * @var string
     */
    protected $_cacheTag = 'ves_megamenu_item';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ves_megamenu_item';

    protected $_eventObject = 'item';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Vnecoms\Megamenu\Model\ResourceModel\Item');
        $this->setIdFieldName('item_id');
    }

    /**
     * Prepare item's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];

    }


    /**
     * Get id item
     *
     * @param void
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ITEM_ID);
    }

    /**
     * Get Parent Item Id
     *
     * @param void
     * @return int
     */
    public function getParentId()
    {
        return $this->getData(self::PARENT_ID);
    }

    /**
     * Get Menu Id
     *
     * @param void
     * @return int
     */
    public function getMenuId()
    {
        return $this->getData(self::MENU_ID);
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get Show Title
     *
     * @return bool|null
     */
    public function isShowTitle()
    {
        return (bool)$this->getData(self::SHOW_TITLE);
    }

    /**
     * Get Show Content
     *
     * @return bool|null
     */
    public function isShowContent()
    {
        return (bool)$this->getData(self::SHOW_CONTENT);
    }

    /**
     * Get content type
     * @return string|null
     */
    public function getContentType()
    {
        return $this->getData(self::CONTENT_TYPE);
    }

    /**
     * @param $contentType
     * @return ItemInterface
     */
    public function setContentType($contentType)
    {
       return $this->setData(self::CONTENT_TYPE, $contentType);
    }

    /**
     * Get Icon Image
     *
     * @return string|null
     */
    public function getIconImage()
    {
        return $this->getData(self::ICON_IMAGE);
    }

    /**
     * Get Font Classes
     *
     * @return string|null
     */
    public function getFontClasses()
    {
        return $this->getData(self::FONT_CLASSES);
    }

    /**
     * Get Link Type
     *
     * @return string|null
     */
    public function getLinkType()
    {
        return $this->getData(self::LINK_TYPE);
    }

    /**
     * Get Link
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->getData(self::LINK);
    }

    /**
     * Get Link Target
     *
     * @return string|null
     */
    public function getLinkTarget()
    {
        return $this->getData(self::LINK_TARGET);
    }

    /**
     * Get Category ID
     *
     * @return int|null
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }

    /**
     * Get Show Number Product
     *
     * @return bool|null
     */
    public function isShowNumberProduct()
    {
        return $this->getData(self::SHOW_NUMBER_PRODUCT);
    }

    /**
     * Get CMS Page
     *
     * @return string|null
     */
    public function getCmsPage()
    {
        return $this->getData(self::CMS_PAGE);
    }

    /**
     * Get Is Group
     *
     * @return bool|null
     */
    public function isGroup()
    {
        return (bool)$this->getData(self::IS_GROUP);
    }

    /**
     * Get Mega Cols
     *
     * @return int|null
     */
    public function getMegaCols()
    {
        return $this->getData(self::MEGA_COLS);
    }

    /**
     * Get Content Width
     *
     * @return int|null
     */
    public function getContentWidth()
    {
        return $this->getData(self::CONTENT_WIDTH);
    }

    /**
     * Get Sub Width
     *
     * @return int|null
     */
    public function getSubWidth()
    {
        return $this->getData(self::SUB_WIDTH);
    }

    /**
     * Get Left Side Width
     *
     * @return int|null
     */
    public function getLeftSideWidth()
    {
        return $this->getData(self::LEFT_SIDEBAR_WIDTH);
    }

    /**
     * Get Right Side Width
     *
     * @return int|null
     */
    public function getRightSideWidth()
    {
        return $this->getData(self::RIGHT_SIDEBAR_WIDTH);
    }


    /**
     * Get Custom Content
     *
     * @return string|null
     */
    public function getContentHtml()
    {
        return $this->getData(self::CONTENT_HTML);
    }

    /**
     * Get Static Blocks
     *
     * @return string|null
     */
    public function getStaticBlocks()
    {
        return $this->getData(self::STATIC_BLOCKS);
    }


    /**
     * Get Visible Option
     *
     * @return bool|null
     */
    public function getVisibleOption()
    {
        return $this->getData(self::VISIBLE_OPTION);
    }

    /**
     * Get Visible In
     *
     * @return string|null
     */
    public function getVisibleIn()
    {
        return $this->getData(self::VISIBLE_IN);
    }

    /**
     * Get Addition Class
     *
     * @return string|null
     */
    public function getAdditionClass()
    {
        return $this->getData(self::ADDITION_CLASS);
    }


    /**
     * Get creation time
     *
     * @return string
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Get sort order
     *
     * @return string
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ITEM_ID, $id);
    }

    /**
     * Set parent id
     *
     * @param string $parentId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setParentId($parentId)
    {
        return $this->setData(self::PARENT_ID, $parentId);
    }

    /**
     * Set menu id
     *
     * @param string $menuId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setMenuId($menuId)
    {
        return $this->setData(self::MENU_ID, $menuId);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set Show Title
     *
     * @param int $isShowTitle
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsShowTitle($isShowTitle)
    {
        return $this->setData(self::SHOW_TITLE, $isShowTitle);
    }

    /**
     * Set Show Content
     *
     * @param int $isShowContent
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsShowContent($isShowContent)
    {
        return $this->setData(self::SHOW_CONTENT, $isShowContent);
    }

    /**
     * Set Icon Image
     *
     * @param string $iconImage
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIconImage($iconImage)
    {
        return $this->setData(self::ICON_IMAGE, $iconImage);
    }

    /**
     * Set Font Classes
     *
     * @param string $fontClasses
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setFontClasses($fontClasses)
    {
        return $this->setData(self::FONT_CLASSES, $fontClasses);
    }

    /**
     * Set Link Type
     *
     * @param string $linkType
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLinkType($linkType)
    {
        return $this->setData(self::LINK_TYPE, $linkType);
    }

    /**
     * Set Link
     *
     * @param string $link
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLink($link)
    {
        return $this->setData(self::LINK, $link);
    }

    /**
     * Set Link Target
     *
     * @param string $linkTarget
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLinkTarget($linkTarget)
    {
        return $this->setData(self::LINK_TARGET, $linkTarget);
    }

    /**
     * Set Category ID
     *
     * @param string $categoryId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    /**
     * Set Show Number Product
     *
     * @param int $isShowNumberProduct
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsShowNumberProduct($isShowNumberProduct)
    {
        return $this->setData(self::SHOW_NUMBER_PRODUCT, $isShowNumberProduct);
    }

    /**
     * Set CMS page
     *
     * @param string $cmsPage
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setCmsPage($cmsPage)
    {
        return $this->setData(self::CMS_PAGE, $cmsPage);
    }

    /**
     * Set Is Group
     *
     * @param string $isGroup
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsGroup($isGroup)
    {
        return $this->setData(self::IS_GROUP, $isGroup);
    }

    /**
     * Set Mega Cols
     *
     * @param string $megaCols
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setMegaCols($megaCols)
    {
        return $this->setData(self::MEGA_COLS, $megaCols);
    }

    /**
     * Set Content Width
     *
     * @param string $contentWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setContentWidth($contentWidth)
    {
        return $this->setData(self::CONTENT_WIDTH, $contentWidth);
    }

    /**
     * Set Sub Width
     *
     * @param string $subWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setSubWidth($subWidth)
    {
        return $this->setData(self::SUB_WIDTH, $subWidth);
    }

    /**
     * Set Left Side Width
     *
     * @param string $leftWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setLeftSideWidth($leftWidth)
    {
        return $this->setData(self::LEFT_SIDEBAR_WIDTH, $leftWidth);
    }

    /**
     * Set Right Side Width
     *
     * @param string $rightWidth
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setRightSideWidth($rightWidth)
    {
        return $this->setData(self::RIGHT_SIDEBAR_WIDTH, $rightWidth);
    }

    /**
     * Set Content Html
     *
     * @param string $contentHtml
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setContentHtml($contentHtml)
    {
        return $this->setData(self::CONTENT_HTML, $contentHtml);
    }

    /**
     * Set Static Blocks
     *
     * @param string $staticBlocks
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setStaticBlocks($staticBlocks)
    {
        return $this->setData(self::STATIC_BLOCKS, $staticBlocks);
    }

    /**
     * Set Visible Option
     *
     * @param bool $visibleOption
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setVisibleOption($visibleOption)
    {
        return $this->setData(self::VISIBLE_OPTION, $visibleOption);
    }

    /**
     * Set Visible In
     *
     * @param string $visibleIn
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setVisibleIn($visibleIn)
    {
        return $this->setData(self::VISIBLE_IN, $visibleIn);
    }

    /**
     * Set Addition Class
     *
     * @param string $additionClass
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setAdditionClass($additionClass)
    {
        return $this->setData(self::ADDITION_CLASS, $additionClass);
    }


    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Set sort order
     *
     * @param string $sortOrder
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }


    /**
     * Set store
     *
     * @param $storeId
     * @return ItemInterface
     */
    public function setStoreId($storeId)
    {
        $this->setData(ItemInterface::STORE_ID, $storeId);
        return $this;
    }

    /**
     * Get store
     *
     * @return array
     */
    public function getStoreId()
    {
        return $this->getData(ItemInterface::STORE_ID);
    }

    /**
     * Get animation type
     * @return string|null
     */
    public function getAnimationIn()
    {
        return $this->getData(ItemInterface::ANIMATION_IN);
    }

    /**
     * @param $animation
     * @return ItemInterface
     */
    public function setAnimationIn($animation)
    {
        return $this->setData(ItemInterface::ANIMATION_IN, $animation);
    }

    /**
     * Get animation time
     * @return string|null
     */
    public function getAnimationTime()
    {
        return $this->getData(ItemInterface::ANIMATION_TIME);
    }

    /**
     * @param $animationTime
     * @return ItemInterface
     */
    public function setAnimationTime($animationTime)
    {
        return $this->setData(ItemInterface::ANIMATION_TIME, $animationTime);
    }

    /**
     * Get align submenu
     * @return string|null
     */
    public function getAlign()
    {
        return $this->getData(ItemInterface::ALIGN);
    }

    /**
     * @param $align
     * @return ItemInterface
     */
    public function setAlign($align)
    {
        return $this->setData(ItemInterface::ALIGN, $align);
    }

    /**
     * Get dropdown inline style
     * @return string|null
     */
    public function getDropdownInlinceCss()
    {
        return $this->getData(ItemInterface::DROPDOWN_INLINECSS);
    }

    /**
     * @param $inline
     * @return ItemInterface
     */
    public function setDropdownInlineCss($inline)
    {
        return $this->setData(ItemInterface::DROPDOWN_INLINECSS, $inline);
    }

    /**
     * Get dropdown inline style
     * @return string|null
     */
    public function getShowIcon()
    {
        return $this->getData(ItemInterface::SHOW_ICON);
    }

    /**
     * @param $showIcon
     * @return ItemInterface
     */
    public function setShowIcon($showIcon)
    {
        return $this->setData(ItemInterface::SHOW_ICON, $showIcon);
    }

    /**
     * Get header html
     * @return string|null
     */
    public function getHeaderHtml()
    {
        return $this->getData(ItemInterface::HEADER_HTML);
    }

    /**
     * @param $showIcon
     * @return ItemInterface
     */
    public function setHeaderHtml($header)
    {
        return $this->setData(ItemInterface::HEADER_HTML, $header);
    }

    /**
     * Get header html
     * @return string|null
     */
    public function getLeftSideHtml()
    {
        return $this->getData(ItemInterface::LEFT_SIDEBAR_HTML);
    }

    /**
     * Set left side html
     * @param $left
     * @return ItemInterface
     */
    public function setLeftSideHtml($left)
    {
        return $this->setData(ItemInterface::LEFT_SIDEBAR_HTML, $left);
    }

    /**
     * Get right side html
     * @return string|null
     */
    public function getRightSideHtml()
    {
        return $this->getData(ItemInterface::RIGHT_SIDEBAR_HTML);
    }

    /**
     * Set right side html
     * @param $right
     * @return ItemInterface
     */
    public function setRightSideHtml($right)
    {
        return $this->setData(ItemInterface::RIGHT_SIDEBAR_HTML, $right);
    }

    /**
     * Get footer html
     * @return string|null
     */
    public function getFooterHtml()
    {
        return $this->getData(ItemInterface::FOOTER_HTML);
    }

    /**
     * Set footer html
     * @param $footer
     * @return ItemInterface
     */
    public function setFooterHtml($footer)
    {
        return $this->setData(ItemInterface::FOOTER_HTML, $footer);
    }
}
