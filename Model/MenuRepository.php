<?php

namespace Vnecoms\Megamenu\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Vnecoms\Megamenu\Api\MenuRepositoryInterface;
use Vnecoms\Megamenu\Api\Data;
use Vnecoms\Megamenu\Api\Data\MenuInterface;
use Vnecoms\Megamenu\Api\Data\MenuInterfaceFactory;
use Vnecoms\Megamenu\Api\Data\MenuSearchResultsInterfaceFactory;
use Vnecoms\Megamenu\Model\ResourceModel\Menu as ResourceMenu;
use Vnecoms\Megamenu\Model\ResourceModel\Menu\Collection;
use Vnecoms\Megamenu\Model\ResourceModel\Menu\CollectionFactory as MenuCollectionFactory;

/**
 * Class MenuRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class MenuRepository implements MenuRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];
    /**
     * @var ResourceMenu
     */
    protected $resource;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var MenuCollectionFactory
     */
    protected $menuCollectionFactory;
    /**
     * @var MenuSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var MenuInterfaceFactory
     */
    protected $menuInterfaceFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * MenuRepository constructor.
     * @param ResourceMenu $resource
     * @param StoreManagerInterface $storeManager
     * @param MenuCollectionFactory $menuCollectionFactory
     * @param MenuSearchResultsInterfaceFactory $menuSearchResultsInterfaceFactory
     * @param MenuInterfaceFactory $menuInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        ResourceMenu $resource,
        StoreManagerInterface $storeManager,
        MenuCollectionFactory $menuCollectionFactory,
        MenuSearchResultsInterfaceFactory $menuSearchResultsInterfaceFactory,
        MenuInterfaceFactory $menuInterfaceFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->resource                 = $resource;
        $this->storeManager             = $storeManager;
        $this->menuCollectionFactory    = $menuCollectionFactory;
        $this->searchResultsFactory     = $menuSearchResultsInterfaceFactory;
        $this->menuInterfaceFactory     = $menuInterfaceFactory;
        $this->dataObjectHelper         = $dataObjectHelper;
    }
    /**
     * Save page.
     *
     * @param \Vnecoms\Megamenu\Api\Data\MenuInterface $menu
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(MenuInterface $menu)
    {
        if (empty($menu->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $menu->setStoreId($storeId);
        }
        try {
            $this->resource->save($menu);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the menu: %1',
                $exception->getMessage()
            ));
        }
        return $menu;
    }

    /**
     * Retrieve Menu.
     *
     * @param int $menuId
     * @return \Vnecoms\Megamenu\Api\Data\MenuInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($menuId)
    {
        if (!isset($this->instances[$menuId])) {
            /** @var \Vnecoms\Megamenu\Api\Data\MenuInterface|\Magento\Framework\Model\AbstractModel $menu */
            $menu = $this->menuInterfaceFactory->create();
            $this->resource->load($menu, $menuId);
            if (!$menu->getId()) {
                throw new NoSuchEntityException(__('Requested menu doesn\'t exist'));
            }
            $this->instances[$menuId] = $menu;
        }
        return $this->instances[$menuId];
    }

    /**
     * Retrieve pages matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Vnecoms\Megamenu\Api\Data\MenuSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Vnecoms\Megamenu\Api\Data\MenuSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Vnecoms\Megamenu\Model\ResourceModel\Menu\Collection $collection */
        $collection = $this->menuCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'menu_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Vnecoms\Megamenu\Api\Data\MenuInterface[] $menus */
        $menus = [];
        /** @var \Vnecoms\Megamenu\Model\Menu $menu */
        foreach ($collection as $menu) {
            /** @var \Vnecoms\Megamenu\Api\Data\MenuInterface $menuDataObject */
            $menuDataObject = $this->menuInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($menuDataObject, $menu->getData(), MenuInterface::class);
            $menus[] = $menuDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($menus);
    }

    /**
     * Delete menu.
     *
     * @param \Vnecoms\Megamenu\Api\Data\MenuInterface $menu
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(MenuInterface $menu)
    {
        /** @var \Vnecoms\Megamenu\Api\Data\MenuInterface|\Vnecoms\Megamenu\Model\Menu $menu */
        $id = $menu->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($menu);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove menu %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete menu by ID.
     *
     * @param int $menuId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($menuId)
    {
        $menu = $this->getById($menuId);
        return $this->delete($menu);
    }
    

}
