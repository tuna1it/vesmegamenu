<?php

namespace Vnecoms\Megamenu\Model;

/**
 * Class AbstractProduct
 * @package Vnecoms\Megamenu\Model
 */
class AbstractProduct extends \Magento\Framework\DataObject
{

    /** @var  \Magento\Catalog\Model\Product */
    protected $_product;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /** @var \Magento\Catalog\Api\StockItemRepositoryInterface  */
    protected $_stockItemRepository;


    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Catalog\Api\StockItemRepositoryInterface $stockItemRepository,
        \Magento\Catalog\Model\Product $product,
        array $data = []
    ){
        $this->_product = $product;
        $this->_localeDate = $localeDate;
        $this->_storeManager = $storeManager;
        $this->_stockItemRepository = $stockItemRepository;
        parent::__construct($data);
    }

    public function getStockProductInformation()
    {
        $stockStatus = $this->_product->getQuantityAndStockStatus()->getIsInStock();
        return $this->_stockItemRepository->get($stockStatus);
    }

}



