<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\Megamenu\Model\Locator;

use Magento\Store\Api\Data\StoreInterface;

/**
 * Interface LocatorInterface
 */
interface LocatorInterface
{

    /**
     * @return StoreInterface
     */
    public function getStore();


}
