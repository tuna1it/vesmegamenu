<?php

namespace Vnecoms\Megamenu\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Vnecoms\Megamenu\Api\ItemRepositoryInterface;
use Vnecoms\Megamenu\Api\Data;
use Vnecoms\Megamenu\Api\Data\ItemInterface;
use Vnecoms\Megamenu\Api\Data\ItemInterfaceFactory;
use Vnecoms\Megamenu\Api\Data\ItemSearchResultsInterfaceFactory;
use Vnecoms\Megamenu\Model\ResourceModel\Item as ResourceItem;
use Vnecoms\Megamenu\Model\ResourceModel\Item\Collection;
use Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory as ItemCollectionFactory;

/**
 * Class ItemRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ItemRepository implements ItemRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];
    /**
     * @var ResourceItem
     */
    protected $resource;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var ItemCollectionFactory
     */
    protected $itemCollectionFactory;
    /**
     * @var ItemSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var ItemInterfaceFactory
     */
    protected $itemInterfaceFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * ItemRepository constructor.
     * @param ResourceItem $resource
     * @param StoreManagerInterface $storeManager
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param ItemSearchResultsInterfaceFactory $itemSearchResultsInterfaceFactory
     * @param ItemInterfaceFactory $itemInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        ResourceItem $resource,
        StoreManagerInterface $storeManager,
        ItemCollectionFactory $itemCollectionFactory,
        ItemSearchResultsInterfaceFactory $itemSearchResultsInterfaceFactory,
        ItemInterfaceFactory $itemInterfaceFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->resource                 = $resource;
        $this->storeManager             = $storeManager;
        $this->itemCollectionFactory    = $itemCollectionFactory;
        $this->searchResultsFactory     = $itemSearchResultsInterfaceFactory;
        $this->itemInterfaceFactory     = $itemInterfaceFactory;
        $this->dataObjectHelper         = $dataObjectHelper;
    }
    /**
     * Save page.
     *
     * @param \Vnecoms\Megamenu\Api\Data\ItemInterface $item
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ItemInterface $item)
    {
        /** @var ItemInterface|\Magento\Framework\Model\AbstractModel $item */
        if (empty($item->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $item->setStoreId($storeId);
        }
        try {
            $this->resource->save($item);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the item: %1',
                $exception->getMessage()
            ));
        }
        return $item;
    }

    /**
     * Retrieve Item.
     *
     * @param int $itemId
     * @return \Vnecoms\Megamenu\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($itemId)
    {
        if (!isset($this->instances[$itemId])) {
            /** @var \Vnecoms\Megamenu\Api\Data\ItemInterface|\Magento\Framework\Model\AbstractModel $item */
            $item = $this->itemInterfaceFactory->create();
            $this->resource->load($item, $itemId);
            if (!$item->getId()) {
                throw new NoSuchEntityException(__('Requested item doesn\'t exist'));
            }
            $this->instances[$itemId] = $item;
        }
        return $this->instances[$itemId];
    }

    /**
     * Retrieve pages matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Vnecoms\Megamenu\Api\Data\ItemSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Vnecoms\Megamenu\Api\Data\ItemSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Vnecoms\Megamenu\Model\ResourceModel\Item\Collection $collection */
        $collection = $this->itemCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'item_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Vnecoms\Megamenu\Api\Data\ItemInterface[] $items */
        $items = [];
        /** @var \Vnecoms\Megamenu\Model\Item $item */
        foreach ($collection as $item) {
            /** @var \Vnecoms\Megamenu\Api\Data\ItemInterface $itemDataObject */
            $itemDataObject = $this->itemInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($itemDataObject, $item->getData(), ItemInterface::class);
            $items[] = $itemDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($items);
    }

    /**
     * Delete item.
     *
     * @param \Vnecoms\Megamenu\Api\Data\ItemInterface $item
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ItemInterface $item)
    {
        /** @var \Vnecoms\Megamenu\Api\Data\ItemInterface|\Vnecoms\Megamenu\Model\Item $item */
        $id = $item->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($item);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove item %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete item by ID.
     *
     * @param int $itemId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($itemId)
    {
        $item = $this->getById($itemId);
        return $this->delete($item);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
