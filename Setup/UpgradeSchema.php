<?php
/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 *
 */

namespace Vnecoms\Megamenu\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        //upgrade to 1.0.1
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            //update ves_megamenu_item table
            $tableName = $setup->getTable('ves_megamenu_item');
            //check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                //declare some new columns
                $columns = [
                    'visible_option' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'nullable' => false,
                        'default' => 0,
                        'comment' => 'Options: 0 - Use general config / 1 - Customize',
                    ],
                    'visible_in' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'size' => 100,
                        'nullable' => false,
                        'comment' => 'Visible in: desktop, tablet, mobile',
                    ],
                    'animation_in' =>
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Animation In'
                    ],
                    'animation_time' =>
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Animation Time'
                    ],
                    'align' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Alignment Type'
                    ],
                    'dropdown_inlinecss' =>
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Dropdown Inline CSS'
                    ]

                ];
                //add columns
                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }

            /**
             * Create table 'ves_megamenu_menu_customergroup'
             */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('ves_megamenu_menu_customergroup')
            )->addColumn(
                'menu_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'primary' => true],
                'Menu ID'
            )->addColumn(
                'customer_group_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Customer Group ID'
            )->addIndex(
                $setup->getIdxName('ves_megamenu_menu_customergroup', ['customer_group_id']),
                ['customer_group_id']
            )->addForeignKey(
                $setup->getFkName('ves_megamenu_menu_customergroup', 'menu_id', 'ves_megamenu_menu', 'menu_id'),
                'menu_id',
                $setup->getTable('ves_megamenu_menu'),
                'menu_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $setup->getFkName('ves_megamenu_menu_customergroup', 'customer_group_id', 'customer_group', 'customer_group_id'),
                'customer_group_id',
                $setup->getTable('customer_group'),
                'customer_group_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Menu Custom Group'
            );
            $setup->getConnection()->createTable($table);
        }



       /* if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $tableName = $setup->getTable('ves_megamenu_item');
            $connection = $setup->getConnection();
            if ($connection->isTableExists($tableName) == true) {
                $query = "UPDATE {$tableName} SET `icon_image` = REPLACE(`icon_image`, '/ves_megamenu/images', '')";
                $connection->query($query);
            }
        }*/

        $setup->endSetup();
    }
}
