<?php

namespace Vnecoms\Megamenu\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;


class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */

    // @codingStandardsIgnoreStart
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    // @codingStandardsIgnoreEnd
    {
        $installer = $setup;

        $installer->startSetup();

        /*
         * Create table ves_megamenu_menu for store collection of menu
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_megamenu_menu')
        )->addColumn(
            'menu_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Menu ID'
        )->addColumn(
            'identifier',
            Table::TYPE_TEXT,
            100,
            ['nullable' => false],
            'Menu Identifier'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Menu Name'
        )->addColumn(
            'mobile_template',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Mobile Template'
        )->addColumn(
            'desktop_template',
             Table::TYPE_TEXT,
             255,
             ['nullable' => false],
             'Desktop Template'
        )->addColumn(
            'animation_type',
            Table::TYPE_TEXT,
            100,
            ['nullable' => true, 'default' => null],
            'Animation effect you want to display sub-menu items'
        )->addColumn(
            'structure',
            Table::TYPE_TEXT,
            '64k',
            ['nullable' => true],
            'Structure Data Contain Child Menus'
        )->addColumn(
            'is_active',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Is Active'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            '2M',
            ['nullable' => true],
            'Menu Description'
        )->addColumn(
            'creation_time',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Menu Creation Time'
        )->addColumn(
            'update_time',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Menu Modification Time'
        )->addColumn(
            'sort_order',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '0'],
            'Sort Order'
        )->addIndex(
            $installer->getIdxName('ves_megamenu_menu', ['identifier']),
            ['identifier']
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table ves_megamenu_menu
         */


        /**
         * Create table 'ves_megamenu_menu_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_megamenu_menu_store')
        )->addColumn(
            'menu_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Menu ID'
        )->addColumn(
            'store_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        )->addIndex(
            $installer->getIdxName('ves_megamenu_menu_store', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('ves_megamenu_menu_store_fk', 'menu_id', 'ves_megamenu_menu', 'menu_id'),
            'menu_id',
            $installer->getTable('ves_megamenu_menu'),
            'menu_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ves_megamenu_menu_store', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Table Menu'
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table ves_megamenu_menu_store
         */


         /*
          * Create table ves_megamenu_item for store item of menu
          */
         $table = $installer->getConnection()->newTable(
             $installer->getTable('ves_megamenu_item')
         )->addColumn(
             'item_id',
             Table::TYPE_SMALLINT,
             null,
             ['identity' => true, 'nullable' => false, 'primary' => true],
             'Item ID'
         )->addColumn(
             'parent_id',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => true, 'default' => 0],
             'Menu Item Parent Id'
         )->addColumn(
             'menu_id',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false],
             'Menu ID'
         )->addColumn(
             'title',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Item Title'
         )->addColumn(
             'show_title',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false, 'default' => '1'],
             'Show/Hide Item Title'
         )->addColumn(
             'font_classes',
             Table::TYPE_TEXT,
             100,
             ['nullable' => true],
             'Font Icon Classes'
         )->addColumn(
             'is_active',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false, 'default'   => '1',],
             'Status'
         )->addColumn(
             'show_icon',
             Table::TYPE_SMALLINT,
             null,
             [],
             'Show Icon'
         )->addColumn(
             'icon_image',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Icon Image Of Item'
         )->addColumn(
             'show_header',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => true],
             'Show Header'
         )->addColumn(
             'header_html',
             Table::TYPE_TEXT,
             '64k',
             ['nullable' => true],
             'Header'
         )->addColumn(
             'show_left_sidebar',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => true],
             'Show Left Sidebar'
         )->addColumn(
             'left_sidebar_html',
             Table::TYPE_TEXT,
             '64k',
             ['nullable' => true],
             'Left Sidebar HTML'
         )->addColumn(
             'left_sidebar_width',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Left Sidebar Width'
         )->addColumn(
             'content_html',
             Table::TYPE_TEXT,
             '64k',
             ['nullable' => true],
             'Content HTML'
         )->addColumn(
             'show_right_sidebar',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => true],
             'Show Right Sidebar'
         )->addColumn(
             'right_sidebar_width',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Right Sidebar Width'
         )->addColumn(
             'right_sidebar_html',
             Table::TYPE_TEXT,
             '64k',
             ['nullable' => true],
             'Right Sidebar HTML'
         )->addColumn(
             'show_footer',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => true],
             'Show Footer'
         )->addColumn(
             'footer_html',
             Table::TYPE_TEXT,
             '64k',
             ['nullable' => true],
             'Footer HTML'
         )->addColumn(
             'link_type',
             Table::TYPE_TEXT,
             100,
             ['nullable' => false, 'default' => 'custom_link'],
             'Link type of Menu Item Link'
         )->addColumn(
             'link',
             Table::TYPE_TEXT,
             500,
             ['nullable' => false, 'default' => null],
             'Menu Item Link'
         )->addColumn(
             'category_id',
             Table::TYPE_INTEGER,
             null,
             ['nullable' => true],
             'Category ID'
         )->addColumn(
             'link_target',
             Table::TYPE_TEXT,
             50,
             ['nullable' => false, 'default' => '_self'],
             'Link Target'
         )->addColumn(
             'show_number_product',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false, 'default' => '0'],
             'Show number products of category'
         )->addColumn(
             'cms_page',
             Table::TYPE_TEXT,
             null,
             ['nullable' => true],
             'ID of CMS page'
         )->addColumn(
             'is_group',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false, 'default' => '0'],
             'Is Item Have Group Childs'
         )->addColumn(
             'mega_cols',
             Table::TYPE_INTEGER,
             null,
             ['nullable' => true],
             'Number columns of sub-menu'
         )->addColumn(
             'content_width',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Width of content (px)'
         )->addColumn(
             'sub_width',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Width of dropdown container (px)'
         )->addColumn(
             'show_content',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false],
             'Show Content'
         )->addColumn(
             'content_type',
             Table::TYPE_TEXT,
             255,
             ['nullable' => true],
             'Sub Content Type'
         )->addColumn(
             'content_html',
             Table::TYPE_TEXT,
             '2M',
             ['nullable' => true],
             'Custom Content (Block short code/Html/Text)'
         )->addColumn(
             'static_blocks',
             Table::TYPE_TEXT,
             500,
             ['nullable' => true],
             'List IDs of CMS Static Blocks'
         )->addColumn(
             'addition_class',
             Table::TYPE_TEXT,
             50,
             ['nullable' => true],
             'Addition Class CSS for menu item'
         )->addColumn(
             'creation_time',
             Table::TYPE_TIMESTAMP,
             null,
             ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
             'Menu Item Creation Time'
         )->addColumn(
             'update_time',
             Table::TYPE_TIMESTAMP,
             null,
             ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
             'Menu Item Modification Time'
         )->addColumn(
             'is_active',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false, 'default' => '1'],
             'Is Menu Item Active'
         )->addColumn(
             'sort_order',
             Table::TYPE_SMALLINT,
             null,
             ['nullable' => false, 'default' => '0'],
             'Menu Item Sort Order'
         )->addIndex(
             $installer->getIdxName('ves_megamenu_item', ['menu_id']),
             ['menu_id']
         )->addForeignKey(
             $installer->getFkName(
                 'ves_megamenu_item_fk',
                 'menu_id',
                 'ves_megamenu_menu',
                 'menu_id'
             ),
             'menu_id',
             $installer->getTable('ves_megamenu_menu'),
             'menu_id',
             Table::ACTION_CASCADE
         )->setComment('Table Menu Item');

         $installer->getConnection()->createTable($table);

        $installer->getConnection()->addIndex(
            $installer->getTable('ves_megamenu_menu'),
            $setup->getIdxName(
                $installer->getTable('ves_megamenu_menu'),
                ['title', 'identifier', 'description'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'identifier', 'description'],
            AdapterInterface::INDEX_TYPE_FULLTEXT
        );
        $installer->getConnection()->addIndex(
            $installer->getTable('ves_megamenu_item'),
            $setup->getIdxName(
                $installer->getTable('ves_megamenu_item'),
                ['title', 'link', 'link_type', 'link_target'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'link', 'link_type', 'link_target'],
            AdapterInterface::INDEX_TYPE_FULLTEXT
        );
        
         /*
          * End create table ves_megamenu_item
          */
        $installer->endSetup();
    }
}
