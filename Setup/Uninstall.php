<?php
/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 *
 */

namespace Vnecoms\Megamenu\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class Uninstall implements UninstallInterface {

    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        //uninstall code, drop related tables
        $installer->getConnection()->dropTable($installer->getTable('ves_megamenu_menu'));
        $installer->getConnection()->dropTable($installer->getTable('ves_megamenu_menu_store'));
        $installer->getConnection()->dropTable($installer->getTable('ves_megamenu_item'));

        $installer->endSetup();
    }
}
