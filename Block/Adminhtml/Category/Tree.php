<?php

// @codingStandardsIgnoreFile

/**
 * Categories tree block
 */
namespace Vnecoms\Megamenu\Block\Adminhtml\Category;

use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\Data\Tree\Node;
use Magento\Store\Model\Store;

/**
 * Class Tree
 *
 * @package Vnecoms\Megamenu\Block\Adminhtml\Category
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Tree extends \Magento\Catalog\Block\Adminhtml\Category\Tree
{


    /**
     * Retrieve list of categories with name containing $namePart and their parents
     *
     * @param string $namePart
     * @param $storeId
     * @return string
     */
    public function getSuggestedCategoriesJson($namePart, $storeId)
    {
        if (is_null($storeId)) {
            $storeId = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());
        }

        //get root category id of this store
        $store = $this->_storeManager->getStore($storeId);
        $rootCategoryId = $store->getRootCategoryId();
        if ($store->getId() == \Magento\Store\Model\Store::DEFAULT_STORE_ID) {
            $rootCategoryId = $this->_storeManager->getDefaultStoreView()->getRootCategoryId();
        }

        /* @var $collection Collection */
        $collection = $this->_categoryFactory->create()->getCollection();

        $matchingNamesCollection = clone $collection;
        $escapedNamePart = $this->_resourceHelper->addLikeEscape(
            $namePart,
            ['position' => 'any']
        );
        $matchingNamesCollection->addAttributeToFilter(
            'name',
            ['like' => $escapedNamePart]
        )->addAttributeToFilter(
            'entity_id',
            ['neq' => \Magento\Catalog\Model\Category::TREE_ROOT_ID]
        )->addAttributeToSelect(
            'path'
        )->setStoreId(
            $storeId
        );

        $shownCategoriesIds = [];
        foreach ($matchingNamesCollection as $category) {
            foreach (explode('/', $category->getPath()) as $parentId) {
                $shownCategoriesIds[$parentId] = 1;
            }
        }

        $collection->addAttributeToFilter(
            'entity_id',
            ['in' => array_keys($shownCategoriesIds)]
        )->addAttributeToSelect(
            ['name', 'is_active', 'parent_id']
        )->setStoreId(
            $storeId
        );

        $categoryById = [
            \Magento\Catalog\Model\Category::TREE_ROOT_ID => [
                'id' => \Magento\Catalog\Model\Category::TREE_ROOT_ID,
                'children' => [],
            ],
        ];
        foreach ($collection as $category) {
            foreach ([$category->getId(), $category->getParentId()] as $categoryId) {
                if (!isset($categoryById[$categoryId])) {
                    $categoryById[$categoryId] = ['id' => $categoryId, 'children' => []];
                }
            }
            $categoryById[$category->getId()]['is_active'] = $category->getIsActive();
            $categoryById[$category->getId()]['label'] = $category->getName();
            $categoryById[$category->getParentId()]['children'][] = & $categoryById[$category->getId()];
        }

        return $this->_jsonEncoder->encode($categoryById[\Magento\Catalog\Model\Category::TREE_ROOT_ID]['children']);
    }


}
