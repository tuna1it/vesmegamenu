<?php

namespace Vnecoms\Megamenu\Block\Adminhtml\Menu\Grid\Renderer\Action;

use Magento\Store\Api\StoreResolverInterface; // New update in 2.1 for UI Grid StoreResolve
/**
 * Class UrlBuilder
 * 
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class UrlBuilder
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $frontendUrlBuilder;

    /**
     * @param \Magento\Framework\UrlInterface $frontendUrlBuilder
     */
    public function __construct(\Magento\Framework\UrlInterface $frontendUrlBuilder)
    {
        $this->frontendUrlBuilder = $frontendUrlBuilder;
    }


    /**
     * Get action url
     *
     * @param string $routePath
     * @param string $scope
     * @param string $store
     * @return string
     */
    public function getUrl($routePath, $scope, $store)
    {
        $this->frontendUrlBuilder->setScope($scope);
        $href = $this->frontendUrlBuilder->getUrl(
            $routePath,
            [
                '_current' => false,
                '_query' => [StoreResolverInterface::PARAM_NAME => $store]
            ]
        );
        return $href;
    }
}

