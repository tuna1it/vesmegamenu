<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\Megamenu\Block\Adminhtml\Menu\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 * @package Vnecoms\Megamenu\Block\Adminhtml\Menu\Edit
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var \Vnecoms\Megamenu\Model\MenuFactory
     */
    protected $_menuFactory;

    /** @var \Magento\Framework\Registry  */
    protected $_coreRegistry;

    /**
     * GenericButton constructor.
     * @param Context $context
     * @param \Vnecoms\Megamenu\Model\MenuFactory $menuFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Context $context,
        \Vnecoms\Megamenu\Model\MenuFactory $menuFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->context = $context;
        $this->_menuFactory = $menuFactory;
        $this->_coreRegistry = $registry;
    }


    /**
     * Return Menu ID
     *
     * @return int|null
     */
    public function getMenuId()
    {
        try {
            $menuId = $this->_coreRegistry->registry('current_menu_id');
            return $menuId ? $menuId : null;

        } catch (NoSuchEntityException $e) {
            $e->getMessage();
        }

    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

    /**
     * Check where button can be rendered
     *
     * @param string $name
     * @return string
     */
    public function canRender($name)
    {
        return $name;
    }
}
