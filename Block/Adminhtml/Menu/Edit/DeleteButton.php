<?php

namespace Vnecoms\Megamenu\Block\Adminhtml\Menu\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 * @package Vnecoms\Megamenu\Block\Adminhtml\Menu\Edit
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        $menuId = (int)$this->getMenuId();
        if ($menuId && $this->canRender('delete')) {
            $data = [
                'label' => __('Delete Menu'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('megamenuadmin/*/delete', ['menu_id' => $this->getMenuId()]);
    }
}
