<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 05/10/2016
 * Time: 16:35
 */

namespace Vnecoms\Megamenu\Block\Adminhtml\Renderer;


class TreeItem extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Vnecoms\Megamenu\Model\ItemFactory
     */
    protected $_itemFactory;

    /** @var  \Magento\Backend\Model\Session */
    protected $_session;

    /** @var \Vnecoms\Megamenu\Model\Item\Image  */
    protected $_imageModel;

    /** @var \Vnecoms\Megamenu\Helper\Menu  */
    protected $menuHelper;

    /**
     * TreeItem constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param \Vnecoms\Megamenu\Model\Item\Image $imageModel
     * @param \Magento\Backend\Model\Session $session
     * @param \Vnecoms\Megamenu\Helper\Menu $menuHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        \Vnecoms\Megamenu\Model\Item\Image $imageModel,
        \Magento\Backend\Model\Session $session,
        \Vnecoms\Megamenu\Helper\Menu $menuHelper,
        array $data = []
    )
    {
        parent::__construct($context);
        $this->_itemFactory = $itemFactory;
        $this->_session = $session;
        $this->_imageModel = $imageModel;
        $this->menuHelper = $menuHelper;
        $menuId = $context->getRequest()->getParam('menu_id');
    }

    /**
     * @return \Magento\Backend\Model\Session
     */
    public function getSession()
    {
        return $this->_session;
    }

    public function getContext()
    {
        return $this->getContext();
    }

    /**
     * Get Menu Id
     *
     * @return int
     */
    public function getMenuId()
    {
        return $this->getSession()->getMenuId();
    }

    /**
     * Get menu options to select
     *
     * @return array
     */
    public function getMenuOptions()
    {
        /** @var \Vnecoms\Megamenu\Model\Item $item */
        $item = $this->_itemFactory->create();
        $options = $item->getResource()->getMenuOptions();
        //var_dump($options);exit;
        return $options;

    }

    /**
     * Get items data
     *
     * @param void
     * @return array
     */
    public function getMenuItems()
    {
        //get current menu group id
        $menuId = (int) $this->getMenuId();

        $collection = $this->_itemFactory->create()->getCollection()
            ->addFieldToSelect(['item_id', 'parent_id', 'title', 'icon_image', 'link', 'is_active', 'sort_order'])
            ->addFieldToFilter('menu_id', ['in' => [$menuId]])
            ->addOrder('sort_order', 'ASC')
            ->addOrder('title', 'ASC')
            ->load();

        $ref   = [];
        $items = [];
        /** @var \Vnecoms\Megamenu\Model\Item $item */
        foreach ($collection->getItems() as $item) {
            $thisRef = &$ref[$item->getId()];

            $thisRef['parent'] = $item->getParentId();
            $thisRef['id'] = $item->getId();
            $thisRef['label'] = $item->getTitle();
            $thisRef['icon_image'] = $item->getIconImage();
            $thisRef['link'] = $item->getLink();
            $thisRef['is_active'] = $item->isActive();

            if($item->getParentId() == 0) {
                $items[$item->getId()] = &$thisRef;
            } else {
                $ref[$item->getParentId()]['child'][$item->getId()] = &$thisRef;
            }
        }

        return $items;
    }

    public function getAllMenuItems()
    {
        $menuItems = [];

        $menuId = (int) $this->getMenuId();

        $collection = $this->_itemFactory->create()->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('menu_id', ['in' => [$menuId]])
            ->load()
            ->getData();
        $data = $collection;

        if(!empty($data)){
            foreach ($data as $k => $v) {
                $v['htmlId'] = 'vesitem-' . $v['item_id'] . time() . rand();
                $menuItems[$v['item_id']] = $v;
            }
        }
        return $menuItems;
    }

    public function getStructure()
    {
        $items = $this->_itemFactory->create()->getCollection()
            ->addFieldToSelect(['item_id','parent_id'])
            ->addFieldToFilter('menu_id', ['in' => [$this->getMenuId()]])
            ->load();
        $structure = $this->menuHelper->rebuildStructure($items);
        //unset($structure[]['parent_id']);
        return $structure;
    }


    public function renderMenuItem($data = [] , $level = 0, $itemBuild = [])
    {
        $items = $this->_itemFactory->create()->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('menu_id', ['in' => [$this->getMenuId()]])
            ->load()->getData();
        $level++;
        //$data_id = isset($data['item_id']) ? $data['item_id'] : 0;
        //$item = isset($items[$data_id])?$items[$data_id]:[];
        foreach ($items as $k => $v) {
            $item[$k] = $v;
        }
        //$html = $this->_menuItems = json_encode($item) . ',';
        $itemBuild = $item;
        $newChildren = [];
        //var_dump($itemBuild);exit;
        if(isset($data['children']) && count($data['children']>0)){
            foreach ($data['children'] as $k => $v) {
                $newChildren[] = $this->renderMenuItem($v, $level, $itemBuild);
            }
        }
        $itemBuild['children'] = $newChildren;

        return $itemBuild;
    }

    public function buildTreeItem($parentId = 0, $items = [], $class = 'dd-list')
    {
        $html = "<ol class=\"".$class."\" id=\"mega-menu-{$parentId}\">";
        foreach($items as $key => $value) {
            $icon = '';
            if (isset($value['icon_image']) && $value['icon_image']) {
                $icon = '<img class="menu-icon" src="' . $this->_imageModel->getMediaUrl() .'item/image'. $value['icon_image'] . '" /> ';
            }
            if ($value['is_active'])
                $btnChangeStatus = '<a class="change-status-button" id="'.$value['id'].'" title="'.__('Disable this item').'"><i class="fa fa-toggle-on"></i></a>';
            else
                $btnChangeStatus = '<a class="change-status-button" id="'.$value['id'].'" title="'.__('Enable this item').'"><i class="fa fa-toggle-off"></i></a>';

            $html.= '<li class="dd-item dd3-item" data-id="'.$value['id'].'" >
                    <div class="dd-handle dd3-handle"><i class="fa fa-move"></i></div>
                    <div class="dd3-content"><span class="menu-item-title" id="label_show'.$value['id'].'">' . $icon . $value['label'] .'</span>
                        <span class="span-right sub-actions">
                        <span id="link_show'.$value['id'].'">'.$value['link'].'</span> &nbsp;&nbsp;
                            <a class="add-button" id="'.$value['id'].'" title="'.__('Add sub item').'" href="'.$this->getUrl('megamenuadmin/item/new', ['parent_id' => $value['id']]).'" label="'.strip_tags($value['label']).'" link="'.$value['link'].'" ><i class="fa fa-plus-circle"></i></a>
                            <a class="edit-button" id="'.$value['id'].'" title="'.__('Edit this item').'" href="'.$this->getUrl('megamenuadmin/item/edit', ['item_id' => $value['id']]).'" label="'.strip_tags($value['label']).'" link="'.$value['link'].'" ><i class="fa fa-pencil"></i></a>
                            '.$btnChangeStatus.'
                            <a class="del-button" id="'.$value['id'].'" title="'.__('Delete this item').'"><i class="fa fa-trash"></i></a></span>
                    </div>';
            if(array_key_exists('child', $value)) {
                $html .= $this->buildTreeItem($value['id'], $value['child']);
            }
            $html .= "</li>";
        }

        $html .= "</ol>";

        return $html;
    }

}
