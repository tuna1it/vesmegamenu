<?php

namespace Vnecoms\Megamenu\Block\Adminhtml;

/**
 * Class Block Menu
 * 
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class Menu extends \Magento\Backend\Block\Widget\Grid\Container
{

    /**
     * parent constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = "adminhtml_menu";
        $this->_blockGroup = 'Vnecoms_Megamenu';
        $this->_headerText = __('Menu');
        parent::_construct();

        if ($this->_isAllowedAction('Vnecoms_Megamenu::menu_save')) {
            $this->buttonList->update('add', 'label', __('Add New Menu'));
        } else {
            $this->buttonList->remove('add');
        }
    }


    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

}
