<?php

namespace Vnecoms\Megamenu\Block\Adminhtml\Item\Edit\Buttons;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class GenericButton
 * @package Vnecoms\Megamenu\Block\Adminhtml\Item\Edit\Buttons
 */
class Generic
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;


    /**
     * @var \Vnecoms\Megamenu\Model\ItemFactory
     */
    protected $_itemFactory;

    /**
     * Generic constructor.
     * @param Context $context
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        DataPersistorInterface $dataPersistor
    ) {
        $this->context = $context;
        $this->_itemFactory = $itemFactory;
        $this->dataPersistor = $dataPersistor;

    }

    /**
     * Return Menu ID
     *
     * @return int|null
     */
    public function getItemId()
    {
        try {
            return $this->_itemFactory->create()->load(
                $this->context->getRequest()->getParam('item_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Get current menuId
     * @param void
     * @return int
     */
    public function getCurrentMenuId()
    {
        try {
            return $this->dataPersistor->get('current_menu_id');
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

}
