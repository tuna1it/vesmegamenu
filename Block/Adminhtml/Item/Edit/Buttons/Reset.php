<?php

namespace Vnecoms\Megamenu\Block\Adminhtml\Item\Edit\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class ResetButton
 * @package Vnecoms\Megamenu\Block\Adminhtml\Menu\Edit\Buttons
 */
class Reset implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Reset'),
            'class' => 'reset',
            'on_click' => 'location.reload();',
            'sort_order' => 30
        ];
    }
}
