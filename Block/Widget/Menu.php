<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 08/07/2016
 * Time: 12:08
 */

namespace Vnecoms\Megamenu\Block\Widget;

use Magento\Framework\View\Element\Template;

class Menu extends \Magento\Framework\View\Element\Template implements \Magento\Framework\DataObject\IdentityInterface
{

    protected $_template = 'Vnecoms_Megamenu::widget/menu.phtml';


    /** @var array  */
    protected $identities = [];


    /** @var  \Vnecoms\Megamenu\Helper\Data */
    public $dataHelper;

    /** @var \Vnecoms\Megamenu\Helper\Menu  */
    public $menuHelper;

    /** @var \Vnecoms\Megamenu\Model\Menu  */
    protected $_menu;

    /** @var \Magento\Customer\Model\Session  */
    protected $_customerSession;

    /** @var  \Vnecoms\Megamenu\Helper\MobileDetect */
    public $mobileDetect;

    /**
     * Menu constructor.
     * @param Template\Context $context
     * @param \Vnecoms\Megamenu\Helper\Data $dataHelper
     * @param \Vnecoms\Megamenu\Helper\Menu $menuHelper
     * @param \Vnecoms\Megamenu\Model\Menu $menu
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Vnecoms\Megamenu\Helper\MobileDetect $mobileDetect
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vnecoms\Megamenu\Helper\Data $dataHelper,
        \Vnecoms\Megamenu\Helper\Menu $menuHelper,
        \Vnecoms\Megamenu\Model\Menu $menu,
        \Magento\Customer\Model\Session $customerSession,
        \Vnecoms\Megamenu\Helper\MobileDetect $mobileDetect,
        array $data = []
    ){
        $this->dataHelper = $dataHelper;
        $this->menuHelper = $menuHelper;
        $this->_menu            = $menu;
        $this->_customerSession = $customerSession;
        $this->mobileDetect = $mobileDetect;
        parent::__construct($context, $data);

        $pageConfig = $context->getPageConfig();
        //TODO: should add assets dynamic via block
    }

    /**
     * @return \Vnecoms\Megamenu\Helper\Data
     */
    public function getDataHelper()
    {
        return $this->dataHelper;
    }

    /**
     * @return \Vnecoms\Megamenu\Helper\Menu
     */
    public function getMenuHelper()
    {
        return $this->menuHelper;
    }

    /**
     * @return \Vnecoms\Megamenu\Helper\MobileDetect
     */
    public function getMobileDetect() {
        return $this->mobileDetect;
    }

    /**
     * @return bool
     */
    public function CheckIsMobile() {
        return $this->getMobileDetect()->isMobile() ? true : false;
    }


    /**
     * Output menu html
     *
     * @return string|void
     */
    protected function _toHtml()
    {

        if(!$this->getTemplate()){
            $this->setTemplate($this->_template);
        }

        $store = $this->_storeManager->getStore();
        $html = $menu = '';
        if ($menuId = $this->getData('menu_id')) {
            $menu = $this->_menu->setStore($store)->load((int)$menuId);
        }elseif($identifier = $this->getData('identifier')){
            $menu = $this->_menu->setStore($store)->load(addslashes($identifier));
        }

        if($menu && $menu->IsActive()){

            $customerGroups = $menu->getData('customer_group_ids');
            $customerGroupId = (int)$this->_customerSession->getCustomerGroupId();
            if(is_array($customerGroups) && !in_array($customerGroupId, $customerGroups)) return;
            $this->setData("menu", $menu);
            $menuId = $menu->getId();
            $this->assign('menu', $menu);

        } else {

            $this->setData("menu", null);
        }
        return parent::_toHtml();
    }

    public function getMenuEvent()
    {
        return false;
    }

    /**
     * @param $menuIdentifier
     * @return string
     */
    public function getMobileTemplateHtml($menuIdentifier)
    {
        // should set block name megamenu.mobile
        $html = '';
        if($menuIdentifier){
            $html = $this->getLayout()->createBlock(
                'Vnecoms\Megamenu\Block\Widget\MobileMenu',
                'megamenu.mobile',
                [
                    'data' => [
                        'identifier' => $menuIdentifier,
                        'area' => 'frontend'
                    ]
                ]
            )->toHtml();
        }
        return $html;
    }

    /**
     * Process data and build html menu
     *
     * @param $menuId
     * @return string
     */
    protected function _generateMenuHtml($menuId)
    {
        $output = '';
        //get menu items
        /** @var  \Vnecoms\Megamenu\Model\ResourceModel\Item\Collection $items */
        $items = $this->dataHelper->getMenuItems($menuId);
        $structure = $this->dataHelper->getMenuItems2($menuId);
        //var_dump($items);exit;

        if (!empty($items)) {
            //build menu items data
            //$this->menuHelper->rebuildData($items);
            //$output = $this->menuHelper->genMenu();
            $rebuildItems = $this->menuHelper->rebuildStructure($structure);
            //echo"<pre>";var_dump($rebuildItems);exit;
            if (is_array($rebuildItems)) {
                // Get each item and child data and draw it
                foreach ($rebuildItems as $k => $v) {

                    //var_dump($itemDatas);exit;
                    //var_dump($v);exit;
                    $itemData = $this->menuHelper->renderMenuItemData($v, [], $items);
                    $output .= $this->menuHelper->drawItem($itemData);
                    //var_dump($output);exit;
                }
            }

        } else {
            $output = '<span class="no-menu">'.__('There are not menu items found.').'</span>';
        }

        return $output;
    }

    /**
     * Get menu
     * @return \Vnecoms\Megamenu\Model\Menu;
     */
    public function getMenu()
    {
        return $this->_menu;
    }

    /**
     * Add identity
     *
     * @param array $identity
     * @return void
     */
    public function addIdentity($identity)
    {
        $this->identities[] = $identity;
    }


    /**
     * @return array
     */
    public function getIdentities() {
        return [
            \Magento\Store\Model\Store::CACHE_TAG,
            \Vnecoms\Megamenu\Model\Menu::CACHE_TAG,
            \Vnecoms\Megamenu\Model\Item::CACHE_TAG
        ];
    }

}
