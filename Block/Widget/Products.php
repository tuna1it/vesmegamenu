<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 05/11/2016
 * Time: 11:58
 */

namespace Vnecoms\Megamenu\Block\Widget;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;

class Products extends \Magento\Catalog\Block\Product\AbstractProduct implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_collection;

    /** @var  \Vnecoms\Megamenu\Model\Product */
    protected $productModel;

    /** @var  \Vnecoms\Megamenu\Helper\Data */
    public $dataHelper;

    /** @var  \Vnecoms\Megamenu\Helper\Menu */
    public $menuHelper;

    /** @var  \Vnecoms\Megamenu\Helper\Image */
    protected $imageHelper;

    protected $httpContext;

    /**
     * Products constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Vnecoms\Megamenu\Model\Product $productModel
     * @param \Vnecoms\Megamenu\Helper\Data $dataHelper
     * @param \Vnecoms\Megamenu\Helper\Menu $menuHelper
     * @param \Vnecoms\Megamenu\Helper\Image $imageHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Vnecoms\Megamenu\Model\Product $productModel,
        \Vnecoms\Megamenu\Helper\Data $dataHelper,
        \Vnecoms\Megamenu\Helper\Menu $menuHelper,
        \Vnecoms\Megamenu\Helper\Image $imageHelper,
        array $data = []
    ) {
        $this->productModel = $productModel;
        $this->dataHelper = $dataHelper;
        $this->menuHelper = $menuHelper;
        $this->imageHelper = $imageHelper;
        parent::__construct($context, $data );
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $template = $this->getConfig('layout_type');
        $categories = $this->getConfig('categories');
        if($categories!=''){
            $catIds = explode(",", $categories);
        }
        if ($this->hasData('layout_type') && $template == 'owl_carousel' ) {
            $this->setTemplate('Vnecoms_Megamenu::/widget/product_owlcarousel.phtml');
        } elseif ($this->hasData('layout_type') && $template == 'bootstrap_carousel') {
            $this->setTemplate('Vnecoms_Megamenu::/widget/product_bootstrapcarousel.phtml');
        } elseif ($this->hasData('layout_type') && $template == 'jcarousel') {
            $this->setTemplate('Vnecoms_Megamenu::/widget/product_jcarousel.phtml');
        }

        $source_key = $this->getConfig("product_source");
        $config = [];
        $config['pagesize'] = $this->getConfig('number_item',12);
        $config['cats'] = $catIds;
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productModel->getProductBySource($source_key, $config);
        $this->_collection = $collection;

        return parent::_beforeToHtml();
    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return [
            'VES_MEGAMENU_PRODUCT',
            $this->_storeManager->getStore()->getId(),
            $this->_design->getDesignTheme()->getId(),
            $this->httpContext->getValue(CustomerContext::CONTEXT_GROUP),
            'template' => $this->getTemplate(),
            $this->getConfig("number_item")
        ];
    }

    /**
     * @return Collection
     */
    public function getProductCollection(){
        return $this->_collection;
    }

    public function getConfig($key, $default = '')
    {
        if($this->hasData($key) && $this->getData($key))
        {
            return $this->getData($key);
        }
        return $default;
    }

    /**
     * Check product is new
     *
     * @param  \Magento\Catalog\Model\Product $_product
     * @return bool
     */
    public function checkProductIsNew($_product = null) {
        $from_date = $_product->getNewsFromDate();
        $to_date = $_product->getNewsToDate();
        $is_new = false;
        $is_new = $this->isNewProduct($from_date, $to_date);
        $today = strtotime("now");

        if ($from_date && $to_date) {
            $from_date = strtotime($from_date);
            $to_date = strtotime($to_date);
            if ($from_date <= $today && $to_date >= $today) {
                $is_new = true;
            }
        }
        elseif ($from_date && !$to_date) {
            $from_date = strtotime($from_date);
            if ($from_date <= $today) {
                $is_new = true;
            }
        }elseif (!$from_date && $to_date) {
            $to_date = strtotime($to_date);
            if ($to_date >= $today) {
                $is_new = true;
            }
        }
        return $is_new;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getWidgetProductPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null,
        $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
        array $arguments = []
    )
    {

        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }
        $arguments['price_id'] = isset($arguments['price_id'])
            ? $arguments['price_id']
            : 'old-price-' . $product->getId() . '-' . $priceType;
        $arguments['include_container'] = isset($arguments['include_container'])
            ? $arguments['include_container']
            : true;
        $arguments['display_minimal_price'] = isset($arguments['display_minimal_price'])
            ? $arguments['display_minimal_price']
            : true;
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                $arguments
            );
        }
        return $price;
    }

    /**
     * @param $created_date
     * @param int $num_days_new
     * @return bool
     */
    public function isNewProduct( $created_date, $num_days_new = 3) {
        $check = false;

        $startTimeStamp = strtotime($created_date);
        $endTimeStamp = strtotime("now");

        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff/86400;

        $numberDays = intval($numberDays);
        if($numberDays <= $num_days_new) {
            $check = true;
        }

        return $check;
    }

    /**
     * @return \Vnecoms\Megamenu\Helper\Data
     */
    public function getDataHelper()
    {
        return $this->dataHelper;
    }

    /**
     * @return \Vnecoms\Megamenu\Helper\Image
     */
    public function getImageHelper()
    {
        return $this->imageHelper;
    }
}
