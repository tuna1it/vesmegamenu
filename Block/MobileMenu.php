<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 01/11/2016
 * Time: 11:49
 */

namespace Vnecoms\Megamenu\Block;


class MobileMenu extends \Vnecoms\Megamenu\Block\Menu
{
    protected $_template = 'Vnecoms_Megamenu::mobile_menu.phtml';


    /** @var array  */
    protected $identities = [];


    /** @var  \Vnecoms\Megamenu\Helper\Data */
    public $dataHelper;

    /** @var \Vnecoms\Megamenu\Helper\Menu  */
    public $menuHelper;

    /** @var \Vnecoms\Megamenu\Model\Menu  */
    protected $_menu;

    /** @var \Magento\Customer\Model\Session  */
    protected $_customerSession;


    /**
     * Output menu html
     *
     * @return string|void
     */
    protected function _toHtml()
    {

        if(!$this->getTemplate()){
            $this->setTemplate($this->_template);
        }

        $store = $this->_storeManager->getStore();
        $html = $menu = '';
        if ($menuId = $this->getData('menu_id')) {
            $menu = $this->_menu->setStore($store)->load((int)$menuId);
        }elseif($identifier = $this->getData('identifier')){
            $menu = $this->_menu->setStore($store)->load(addslashes($identifier));
        }

        if($menu && $menu->IsActive()){

            $customerGroups = $menu->getData('customer_group_ids');
            $customerGroupId = (int)$this->_customerSession->getCustomerGroupId();
            if(is_array($customerGroups) && !in_array($customerGroupId, $customerGroups)) return;

            $this->setData("menu", $menu);
            $menuId = $menu->getId();
            $this->assign('menu', $menu);

        } else {

            $this->setData("menu", null);
        }
        return parent::_toHtml();
    }
}