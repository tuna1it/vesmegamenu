<?php

namespace Vnecoms\Megamenu\Test\Unit\Model;

use Vnecoms\Megamenu\Model\Menu;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\Context;
use Vnecoms\Megamenu\Model\ResourceModel\Menu as MenuResource;
use Magento\Framework\Model\ResourceModel\AbstractResource;

/**
 * @covers \Vnecoms\Megamenu\Model\Menu
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class MenuTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Vnecoms\Megamenu\Model\Menu
     */
    protected $model;

    /**
     * @var \Magento\Backend\Block\Template\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $contextMock;

    /**
     * @var ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $eventManagerMock;

    /**
     * @var MenuResource|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resourceMenuMock;

    /**
     * @var AbstractResource|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resourcesMock;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $scopeConfigMock;

    protected function setUp()
    {
        $this->eventManagerMock = $this->getMockBuilder(ManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->contextMock = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventManagerMock = $this->getMockBuilder(ManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->scopeConfigMock = $this->getMockBuilder(ScopeConfigInterface::class)
            ->getMockForAbstractClass();

        $this->contextMock->expects($this->any())
            ->method('getEventDispatcher')
            ->willReturn($this->eventManagerMock);
        $this->resourceMenuMock->expects($this->any())
            ->method('getResources')
            ->willReturn($this->resourcesMock);

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManager->getObject(
            Menu::class,
            [
                'context' => $this->contextMock,
                'resource' => $this->resourcesMock,
            ]
        );
        $objectManager->setBackwardCompatibleProperty(
            $this->model,
            'scopeConfig',
            $this->scopeConfigMock
        );
    }



}
