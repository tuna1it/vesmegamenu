<?php

namespace Vnecoms\Megamenu\Controller;

class RegistryConstants
{
    /**
     * Registry key where current entity ID is stored
     */
    const CURRENT_ENTITY_ID = 'current_entity_id';
    const CURRENT_ITEM_ID = 'current_item_id';
}
