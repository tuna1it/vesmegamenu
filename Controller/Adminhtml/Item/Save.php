<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Item;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Vnecoms\Megamenu\Model\Item as ItemModel;
use Vnecoms\Megamenu\Model\ItemFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Vnecoms\Megamenu\Model\Uploader;
use Vnecoms\Megamenu\Model\UploaderPool;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\DataObjectHelper;

use Vnecoms\Megamenu\Api\ItemRepositoryInterface;
use Vnecoms\Megamenu\Api\Data\ItemInterface;
use Vnecoms\Megamenu\Api\Data\ItemInterfaceFactory;

/**
 * Class Save Menu
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Vnecoms_Megamenu::item_save';

    /** @var ItemFactory  */
    protected $_itemFactory;

    /** @var Registry  */
    protected $_coreRegistry;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /** @var ItemInterfaceFactory  */
    protected $itemFactory;

    /** @var ItemRepositoryInterface  */
    protected $itemRepository;

    /** @var \Magento\Backend\Model\Session  */
    protected $_session;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param Registry $registry
     * @param ItemRepositoryInterface $itemRepository
     * @param ItemInterfaceFactory $itemFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param UploaderPool $uploaderPool
     * @param \Magento\Backend\Model\Session $session
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        Registry $registry,
        ItemRepositoryInterface $itemRepository,
        ItemInterfaceFactory $itemFactory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        UploaderPool $uploaderPool,
        \Magento\Backend\Model\Session $session
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_coreRegistry = $registry;
        $this->itemRepository = $itemRepository;
        $this->itemFactory = $itemFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->uploaderPool = $uploaderPool;
        $this->_session = $session;
        parent::__construct($context);
    }


    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Vnecoms_Megamenu::item_save');
    }
    

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $storeViewId = $this->getRequest()->getParam('store');


        $data = $this->getRequest()->getPostValue();

        /** @var \Vnecoms\Megamenu\Api\Data\ItemInterface $item */
        $item = null;

        $id = !empty($data['item_id']) ? $data['item_id'] : null;

        if ($data) {
            try {
                if ($id) {
                    $item = $this->itemRepository->getById((int)$id);
                } else {
                    unset($data['item_id']);
                    $item = $this->itemFactory->create();
                }

                $icon = $this->getUploader('image')->uploadFileAndGetName('icon_image', $data);
                $data['icon_image'] = $icon;
                $this->dataObjectHelper->populateWithArray($item, $data, ItemInterface::class);
                $this->itemRepository->save($item);
                $this->_getSession()->setItemData($item->getData());
                $this->messageManager->addSuccessMessage(__('You saved the item'));
                $this->_getSession()->setItemData(false);
                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath('megamenuadmin/item/edit', ['item_id' => $item->getId()]);
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                if ($item != null) {
                    $this->storeItemDataToSession(
                        $this->dataObjectProcessor->buildOutputDataArray(
                            $item,
                            ItemInterface::class
                        )
                    );
                }
                $resultRedirect->setPath('megamenuadmin/item/edit', ['item_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem saving the item'));
                if ($item != null) {
                    $this->storeItemDataToSession(
                        $this->dataObjectProcessor->buildOutputDataArray(
                            $item,
                            ItemInterface::class
                        )
                    );
                }
                $data = !empty($data) ? $data : [];
                $this->_getSession()->setItemData($data);
                $resultRedirect->setPath('megamenuadmin/item/edit', ['item_id' => $id]);
            }

            $this->dataPersistor->set('item_data', $data);
        }

        return $resultRedirect->setPath('*/*/', ['menu_id' => $data['menu_id']]);

    }

    /**
     * @param $type
     * @return Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }

    /**
     * @param $itemData
     */
    protected function storeItemDataToSession($itemData)
    {
        $this->_getSession()->setItemData($itemData);
    }

    /**
     * @return \Magento\Backend\Model\Session
     */
    public function getSession()
    {
        return $this->_session;
    }
}
