<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 29/09/2016
 * Time: 10:02
 */

namespace Vnecoms\Megamenu\Controller\Adminhtml\Item;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Json\Encoder;
use Magento\Framework\Json\Decoder;
use Magento\Framework\Controller\Result\Raw;
use Magento\Store\Model\StoreManagerInterface;

class AjaxChangeStatus extends Action
{

    const ADMIN_RESOURCE = 'Vnecoms_Megamenu::item_save';
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /** @var Encoder  */
    protected $jsonEncoder;
    /** @var Decoder  */
    protected $jsonDecoder;
    /** @var Raw  */
    protected $rawResult;
    /** @var StoreManagerInterface  */
    protected $storeManager;

    /** @var  \Vnecoms\Megamenu\Model\ItemFactory */
    protected $itemModel;


    /**
     * AjaxChangeStatus constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Encoder $jsonEncoder
     * @param Decoder $jsonDecoder
     * @param Raw $rawResult
     * @param StoreManagerInterface $storeManager
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemModel
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Encoder $jsonEncoder,
        Decoder $jsonDecoder,
        Raw $rawResult,
        StoreManagerInterface $storeManager,
        \Vnecoms\Megamenu\Model\ItemFactory $itemModel
    ){
        parent::__construct($context);
        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->rawResult = $rawResult;
        $this->storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        $this->itemModel = $itemModel;
    }

    public function execute()
    {
        $item_id = $this->getRequest()->getParam('item_id');
        $result = [
            'success' => false
        ];

        if ($item_id) {
            try {
                /** @var \Vnecoms\Megamenu\Model\Item $item */
                $item = $this->itemModel->create()->load($item_id);
                if ($item->isActive()) {
                    $item->setIsActive(false);
                    $result['action'] = 'disabled';
                } else {
                    $item->setIsActive(true);
                    $result['action'] = 'enabled';
                }

                //save status
                $item->save();

                // display success message
                $result['success'] = true;
                $result['message'] = __("The menu item has been {$result['action']}.");

            } catch(\Exception $e) {
                $result['message'] = $e->getMessage();
            }
        } else {
            // display error message
            $result['message'] = __('We can\'t find a menu item to change status.');
        }

        $this->rawResult->setHeader('Content-type', 'application/json');
        return $this->rawResult->setContents($this->jsonEncoder->encode($result));
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}