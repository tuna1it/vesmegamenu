<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Item;

use Vnecoms\Megamenu\Controller\Adminhtml\Item;
use Magento\Backend\App\Action;
use Vnecoms\Megamenu\Controller\RegistryConstants;
use Vnecoms\Megamenu\Api\ItemRepositoryInterface;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class Edit
 * @package Vnecoms\Megamenu\Controller\Adminhtml\Item
 */
class Edit extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Vnecoms_Megamenu::item_save';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /** @var  \Magento\Backend\Model\Session */
    protected $_session;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /** @var  \Vnecoms\Megamenu\Model\ItemFactory */
    protected $_itemFactory;

    /** @var ItemRepositoryInterface  */
    protected $itemRepository;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param DataPersistorInterface $dataPersistor
     * @param ItemRepositoryInterface $itemRepository
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        DataPersistorInterface $dataPersistor,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_itemFactory = $itemFactory;
        $this->_session = $context->getSession();
        $this->itemRepository = $itemRepository;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * @return \Vnecoms\Megamenu\Model\Item
     */
    protected function _initItem()
    {

        $itemId = $this->getRequest()->getParam('item_id');
        $itemModel = $this->_itemFactory->create();
        if ($itemId) {
            $itemModel->load($itemId);
        }
        $this->_coreRegistry->register(RegistryConstants::CURRENT_ITEM_ID, $itemId);
        $this->_coreRegistry->register('current_item', $itemModel);

        return $itemModel;
    }

    /**
     * Edit Action
     *
     * @param void
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {

        $item = $this->_initItem();
        $itemId = $item->getId();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Vnecoms_Megamenu::item');
        $resultPage->getConfig()->getTitle()->prepend(__('ITems'));
        $resultPage->addBreadcrumb(__('Menu'), __('Menu'));
        $resultPage->addBreadcrumb(__('Items'), __('Items'), $this->getUrl('megamenuadmin/item'));

        // set entered data if was error when we do save
        //$data = $this->_getSession()->getItemData(true);

        // new
        if ($itemId === null) {
            $resultPage->addBreadcrumb(__('Add New Item '), __('Add New Item'));
            $resultPage->getConfig()->getTitle()->prepend(__('Add New Item'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Item'), __('Edit Item'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->itemRepository->getById($itemId)->getTitle()
            );
        }

        return $resultPage;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}
