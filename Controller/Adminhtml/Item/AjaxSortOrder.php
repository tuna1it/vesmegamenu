<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 29/09/2016
 * Time: 10:02
 */

namespace Vnecoms\Megamenu\Controller\Adminhtml\Item;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Json\Encoder;
use Magento\Framework\Json\Decoder;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Store\Model\StoreManagerInterface;

class AjaxSortOrder extends Action
{

    const ADMIN_RESOURCE = 'Vnecoms_Megamenu::item_save';

    /** @var Encoder  */
    protected $jsonEncoder;
    /** @var Decoder  */
    protected $jsonDecoder;
    /** @var RawFactory  */
    protected $rawResultFactory;
    /** @var StoreManagerInterface  */
    protected $storeManager;

    /** @var  \Vnecoms\Megamenu\Model\ItemFactory */
    protected $itemFactory;


    /**
     * AjaxSortOrder constructor.
     * @param Context $context
     * @param Encoder $jsonEncoder
     * @param Decoder $jsonDecoder
     * @param RawFactory $rawResultFactory
     * @param StoreManagerInterface $storeManager
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     */
    public function __construct(
        Context $context,
        Encoder $jsonEncoder,
        Decoder $jsonDecoder,
        RawFactory $rawResultFactory,
        StoreManagerInterface $storeManager,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
    ){
        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->rawResultFactory = $rawResultFactory;
        $this->storeManager = $storeManager;
        $this->itemFactory = $itemFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $menuData = $this->getRequest()->getParam('menu_data');
        $result = ['success' => false];

        if ($menuData) {
            try {
                //create menu item model
                $model = $this->itemFactory->create();

                //decode menu data
                $menuData = $this->jsonDecoder->decode($menuData);

                //parse menu data to array
                $menuData = self::parseJsonArray($menuData);

                //load and save menu item data
                $i = 0;
                foreach ($menuData as $item) {
                    $i++;
                    $model->load($item['id']);
                    if ($model) {
                        $model->setParentId($item['parent_id']);
                        $model->setSortOrder($i);
                        $model->save();
                    }
                }

                $result['success'] =  true;
                $result['message'] = __('Menu items was saved.');

            } catch (\Exception $e) {
                $result['message'] = $e->getMessage();
            }
        }

        $response = $this->rawResultFactory->create()->setHeader('Content-type', 'application/json');
        return $response->setContents($this->jsonEncoder->encode($result));
    }

    /**
     * @param $jsonData
     * @param int $parentId
     * @return array
     */
    public static function parseJsonArray($jsonData, $parentId = 0)
    {
        $rs = [];
        foreach ($jsonData as $subArray) {
            $returnSubArray = [];
            if (isset($subArray['children'])) {
                $returnSubArray = self::parseJsonArray($subArray['children'], $subArray['id']);
            }

            $rs[] = ['id' => $subArray['id'], 'parent_id' => $parentId];
            $rs = array_merge($rs, $returnSubArray);
        }

        return $rs;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}