<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Item;

use Magento\Backend\App\Action\Context;
use Vnecoms\Megamenu\Model\MenuFactory;
use Magento\Framework\Registry;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class Item Index
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class Index extends \Magento\Backend\App\Action
{


    /** @var  \Vnecoms\Megamenu\Model\MenuFactory */
    protected $menuFactory;

    /** @var  \Vnecoms\Megamenu\Helper\Menu  */
    protected $menuHelper;

    /** @var  \Magento\Framework\Registry */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /** @var \Magento\Framework\View\Result\PageFactory  */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;


    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param MenuFactory $menuFactory
     * @param \Vnecoms\Megamenu\Helper\Menu $menuHelper
     * @param Registry $registry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Vnecoms\Megamenu\Model\MenuFactory $menuFactory,
        \Vnecoms\Megamenu\Helper\Menu $menuHelper,
        Registry $registry,
        DataPersistorInterface $dataPersistor
    ){
        parent::__construct($context);
        $this->_authorization = $context->getAuthorization();
        $this->resultPageFactory = $resultPageFactory;
        $this->menuFactory = $menuFactory;
        $this->menuHelper = $menuHelper;
        $this->dataPersistor = $dataPersistor;
        $this->_coreRegistry = $registry;

    }

    /**
     * @return \Magento\Backend\Model\Session
     */
    public function _getSession()
    {
        return parent::_getSession();
    }

    /**
     * View CMS page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        //get current selected menu group and save to session to use in other context later
        $menuId = $this->getRequest()->getParam('menu_id');
        if ($menuId) {
            $this->_coreRegistry->unregister('current_menu');
            $this->_coreRegistry->unregister('current_menu_id');
            $this->_getSession()->unsMenuId();
            $this->dataPersistor->clear('current_menu_id');

            $menu = $this->menuFactory->create()->load($menuId);
            $this->_coreRegistry->register('current_menu', $menu);

            $this->_getSession()->setMenuId($menuId);


            $this->dataPersistor->set('current_menu_id', $menuId);
            $this->dataPersistor->clear('init_data');
            $current_data = [
              'menu_id' => $menuId, 'parent_id' => ''
            ];
            $this->dataPersistor->set('init_data', $current_data);
            $this->_coreRegistry->register('current_menu_id', $menuId);
        } else {
            $menuId = $this->_getSession()->getMenuId();
        }

        if ($menuId) {
            /** @var \Vnecoms\Megamenu\Model\Menu $model */
            $model = $this->menuFactory->create();
            $model->load($menuId);
            $title = $model->getTitle()." ({$model->getIdentifier()})";
        }
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Vnecoms_Megamenu::menu');
        $resultPage->addBreadcrumb(__('Mega Menu'), __('Mega Menu'));
        $resultPage->addBreadcrumb(__('Manage Menu Items'), __('Manage Menu Items'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Menu Items of Menu: %1', $title));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
