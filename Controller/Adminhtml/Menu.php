<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 11/07/2016
 * Time: 14:35
 */

namespace Vnecoms\Megamenu\Controller\Adminhtml;


abstract class Menu extends AbstractAction
{
    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Vnecoms_Megamenu::menu');
    }
}