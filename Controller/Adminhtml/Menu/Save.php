<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;

use Vnecoms\Megamenu\Controller\Adminhtml\Menu;
use Magento\Framework\Exception\LocalizedException;


/**
 * Class Save Menu
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class Save extends Menu
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Vnecoms_Megamenu::menu_save';


    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Vnecoms_Megamenu::menu_save');
    }
    

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->_resultRedirectFactory->create();

        $storeViewId = $this->getRequest()->getParam('store');

        $data = $this->getRequest()->getPostValue();
        //var_dump($data);exit;
        if ($data) {
            $menuId = (int) $this->getRequest()->getParam('menu_id');
            /** @var \Vnecoms\Megamenu\Model\Menu $menu */
            $menu = $this->_menuFactory->create();

            if ($menuId) {
                $menu->load($menuId);
            }

            $this->_eventManager->dispatch(
                'megamgnu_menu_prepare_save',
                ['menu' => $menu, 'request' => $this->getRequest()]
            );

            /**
             *  No need to setData in ui form way
             */
            $menu->setData($data);

            // Save
            try {

                $this->dataPersistor->set('megamenu_menu', $data);

                $menu->save();
                //$this->menuRepository->save($menu);

                // display success message
                $this->messageManager->addSuccess(__('Menu has been saved.'));
                // clear previously saved data from session
                $this->_getSession()->setFormData(false); //OLD WAY SESSION SET DATA
                $this->dataPersistor->clear('megamenu_menu');

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back') === 'edit') {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        [
                            'menu_id' => $menu->getId(),
                            '_current' => true,
                            'store' => $storeViewId
                        ]
                    );
                } elseif ($this->getRequest()->getParam('back') === 'new') {
                    return $resultRedirect->setPath(
                        '*/*/new',
                        ['_current' => true]
                    );
                }

                if(!$this->getRequest()->getParam("duplicate")){
                    return $resultRedirect->setPath('*/*/');
                }

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e, __('Something went wrong while saving the menu.'));
                // save data in session
                //$this->_getSession()->setFormData($data);
                //$this->dataPersistor->set('megamenu_menu', $data);
                // redirect to edit form
                return $resultRedirect->setPath(
                    '*/*/edit',
                    [
                        'menu_id' => $menu->getId(),
                        '_current' => true
                    ]
                );
            }

        }
        return $resultRedirect->setPath('megamenuadmin/*/');
    }

}
