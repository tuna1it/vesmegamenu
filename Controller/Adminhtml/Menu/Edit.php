<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;

use Vnecoms\Megamenu\Controller\Adminhtml\Menu;

/**
 * Class Edit
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class Edit extends Menu
{

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Vnecoms_Megamenu::menu')
            ->addBreadcrumb(__('Menu'), __('Menu'))
            ->addBreadcrumb(__('Manage Menus'), __('Manage Menus'));
        return $resultPage;
    }


    /**
     * Edit Action
     *
     * @param void
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('menu_id');

        /** @var \Vnecoms\Megamenu\Model\Menu $model */
        $model = $this->_menuFactory->create();

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This menu no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        // 3. Set entered data if was error when we do save
        $data =  $this->_getSession()->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks event it exist or not
        $this->_coreRegistry->register('megamenu_menu', $model);
        $this->_coreRegistry->register('current_menu_id', $id);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Menu') : __('New Menu'),
            $id ? __('Edit Menu') : __('New Menu')
        );

        // show name of menu when edit
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ?'Edit Menu: '. $model->getTitle() : __('New Menu'));

        return $resultPage;
    }
    
}


