<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 26/07/2016
 * Time: 17:20
 */

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;

use Vnecoms\Megamenu\Controller\Adminhtml\Menu;

class Duplicate extends Menu
{

    const ADMIN_RESOURCE = 'Vnecoms_Megamenu::menu_duplicate';


    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }


    /**
     * @return $this
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // get group id from request
        $id = $this->getRequest()->getParam('menu_id');
        if ($id) {
            try {
                // load group by id
                $menu = $this->_menuFactory->create()->load($id);

                // start clone
                $newMenu = $this->_menuFactory->create();
                $data = $menu->getData();
                // clone group
                unset($data['menu_id']);
                unset($data['creation_time']);
                unset($data['update_time']);
                $data['title'] = $data['title']."-".uniqid();
                $data['identifier'] = $data['identifier']."-".uniqid();
                $newMenu->setData($data);
                $newMenu->save();

                // clone items of group
                $this->findDuplicate(0, $menu->getId(), 0, $newMenu->getId());

                // end clone, display success message
                $this->messageManager->addSuccess(__('The Menu and Menu Items of it has been duplicated.'));

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['menu_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a Menu to duplicate.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param $parentItemId
     * @param $menuId
     * @param $parentItemIdNew
     * @param $newMenuId
     */
    private function findDuplicate($parentItemId, $menuId, $parentItemIdNew, $newMenuId) {
        $collection = $this->_itemFactory->create()->getCollection()
            ->addFieldToFilter('menu_id', ['eq' => $menuId])
            ->addFieldToFilter("parent_id", array('eq' => $parentItemId));
        $items = $collection->getItems();
        foreach ($items as $item) {
            $itemIdNew = $this->addDuplicate($item->getData(), $parentItemIdNew, $newMenuId);
            $this->findDuplicate($item->getId(), $menuId, $itemIdNew, $newMenuId);
        }
    }

    /**
     * @param $data
     * @param $parentNewId
     * @param $newMenuId
     * @return mixed
     */
    private function addDuplicate($data, $parentNewId, $newMenuId) {
        unset($data['item_id']);
        unset($data['creation_time']);
        unset($data['update_time']);
        $data['menu_id'] = $newMenuId;
        $data['parent_id'] = $parentNewId;
        $item = $this->_itemFactory->create()->setData($data)->save();

        return $item->getId();
    }

}
