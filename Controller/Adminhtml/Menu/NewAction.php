<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;


use Vnecoms\Megamenu\Controller\Adminhtml\Menu;

/**
 * Class New Action
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class NewAction extends Menu
{

    /**
     * New Action
     *
     * @param void
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultForward = $this->_resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
