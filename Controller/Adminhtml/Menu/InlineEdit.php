<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action\Context;
use Vnecoms\Megamenu\Api\MenuRepositoryInterface as MenuRepository;
use Magento\Framework\Controller\Result\JsonFactory;
use Vnecoms\Megamenu\Api\Data\MenuInterface;

class InlineEdit extends \Magento\Backend\App\Action
{
    /** @var MenuRepository  */
    protected $menuRepository;

    /** @var JsonFactory  */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param MenuRepository $menuRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        MenuRepository $menuRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->menuRepository = $menuRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $menuId) {
                    /** @var \Vnecoms\Megamenu\Model\Menu $menu */
                    $menu = $this->menuRepository->getById($menuId);
                    try {
                        $menu->setData(array_merge($menu->getData(), $postItems[$menuId]));
                        $this->menuRepository->save($menu);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithMenuId(
                            $menu,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add menu title to error message
     *
     * @param MenuInterface $menu
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithMenuId(MenuInterface $menu, $errorText)
    {
        return '[Menu ID: ' . $menu->getId() . '] ' . $errorText;
    }
}
