<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;

use Vnecoms\Megamenu\Controller\Adminhtml\Menu;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends \Magento\Backend\App\Action
{
    /** @var \Vnecoms\Megamenu\Model\MenuRepository  */
    protected $menuRepository;
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Vnecoms_Megamenu::menu_delete');
    }

    /**
     * Delete constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Vnecoms\Megamenu\Model\MenuRepository $menuRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Vnecoms\Megamenu\Model\MenuRepository $menuRepository
    ){
        parent::__construct($context);
        $this->menuRepository = $menuRepository;
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('menu_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {

            try {
                $this->menuRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The menu has been deleted.'));
                return $resultRedirect->setPath('megamenuadmin/*/');

            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The menu no longer exists.'));
                return $resultRedirect->setPath('megamenuadmin/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('megamenuadmin/menu/edit', ['menu_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the menu'));
                return $resultRedirect->setPath('megamenuadmin/menu/edit', ['menu_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a menu to delete.'));
        // go to grid
        return $resultRedirect->setPath('megamenuadmin/*/');
    }
}
