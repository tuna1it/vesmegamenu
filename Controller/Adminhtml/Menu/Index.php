<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml\Menu;

use Vnecoms\Megamenu\Controller\Adminhtml\Menu;

/**
 * Class Index
 * @category Vnecoms
 * @package  Vnecoms_Megamenu
 * @module   Megamenu
 * @author   Vnecoms Developer Team
 */
class Index extends Menu
{

    /**
     * Index Action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_session->unsMenuId();
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Vnecoms_Megamenu::menu_new');
        $resultPage->addBreadcrumb(__('Menu'), __('Menu'));
        $resultPage->addBreadcrumb(__('Manage Menus'), __('Manage Menus'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Menus'));

        $dataPersistor = $this->dataPersistor->clear('megamenu_menu');

        return $resultPage;

    }

}
