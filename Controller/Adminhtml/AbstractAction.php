<?php

namespace Vnecoms\Megamenu\Controller\Adminhtml;

use Magento\Framework\App\Request\DataPersistorInterface;
use Vnecoms\Megamenu\Api\MenuRepositoryInterface;
/**
 * Class AbstractAction
 * @package Vnecoms\Megamenu\Controller\Adminhtml
 */
abstract class AbstractAction extends \Magento\Backend\App\Action
{

    /** @var \Magento\Backend\Helper\Js  */
    protected $_jsHelper;
    /** @var \Magento\Store\Model\StoreManagerInterface  */
    protected $_storeManager;
    /** @var \Magento\Framework\Registry  */
    protected $_coreRegistry;
    /** @var \Magento\Backend\Model\View\Result\ForwardFactory  */
    protected $_resultForwardFactory;
    /** @var \Magento\Framework\View\Result\LayoutFactory  */
    protected $_resultLayoutFactory;

    /** @var \Magento\Framework\Controller\Result\RedirectFactory  */
    protected $_resultRedirectFactory;

    /** @var \Magento\Framework\View\Result\PageFactory  */
    protected $_resultPageFactory;
    /** @var \Magento\Framework\App\Response\Http\FileFactory  */
    protected $_fileFactory;

    /** @var \Magento\Framework\Api\DataObjectHelper */
    protected $_dataObjectHelper;

    /** @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface */
    protected $_localeDate;

    /** @var \Magento\Framework\Filesystem\Driver\File  */
    protected $_file;

    /** @var \Magento\Framework\Filesystem  */
    protected $_fileSystem;
    /** @var \Magento\Framework\Controller\Result\JsonFactory  */
    protected $jsonFactory;

    /** @var \Magento\Ui\Component\MassAction\Filter  */
    protected $filter;

    /** @var \Vnecoms\Megamenu\Model\MenuFactory  */
    protected $_menuFactory;

    /** @var \Vnecoms\Megamenu\Model\MenuRepository */
    protected $menuRepository;

    /** @var \Vnecoms\Megamenu\Model\ItemRepository */
    protected $itemRepository;

    /** @var DataPersistorInterface  */
    protected $dataPersistor;


    /**
     * AbstractAction constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Helper\Js $jsHelper
     * @param DataPersistorInterface $dataPersistor
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Vnecoms\Megamenu\Model\MenuFactory $menuFactory
     * @param \Vnecoms\Megamenu\Model\ItemFactory $itemFactory
     * @param \Vnecoms\Megamenu\Model\MenuRepository $menuRepository
     * @param \Vnecoms\Megamenu\Model\ItemRepository $itemRepository
     * @param \Vnecoms\Megamenu\Model\ResourceModel\Menu\CollectionFactory $menuCollectionFactory
     * @param \Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory $itemCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Helper\Js $jsHelper,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,

        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,


        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Vnecoms\Megamenu\Model\MenuFactory $menuFactory,
        \Vnecoms\Megamenu\Model\ItemFactory $itemFactory,
        \Vnecoms\Megamenu\Model\MenuRepository $menuRepository,
        \Vnecoms\Megamenu\Model\ItemRepository $itemRepository,
        \Vnecoms\Megamenu\Model\ResourceModel\Menu\CollectionFactory $menuCollectionFactory,
        \Vnecoms\Megamenu\Model\ResourceModel\Item\CollectionFactory $itemCollectionFactory
    ){
        parent::__construct($context);
        $this->_coreRegistry            = $registry;
        $this->_jsHelper                = $jsHelper;
        $this->dataPersistor            = $dataPersistor;
        $this->_storeManager            = $storeManager;
        $this->_fileFactory             = $fileFactory;
        $this->_fileSystem              = $fileSystem;
        $this->_dataObjectHelper        = $dataObjectHelper;
        $this->_resultPageFactory       = $resultPageFactory;
        $this->_resultLayoutFactory     = $resultLayoutFactory;
        $this->_resultRedirectFactory   = $context->getResultRedirectFactory();
        $this->_resultForwardFactory    = $resultForwardFactory;

        $this->_localeDate              = $localeDate;
        $this->_file                    = $file;
        $this->jsonFactory              = $jsonFactory;
        $this->filter                   = $filter;
        $this->_menuFactory             = $menuFactory;
        $this->_itemFactory             = $itemFactory;
        $this->menuRepository           = $menuRepository;
        $this->itemRepository           = $itemRepository;
        $this->_menuCollectionFactory   = $menuCollectionFactory;
        $this->_itemCollectionFactory   = $itemCollectionFactory;
    }

}
